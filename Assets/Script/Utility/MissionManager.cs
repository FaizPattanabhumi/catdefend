using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Utility
{
    public class MissionManager : MonoSingleton<MissionManager>
    {
        [SerializeField] public int allEnemyThisMission;
        [SerializeField] public int enemyLeft;
        [SerializeField] private int wave;
        [SerializeField] private EnemyList enemyList;
        [SerializeField] public WaveSystem waveSystem;

        void Start()
        {
            MissionUpdate();
        }

        public void MissionUpdate()
        {
            allEnemyThisMission = waveSystem.enemyLists.Length;
            enemyLeft = allEnemyThisMission;
        }
        
        public void EnemyScoreReset()
        {
            enemyLeft = allEnemyThisMission;
        }

        public void EnemyScored(int quantity)
        {
            enemyLeft -= quantity;

            if (enemyLeft <= 0)
            {
                GameManager.Instance.Win();
            }
        }
    }
}

