using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Scene = UnityEngine.SceneManagement.Scene;

namespace Script.Utility
{
    public class LoadManager : MonoBehaviour
    {

        void Start()
        {
            SceneManager.LoadScene("GameEngine", LoadSceneMode.Additive);
            //SceneManager.LoadScene("Mission1", LoadSceneMode.Additive);
        }
    
        void Update()
        {
            
        }
    }
}

