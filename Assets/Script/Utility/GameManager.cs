using System.Collections;
using System.Collections.Generic;
using Script.Map;
using Script.UI;
using Script.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
    public bool isCardOnHand;
    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject missionMenu;
    [SerializeField] private GameObject[] allEnemys;
    [SerializeField] private GameObject[] allCharacters;
    [SerializeField] private AudioSource audioSource;

    public void StartPlayGame()
    {
        audioSource.Play();
        mainMenu.SetActive(false);
        SceneManager.LoadScene("Mission1", LoadSceneMode.Additive);
    }
    public void GameOver()
    {
        gameOverMenu.SetActive(true);
    }
    
    public void Win()
    {
        MainMenu.Instance.GameOverMenu();
    }

    public void DestroyAllEnemy()
    {
        allEnemys = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in allEnemys)
        {
            Destroy(enemy);
        }
    }
    
    public void DestroyAllUnit()
    {
        allCharacters = GameObject.FindGameObjectsWithTag("Character");
        foreach (GameObject character in allCharacters)
        {
            Destroy(character);
        }
    }


    void Update()
    {
        Click();
    }
    
    void Click()
    {

        RaycastHit hit;
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
    
        if (Physics.Raycast(camRay, out hit ,100.0f))
        {
            if (hit.transform != null)
            {
                Debug.Log("hello");
            }
        }
        
    }
}
