using System.Collections;
using System.Collections.Generic;
using Script.Utility;
using UnityEngine;

namespace Script.Utility
{
    public class WaveSystem : MonoBehaviour
    {
        [SerializeField] private EnemyList enemyList;
        [SerializeField] public GameObject[] enemyLists;

        
        void SetEnemyList(GameObject enemy,int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                enemyLists[i] = enemy;
                MissionManager.Instance.allEnemyThisMission++;
            }
        }
    }
}

