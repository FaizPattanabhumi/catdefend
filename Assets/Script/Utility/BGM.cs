using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Utility
{
    public class BGM : MonoSingleton<BGM>
    {
        [SerializeField] public AudioSource audioSource;
        [SerializeField] public AudioClip mainMenuBGM;
        [SerializeField] public AudioClip mission1;
        [SerializeField] public AudioClip mission2;
        [SerializeField] public AudioClip mission3;
        [SerializeField] public AudioClip mission4;

        public void PlaySound(AudioClip sound)
        {
            audioSource.volume = 1;
            audioSource.clip = sound;
            audioSource.Play();
        }

    }
}

