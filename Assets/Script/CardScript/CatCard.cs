using System.Collections;
using System.Collections.Generic;
using Script.Character;
using Script.Map;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class CatCard : MonoBehaviour
{
    [SerializeField] protected GameObject character;
    [SerializeField] protected AudioSource audioSource;
    void Start()
    {
        
    }

    public void PickCard()
    {
        if (!GameManager.Instance.isCardOnHand)
        {
            audioSource.Play();
            Instantiate(character, new Vector3(0,0,0), quaternion.identity);
        }
    }
}
