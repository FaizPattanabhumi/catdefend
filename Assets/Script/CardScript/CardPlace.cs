using System;
using System.Collections;
using System.Collections.Generic;
using Script.Character;
using Script.Map;
using UnityEngine;

namespace Script.CardScript
{
    public class CardPlace : MonoBehaviour
    {
        protected Vector3 mousePosition;
        [SerializeField] protected BaseCatChar baseCatChar;
        [SerializeField] protected SnapPoint snappoint ;
        [SerializeField] protected SpriteRenderer spriteRenderer;
        [SerializeField] protected Color placeableColor;
        [SerializeField] protected Color unplaceableColor;
        [SerializeField] protected PlaceableArea placeableArea;
        [SerializeField] protected AudioClip placeSound;
        [SerializeField] protected AudioSource audioSource;
        public bool placeable;
        public bool isPlace;


        protected void Awake()
        {
            baseCatChar = GetComponent<BaseCatChar>();
        }

        protected virtual void Start()
        {
            if (spriteRenderer ==null)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
            isPlace = false;
            //PlaceablePoint();

        }

        protected void Update()
        {
            Control();
            if (placeable)
            {
                spriteRenderer.color = placeableColor;
            }
            else
            {
                spriteRenderer.color = unplaceableColor;

            }
            
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!isPlace)
            {
                transform.position = new Vector3(mousePosition.x, mousePosition.y, 1);
            }
        }

        void PlaceablePoint()
        {
            placeable = enabled;
        
        }

        void Control()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (placeable && Money.Instance.money >= baseCatChar.price && snappoint != null)
                {
                    baseCatChar.PlaySound(placeSound);
                    Debug.Log("hello");
                    Money.Instance.MoneyCheckOut(baseCatChar.price);
                    baseCatChar.isPlaced = true;
                    GameManager.Instance.isCardOnHand = false;
                    snappoint.isEmpty = false;
                    Destroy(gameObject.GetComponent<CardPlace>());

                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
            {
                Destroy(gameObject);
            }
        }

        protected void OnTriggerEnter(Collider other)
        {
            snappoint = other.GetComponent<SnapPoint>();
            
            if (snappoint != null && snappoint.isEmpty)
            {
                transform.position = snappoint.transform.position;
                baseCatChar.snappoint = snappoint;
                placeable = true;
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            snappoint = other.GetComponent<SnapPoint>();
            baseCatChar.snappoint = snappoint;
            
            if (snappoint != null && snappoint.isEmpty)
            {
                transform.position = snappoint.transform.position;
                placeable = true;
            }
            
            /*placeableArea = other.GetComponent<PlaceableArea>();

            if (placeableArea != null)
            {
                placeable = true;
            }*/
        }
        
        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            snappoint = other.GetComponent<SnapPoint>();

            if (snappoint != null)
            {
                placeable = false;
            }
            
            /*placeableArea = other.GetComponent<PlaceableArea>();
            if (placeableArea != null)
            {
                placeable = false;
            }*/
        }
        
        protected void OnDestroy()
        {
            GameManager.Instance.isCardOnHand = false;
            spriteRenderer.color = Color.white;
        }
    }
}

