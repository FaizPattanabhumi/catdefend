using System.Collections;
using System.Collections.Generic;
using Script.Character;
using Script.Map;
using UnityEngine;

namespace Script.CardScript
{
    public class CardPlaceField : MonoBehaviour
    {
        protected Vector3 mousePosition;
        [SerializeField] protected BaseCatChar baseCatChar;
        [SerializeField] protected SnapPointField snapPointField ;
        [SerializeField] protected SpriteRenderer spriteRenderer;
        [SerializeField] protected Color placeableColor;
        [SerializeField] protected Color unplaceableColor;
        [SerializeField] protected PlaceableArea placeableArea;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected AudioClip placeSound;

        public bool placeable;
        public bool isPlace;


        protected void Awake()
        {
            baseCatChar = GetComponent<BaseCatChar>();
        }

        protected virtual void Start()
        {
            if (spriteRenderer ==null)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
            isPlace = false;
            //PlaceablePoint();

        }

        protected void Update()
        {
            Control();
            if (placeable)
            {
                spriteRenderer.color = placeableColor;
            }
            else
            {
                spriteRenderer.color = unplaceableColor;

            }
            
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!isPlace)
            {
                transform.position = new Vector3(mousePosition.x, mousePosition.y, 1);
            }
        }

        void PlaceablePoint()
        {
            placeable = enabled;
        
        }

        void Control()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (placeable && Money.Instance.money >= baseCatChar.price && snapPointField != null)
                {
                    baseCatChar.PlaySound(placeSound);
                    audioSource.Play();
                    Debug.Log("hello");
                    Money.Instance.MoneyCheckOut(baseCatChar.price);
                    baseCatChar.isPlaced = true;
                    GameManager.Instance.isCardOnHand = false;
                    snapPointField.isEmpty = false;
                    Destroy(gameObject.GetComponent<CardPlaceField>());

                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
            {
                Destroy(gameObject);
            }
        }

        protected void OnTriggerEnter(Collider other)
        {
            snapPointField = other.GetComponent<SnapPointField>();
            
            if (snapPointField != null && snapPointField.isEmpty)
            {
                transform.position = snapPointField.transform.position;
                baseCatChar.snapPointField = snapPointField;
                placeable = true;
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            snapPointField = other.GetComponent<SnapPointField>();
            baseCatChar.snapPointField = snapPointField;
            
            if (snapPointField != null && snapPointField.isEmpty)
            {
                transform.position = snapPointField.transform.position;
                placeable = true;
            }
        }
        
        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            snapPointField = other.GetComponent<SnapPointField>();

            if (snapPointField != null)
            {
                placeable = false;
            }
        }
        
        protected void OnDestroy()
        {
            GameManager.Instance.isCardOnHand = false;
            spriteRenderer.color = Color.white;
        }
    }
}

