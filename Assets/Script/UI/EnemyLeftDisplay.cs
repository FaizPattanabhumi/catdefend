using System.Collections;
using System.Collections.Generic;
using Script.Map;
using Script.Utility;
using UnityEngine;
using UnityEngine.UI;

namespace Script.UI
{
    public class EnemyLeftDisplay : MonoBehaviour
    {
        [SerializeField] private Text enemyLeftText;
        void Start()
        {
            if (enemyLeftText == null)
            {
                GetComponent<Text>();
            }
        }
        
        void Update()
        {
            enemyLeftText.text = $"Enemy Left : {MissionManager.Instance.enemyLeft}";
        }
    }

}
