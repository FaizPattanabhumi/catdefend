using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Script.UI
{
    public class HPBarEnemy : MonoBehaviour
    {
        [SerializeField] private Script.Enemy.Enemy enemy;
    
        public Slider hpSlider;
        void Start()
        {
            hpSlider = GetComponent<Slider>();
            hpSlider.maxValue = enemy.hp;
        }
    
        void Update()
        {
            hpSlider.value = enemy.hp;
        }
    }
}


