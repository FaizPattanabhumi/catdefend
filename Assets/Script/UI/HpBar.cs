using System.Collections;
using System.Collections.Generic;
using Script.Map;
using UnityEngine;
using UnityEngine.UI;

namespace Script.UI
{
    public class HpBar : MonoBehaviour
    {
        [SerializeField] private ExitDoor exitDoorSript;

        public Slider hpSlider;
        void Start()
        {
            hpSlider = GetComponent<Slider>();
            hpSlider.maxValue = exitDoorSript.hpMax;
        }

        void Update()
        {
            hpSlider.value = exitDoorSript.hp;
        }
    }
}

