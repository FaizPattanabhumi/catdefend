using System;
using System.Collections;
using System.Collections.Generic;
using Script.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Script.UI
{
    public class MainMenu : MonoSingleton<MainMenu>
    {
        [SerializeField] private GameObject mainMenu;
        [SerializeField] private GameObject gameOverMenu;
        [SerializeField] private GameObject missionMenu;
        [SerializeField] public GameObject pauseMenu;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] public AudioClip mainMenuBGM;
        [SerializeField] public AudioClip missionSelectMenuBGM;
        [SerializeField] private string sceneNow;

        private void Start()
        {
            BGM.Instance.PlaySound(mainMenuBGM);
            mainMenu.SetActive(true);
            gameOverMenu.SetActive(false);
            missionMenu.SetActive(false);
            pauseMenu.SetActive(false);
        }

        public void ToMissionMenu()
        {
            audioSource.Play();
            BGM.Instance.PlaySound(missionSelectMenuBGM);
            mainMenu.SetActive(false);
            missionMenu.SetActive(true);
            gameOverMenu.SetActive(false);
            pauseMenu.SetActive(false);
        }
        
        public void BackToMainMenu()
        {
            audioSource.Play();
            BGM.Instance.PlaySound(mainMenuBGM);
            mainMenu.SetActive(true);
            gameOverMenu.SetActive(false);
            missionMenu.SetActive(false);
            pauseMenu.SetActive(false);
        }
        
        public void LoadMission(string mission)
        {
            audioSource.Play();
            missionMenu.SetActive(false);
            sceneNow = mission;
            SceneManager.LoadScene(mission, LoadSceneMode.Additive);
        }

        public void ContinueGame()
        {
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
            mainMenu.SetActive(false);
            gameOverMenu.SetActive(false);
            missionMenu.SetActive(false);
            pauseMenu.SetActive(false);
        }

        public void Unpause()
        {
            audioSource.Play();
            Time.timeScale = 1;
            mainMenu.SetActive(false);
            gameOverMenu.SetActive(false);
            pauseMenu.SetActive(false);
            missionMenu.SetActive(false);
        }

        public void GameOverMenu()
        {
            Unpause();
            mainMenu.SetActive(false);
            gameOverMenu.SetActive(true);
            pauseMenu.SetActive(false);
            missionMenu.SetActive(false);
        }

        public void ExitGame()
        {
            audioSource.Play();
            Application.Quit();
        }
        
        public void ToSelectMission()
        {
            audioSource.Play();
            BGM.Instance.PlaySound(missionSelectMenuBGM);
            Unpause();
            QuitMission();
            mainMenu.SetActive(false);
            gameOverMenu.SetActive(false);
            pauseMenu.SetActive(false);
            missionMenu.SetActive(true);
        }

        public void QuitMission()
        {
            audioSource.Play();
            GameManager.Instance.DestroyAllEnemy();
            GameManager.Instance.DestroyAllUnit();
            gameOverMenu.SetActive(false);
            SceneManager.UnloadSceneAsync(sceneNow);

        }
        
        public void RestartMission()
        {
            Unpause();
            audioSource.Play();
            QuitMission();
            SceneManager.LoadScene(sceneNow, LoadSceneMode.Additive);
            MissionManager.Instance.EnemyScoreReset();
        }
    }
}

