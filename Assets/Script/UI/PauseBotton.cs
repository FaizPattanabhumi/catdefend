using System;
using System.Collections;
using System.Collections.Generic;
using Script.UI;
using UnityEngine;

public class PauseBotton : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;


    public void Pause()
    {
        audioSource.Play();

        
        if (Time.timeScale >= 1)
        {
            Time.timeScale = 0;
            MainMenu.Instance.pauseMenu.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            MainMenu.Instance.pauseMenu.SetActive(false);
        }
    }
}
