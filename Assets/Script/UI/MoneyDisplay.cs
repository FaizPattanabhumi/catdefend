using System.Collections;
using System.Collections.Generic;
using Script.Map;
using UnityEngine;
using UnityEngine.UI;

namespace Script.UI
{
    public class MoneyDisplay : MonoBehaviour
    {
        [SerializeField] private Text moneyText;
        void Start()
        {
            GetComponent<Text>();
        }
        
        void Update()
        {
            moneyText.text = $"{Money.Instance.money}";
        }
    }
}

