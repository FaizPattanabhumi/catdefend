using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Enemy
{
    public interface IDamageable
    {
        void TakeHit(float damage);
    }
}

