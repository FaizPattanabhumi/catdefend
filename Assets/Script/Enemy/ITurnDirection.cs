using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Enemy
{
    
    public interface ITurnDirection
    {
        void ChangeMoveDir(Vector3 direction);
    }
}

