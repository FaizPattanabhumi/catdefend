using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Script.Character;
using Script.Map;
using Script.Utility;
using Unity.Mathematics;
using UnityEngine;

namespace Script.Enemy
{
    public class Enemy : MonoBehaviour,IDamageable,ITurnDirection
    {
        [SerializeField] public float hp;
        [SerializeField] private int moneyDrop;
        [SerializeField] protected Vector3 moveDir;
        [SerializeField] public float speed;
        [SerializeField] public float multSpeed;
        [SerializeField] private float damage;
        [SerializeField] protected Rigidbody2D rb;
        [SerializeField] public GameObject hitType;
        [SerializeField] public CatShield target;
        [SerializeField] public ExitDoor exitDoor;
        [SerializeField] public string moveStatus;
        [SerializeField] protected float cdt;
        [SerializeField] protected float cdtMax;
        [SerializeField] protected float knockBackForce;
        [SerializeField] protected AudioSource audioSource;
        [SerializeField] protected AudioClip aud_Takehit;
        
        private void Start()
        {
            ResetTimeCtrl();
            MoveLeft();
        }

        void Update()
        {
            Move();
            TimeCtrl();
        }

        public void MoveLeft()
        {
            moveDir = new Vector3(-speed,0,0);
            moveStatus = "left";
        }
        public void MoveRight()
        {
            moveDir = new Vector3(speed,0,0);
            moveStatus = "right";

        }
        public void MoveUp()
        {
            moveDir = new Vector3(0,speed,0);
            moveStatus = "up";

        }
        public void MoveDown()
        {
            moveDir = new Vector3(0,-speed,0);
            moveStatus = "down";

        }

        private void Move()
        {
            transform.position += moveDir * Time.deltaTime* multSpeed;
        }

        public void TakeHit(float damage)
        { 
            Debug.Log("Take hit!!");
            
            Playsound(aud_Takehit);
            hp -= damage;
        
            if (hp <= 0)
            {
                Dead();
            }
        }

        public void Kill()
        {
            Dead();
        }

        protected void Playsound(AudioClip sound)
        {
            audioSource.clip = sound;
            audioSource.Play();
        }
        
        public void ChangeMoveDir(Vector3 direction)
        {
            moveDir = direction;
        }
        
        
        void Dead()
        {
            Money.Instance.GiveMoney(moneyDrop);
            MissionManager.Instance.EnemyScored(1);
            Destroy(gameObject);
        }

        void FlipMoveDir()
        {
            
            switch (moveStatus)
            {
                case "up":
                {
                    MoveDown();
                    break;
                }
                case "down":
                {
                    MoveUp();
                    break;
                }
                case "left":
                {
                    MoveRight();
                    break;
                }
                case "right":
                {
                    MoveLeft();
                    break;
                }
                
            }
        }

        void Reflect()
        {
            FlipMoveDir();
            multSpeed = knockBackForce;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            target = other.GetComponent<CatShield>();
            exitDoor = other.GetComponent<ExitDoor>();

            if (target != null && target.isPlaced)
            {
                Reflect();
                ResetTimeCtrl();
                target.TakeHit(damage);
            }
            if (exitDoor != null)
            {
                exitDoor.TakeHit(damage);
            }
            
        }
        
        protected void ResetSpeedMult()
        {
            multSpeed = 1;
        }
        
        protected void ResetTimeCtrl()
        {
            cdt = cdtMax;
        }
        
        protected void TimeCtrl()
        {
            if (cdt > 0)
            {
                cdt -= Time.deltaTime;
            }
            if (cdt <= 0 && multSpeed != 1)
            {
                FlipMoveDir();
                ResetSpeedMult();
            }
        }
        
    }
}

