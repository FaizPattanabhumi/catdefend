using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Character.DamageArea
{
    public class CharDamAreaShield : MonoBehaviour
    {
        [SerializeField] protected CatShield catShield;
        [SerializeField] public Enemy.Enemy target;

        void Start()
        {
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            target = other.GetComponent<Enemy.Enemy>();
            target.TakeHit(catShield.damage);
            Debug.Log("Royal Guard!!");
        }
    }
}

