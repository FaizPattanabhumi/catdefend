using System.Collections;
using System.Collections.Generic;
using Script.Character.Hit;
using UnityEngine;

namespace Script.Character.DamageArea
{
    public class CharDamAreaFire : MonoBehaviour
    {
        [SerializeField] protected CircleCollider2D hitBox;
        [SerializeField] protected CatFire catFire;
        [SerializeField] public float radius;
        [SerializeField] public Enemy.Enemy target;

        void Start()
        {
            hitBox.radius = radius;
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            target = other.GetComponent<Enemy.Enemy>();
            catFire.target = target;

            if (catFire.attackCoolDown <= 0 && target != null)
            {
                catFire.Attack(transform.position);
                Debug.Log("Attack!!");
                catFire.ResetCoolDownTime();
            }
        }
    }
}

