using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Script.Character.DamageArea
{
    public class CharacterDamageArea : MonoBehaviour
    {
        [SerializeField] protected CircleCollider2D hitBox;
        [SerializeField] protected CatMelee catMelee;
        [SerializeField] public float radius;
        [SerializeField] public Enemy.Enemy target;

        void Start()
        {
            hitBox.radius = radius;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            target = other.GetComponent<Enemy.Enemy>();
            catMelee.target = target;

            if (catMelee.attackCoolDown <= 0)
            {
                catMelee.Attack(target.transform.position);
                target.TakeHit(catMelee.damage);
                Debug.Log("Attack!!");
                catMelee.ResetCoolDownTime();
            }
        }
    }
}


