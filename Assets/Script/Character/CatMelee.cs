using System;
using System.Collections;
using System.Collections.Generic;
using Script.Map;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;

namespace Script.Character
{
    public class CatMelee : BaseCatChar
    {
        
        [SerializeField] public float damage;
        [SerializeField] protected float lookRadius;
        [SerializeField] public float attackCoolDown;
        [SerializeField] protected float attackCoolDownMax;
        [SerializeField] public GameObject hitType;
        [SerializeField] public Enemy.Enemy target;
        [SerializeField] public Animation animation;
        [SerializeField] public AudioClip aud_Attack;
        protected void Start()
        {
            attackCoolDown = attackCoolDownMax;
            GameManager.Instance.isCardOnHand = true;
            isPlaced = false;
            normalColor = spriteRenderer.color;
        }
        
        protected void Update()
        {
            if (isPlaced)
            {
                CoolDownCtrl();
            }
        }

        protected void CoolDownCtrl()
        {
            attackCoolDown -= Time.deltaTime;
            if (attackCoolDown <= -1)
            {
                ResetCoolDownTime();
            }  
            
        }

        public void ResetCoolDownTime()
        {
            attackCoolDown = attackCoolDownMax;
        }

        public void StopAttack()
        {
            animator.SetBool("IsAttack",false);
        }
        
        public void Attack(Vector3 enemyPosision)
        {
            var enemyDir = target.gameObject.transform.position;
            
            if (enemyDir.x > transform.position.x)
            {
                spriteRenderer.flipX = false;
            }
            else if (enemyDir.x <= transform.position.x)
            {
                spriteRenderer.flipX = true;
            }
            
            PlaySound(aud_Attack);
            animator.SetBool("IsAttack",true);
            Instantiate(hitType,enemyPosision,quaternion.identity);
        }
    

    }
}

