using System.Collections;
using System.Collections.Generic;
using Script.Character.Hit;
using Unity.Mathematics;
using UnityEngine;


namespace Script.Character
{
    public class CatFire : CatMelee
    {

        void Start()
        {
            attackCoolDown = attackCoolDownMax;
            GameManager.Instance.isCardOnHand = true;
            isPlaced = false;
            normalColor = spriteRenderer.color;

        }
        
        public void Attack(Vector3 atkPosision)
        {
            hitType.GetComponent<HitDamageBullet>().targetOBJ = target.gameObject;
            
            var enemyDir = target.gameObject.transform.position;
            
            if (enemyDir.x > transform.position.x)
            {
                spriteRenderer.flipX = false;
            }
            else if (enemyDir.x <= transform.position.x)
            {
                spriteRenderer.flipX = true;
            }
            
            PlaySound(aud_Attack);
            animator.SetBool("IsAttack",true);
            Instantiate(hitType,atkPosision,quaternion.identity);
        }
    }
}

