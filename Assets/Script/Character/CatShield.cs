using System.Collections;
using System.Collections.Generic;
using Script.Enemy;
using Script.Map;
using Unity.Mathematics;
using UnityEngine;

namespace Script.Character
{
    public class CatShield : BaseCatChar,IDamageable
    {
        [SerializeField] private float hp;
        [SerializeField] public float damage;
        [SerializeField] protected float lookRadius;
        [SerializeField] public GameObject hitType;
        [SerializeField] protected AudioClip aud_Takehit;
        protected void Start()
        {
            GameManager.Instance.isCardOnHand = true;
            isPlaced = false;
            normalColor = spriteRenderer.color;
        }
        
        public void TakeHit(float damage)
        {
            PlaySound(aud_Takehit,0.4f);
            animator.SetBool("IsAttack",true);
            hp -= damage;
            if (hp <= 0)
            {
                Dead();
            }
            //Instantiate(hitType, transform.position, quaternion.identity);
        }
        
        public void StopTakeHit()
        {
            animator.SetBool("IsAttack",false);
        }
                
        void Dead()
        {
            Destroy(gameObject);
        }
        
        protected void OnDestroy()
        {
            snapPointField.isEmpty = true;
        }
    }
}

