using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;


namespace Script.Character.Hit
{
    public class HitDamageBullet : HitDamage
    {
        [SerializeField] public GameObject targetOBJ;
        [SerializeField] public CatFire catFire;
        [SerializeField] private float speed;
        
        void Start()
        {
            Vector3 enemyDir = targetOBJ.transform.position.normalized;
        }
        
        void Update()
        {
            
            if (targetOBJ != null)
            {
                MoveTo(targetOBJ);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == targetOBJ)
            {
                var target = other.GetComponent<Enemy.Enemy>();
                target.TakeHit(catFire.damage);
                Destroy(gameObject);
            }
        }

        void MoveTo(GameObject target)
        {
            Vector3 enemyDir = target.transform.position - transform.position;
            
            enemyDir.Normalize();
            transform.Translate(enemyDir*Time.deltaTime*speed);

        }
    }
}

