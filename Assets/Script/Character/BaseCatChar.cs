using System;
using System.Collections;
using System.Collections.Generic;
using Script.Enemy;
using Script.Map;
using UnityEngine;

namespace Script.Character
{
    public class BaseCatChar : MonoBehaviour
    {
        [SerializeField] public int price;
        [SerializeField] public bool isPlaced;
        [SerializeField] protected Animator animator;
        [SerializeField] private AudioSource audioSource;
        [SerializeField] public SnapPoint snappoint ;
        [SerializeField] public SnapPointField snapPointField ;
        [SerializeField] protected SpriteRenderer spriteRenderer;
        [SerializeField] protected Color normalColor;
        [SerializeField] protected Color selectColor;
        
        
        public void PlaySound(AudioClip sound)
        {
            audioSource.volume = 1;
            audioSource.clip = sound;
            audioSource.Play();
        }
        public void PlaySound(AudioClip sound,float volume)
        {
            audioSource.clip = sound;
            audioSource.volume = volume;
            audioSource.Play();
        }
        protected void OnMouseDown()
        {
            if (isPlaced)
            {
                Sale();
            }
        }

        protected void OnMouseOver()
        {
            if (isPlaced)
            {
                spriteRenderer.color = selectColor;
            }
        }

        protected void OnMouseExit()
        {
            if (isPlaced)
            {
                spriteRenderer.color = normalColor;
            }
        }
        
        public void Sale()
        {
            Money.Instance.GiveMoney(price/2);
            Dead();
        }

        protected void OnDestroy()
        {
            snappoint.isEmpty = true;
        }

        protected void Dead()
        {
            Destroy(gameObject);
        }

    }
}

