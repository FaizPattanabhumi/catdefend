using System.Collections;
using System.Collections.Generic;
using Script.Utility;
using Unity.Mathematics;
using UnityEngine;

namespace Script.Map
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private GameObject enemy;
        [SerializeField] private float cooldown; 
        [SerializeField] private float cooldownMax;
        [SerializeField] private WaveSystem waveSystem;
        [SerializeField] private int enemyListsIndex;
        void Start()
        {
            enemyListsIndex = 0;
        }

        void Update()
        {
            cooldown -= Time.deltaTime;
            Spawn();
        }

        void Spawn()
        {
            if (enemyListsIndex <= MissionManager.Instance.allEnemyThisMission)
            {
                enemy = MissionManager.Instance.waveSystem.enemyLists[enemyListsIndex];

                if (cooldown <= 0)
                {
                    Instantiate(enemy, transform.position, quaternion.identity);
                    cooldown = cooldownMax;
                    enemyListsIndex ++;
                }
            }
        }
    }
}

