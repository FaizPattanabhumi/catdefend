using System;
using System.Collections;
using System.Collections.Generic;
using Script.Enemy;
using UnityEngine;

public class TurnDirPoint : MonoBehaviour
{
    [SerializeField] private Vector3 newDir;
    [SerializeField] private Enemy enemyScript;
    [SerializeField] private string DirStatus;
    
    private void Start()
    {
        DirStatus.ToLower();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        enemyScript = other.GetComponent<Enemy>();
        if (enemyScript != null)
        {
            if (DirStatus == "up")
            {
                enemyScript.MoveUp();
            }
            else if (DirStatus == "down")
            {
                enemyScript.MoveDown();

            }
            else if (DirStatus == "left")
            {
                enemyScript.MoveLeft();
            }
            else if (DirStatus == "right")
            {
                enemyScript.MoveRight();
            }
            //enemyScript.ChangeMoveDir(newDir);
        }
    }
}
