using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Map
{
    public class Money : MonoSingleton<Money>
    {
        [SerializeField] public int money;
        
        private void Update()
        {
            
        }

        public void GiveMoney(int money)
        {
            this.money += money;
        }
        public void MoneyCheckOut(int money)
        {
            this.money -= money;
        }

    }
}

