using System;
using System.Collections;
using System.Collections.Generic;
using Script.Enemy;
using UnityEngine;

namespace Script.Map
{
    public class ExitDoor : MonoBehaviour,IDamageable
    {
        [SerializeField] public float hpMax;
        [SerializeField] public float hp;
        [SerializeField] private Enemy.Enemy enemy;

        void Start()
        {
            hp = hpMax;
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            enemy = other.GetComponent<Enemy.Enemy>();
            
            if (enemy != null)
            {
                enemy.Kill();
            }
        }
        
        public void TakeHit(float damage)
        {
            hp -= damage;
            if (hp <= 0)
            {
                GameManager.Instance.GameOver();
            }
        }
    }
}

