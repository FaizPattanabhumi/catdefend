using System.Collections;
using System.Collections.Generic;
using Script.Utility;
using UnityEngine;

namespace Script.Map
{
   public class Mission : MonoBehaviour
   {
       [SerializeField] public GameObject[] enemyLists;
       [SerializeField] public int moneyStart;
       [SerializeField] public AudioClip bgm;
       
       // Start is called before the first frame update
       void Start()
       {
           Money.Instance.money = moneyStart;
           MissionManager.Instance.waveSystem.enemyLists = enemyLists;
           MissionManager.Instance.MissionUpdate();
           BGM.Instance.PlaySound(bgm);
       }
   
       // Update is called once per frame
       void Update()
       {
           
       }
   } 
}

