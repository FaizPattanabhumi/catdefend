﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6_CustomAttributesCacheGenerator_character(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_newDir(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_enemyScript(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_DirStatus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_gameOverMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_mainMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_missionMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_allEnemys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_allCharacters(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E_CustomAttributesCacheGenerator_cat1(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E_CustomAttributesCacheGenerator_enemy1(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8_CustomAttributesCacheGenerator_allEnemies(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_allEnemyThisMission(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_enemyLeft(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_wave(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_enemyList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_waveSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA_CustomAttributesCacheGenerator_enemyList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA_CustomAttributesCacheGenerator_enemyLists(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5_CustomAttributesCacheGenerator_enemyLeftText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB_CustomAttributesCacheGenerator_exitDoorSript(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_mainMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_gameOverMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_missionMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_pauseMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_sceneNow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D_CustomAttributesCacheGenerator_moneyText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_hpMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_hp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_enemy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51_CustomAttributesCacheGenerator_enemyLists(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51_CustomAttributesCacheGenerator_moneyStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4_CustomAttributesCacheGenerator_money(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_CustomAttributesCacheGenerator_isEmpty(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_enemy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_cooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_cooldownMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_waveSystem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_enemyListsIndex(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_hp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moneyDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moveDir(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_multSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_rb(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_hitType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_exitDoor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moveStatus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_cdt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_cdtMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_knockBackForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_aud_Takehit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_price(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_isPlaced(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_snappoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_snapPointField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_spriteRenderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_normalColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_selectColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_lookRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_attackCoolDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_attackCoolDownMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_hitType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_spriteRenderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_animation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_aud_Attack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_hp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_lookRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_hitType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_spriteRenderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_aud_Takehit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD_CustomAttributesCacheGenerator_timeToDestroy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_targetOBJ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_catFire(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_hitBox(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_catFire(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89_CustomAttributesCacheGenerator_catShield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_hitBox(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_catMelee(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_baseCatChar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_snappoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_spriteRenderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_placeableColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_unplaceableColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_placeableArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_baseCatChar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_snapPointField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_spriteRenderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_placeableColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_unplaceableColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_placeableArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[112] = 
{
	CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6_CustomAttributesCacheGenerator_character,
	CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6_CustomAttributesCacheGenerator_audioSource,
	TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_newDir,
	TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_enemyScript,
	TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B_CustomAttributesCacheGenerator_DirStatus,
	PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF_CustomAttributesCacheGenerator_audioSource,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_gameOverMenu,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_mainMenu,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_missionMenu,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_allEnemys,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_allCharacters,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_audioSource,
	CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E_CustomAttributesCacheGenerator_cat1,
	CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E_CustomAttributesCacheGenerator_enemy1,
	EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8_CustomAttributesCacheGenerator_allEnemies,
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_allEnemyThisMission,
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_enemyLeft,
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_wave,
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_enemyList,
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC_CustomAttributesCacheGenerator_waveSystem,
	WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA_CustomAttributesCacheGenerator_enemyList,
	WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA_CustomAttributesCacheGenerator_enemyLists,
	EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5_CustomAttributesCacheGenerator_enemyLeftText,
	HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB_CustomAttributesCacheGenerator_exitDoorSript,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_mainMenu,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_gameOverMenu,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_missionMenu,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_pauseMenu,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_audioSource,
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712_CustomAttributesCacheGenerator_sceneNow,
	MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D_CustomAttributesCacheGenerator_moneyText,
	ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_hpMax,
	ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_hp,
	ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_CustomAttributesCacheGenerator_enemy,
	Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51_CustomAttributesCacheGenerator_enemyLists,
	Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51_CustomAttributesCacheGenerator_moneyStart,
	Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4_CustomAttributesCacheGenerator_money,
	SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_CustomAttributesCacheGenerator_isEmpty,
	Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_enemy,
	Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_cooldown,
	Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_cooldownMax,
	Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_waveSystem,
	Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C_CustomAttributesCacheGenerator_enemyListsIndex,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_hp,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moneyDrop,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moveDir,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_speed,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_multSpeed,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_damage,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_rb,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_hitType,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_target,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_exitDoor,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_moveStatus,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_cdt,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_cdtMax,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_knockBackForce,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_audioSource,
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_CustomAttributesCacheGenerator_aud_Takehit,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_price,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_isPlaced,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_animator,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_audioSource,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_snappoint,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_snapPointField,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_spriteRenderer,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_normalColor,
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_CustomAttributesCacheGenerator_selectColor,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_damage,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_lookRadius,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_attackCoolDown,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_attackCoolDownMax,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_hitType,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_spriteRenderer,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_target,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_animation,
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241_CustomAttributesCacheGenerator_aud_Attack,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_hp,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_damage,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_lookRadius,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_hitType,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_spriteRenderer,
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_CustomAttributesCacheGenerator_aud_Takehit,
	HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD_CustomAttributesCacheGenerator_timeToDestroy,
	HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_targetOBJ,
	HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_catFire,
	HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_CustomAttributesCacheGenerator_speed,
	CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_hitBox,
	CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_catFire,
	CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_radius,
	CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60_CustomAttributesCacheGenerator_target,
	CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89_CustomAttributesCacheGenerator_catShield,
	CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89_CustomAttributesCacheGenerator_target,
	CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_hitBox,
	CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_catMelee,
	CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_radius,
	CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE_CustomAttributesCacheGenerator_target,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_baseCatChar,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_snappoint,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_spriteRenderer,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_placeableColor,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_unplaceableColor,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_placeableArea,
	CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_CustomAttributesCacheGenerator_audioSource,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_baseCatChar,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_snapPointField,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_spriteRenderer,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_placeableColor,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_unplaceableColor,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_placeableArea,
	CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_CustomAttributesCacheGenerator_audioSource,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
