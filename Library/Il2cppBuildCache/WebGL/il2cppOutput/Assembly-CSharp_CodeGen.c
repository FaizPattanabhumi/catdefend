﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CatCard::Start()
extern void CatCard_Start_mC241CE6745BC8FD82EC3AE235240B2A174789C5E (void);
// 0x00000002 System.Void CatCard::PickCard()
extern void CatCard_PickCard_m5FA425EBE1D19368F1BEF2AF0AEBFEB628C3B120 (void);
// 0x00000003 System.Void CatCard::.ctor()
extern void CatCard__ctor_mA8DB16F69268E6DFF800BFBDC376F21E81FD860C (void);
// 0x00000004 System.Void TurnDirPoint::Start()
extern void TurnDirPoint_Start_m0DD9DDD9AB0645EEBCE50C32708737663701F2AA (void);
// 0x00000005 System.Void TurnDirPoint::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void TurnDirPoint_OnTriggerEnter2D_mAB3275449BFFC89D2363BAD76749066A7596D8CB (void);
// 0x00000006 System.Void TurnDirPoint::.ctor()
extern void TurnDirPoint__ctor_m65113737944B0D2DFF22C8E643B13E9A3E0C3757 (void);
// 0x00000007 System.Void PauseBotton::Pause()
extern void PauseBotton_Pause_m7EACECE09CBE86C991D2E9E09B10EA654D2A3025 (void);
// 0x00000008 System.Void PauseBotton::.ctor()
extern void PauseBotton__ctor_mF41F008D32F714DFABAFF5B445B275F3E847851F (void);
// 0x00000009 System.Void DontDestoryOnLoad::Start()
extern void DontDestoryOnLoad_Start_mF1ECB298CFB40940BA1402634E7553F04350C70D (void);
// 0x0000000A System.Void DontDestoryOnLoad::Update()
extern void DontDestoryOnLoad_Update_mDBA5350D6EECD070FBFA9A32867F7F736AE23BC9 (void);
// 0x0000000B System.Void DontDestoryOnLoad::.ctor()
extern void DontDestoryOnLoad__ctor_m684596B308FCF9D95C6C3FE10FE0799361B41C7F (void);
// 0x0000000C System.Void GameManager::StartPlayGame()
extern void GameManager_StartPlayGame_m7BBCED5DB288F74C0CD49F9F943D854C381E23E0 (void);
// 0x0000000D System.Void GameManager::GameOver()
extern void GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A (void);
// 0x0000000E System.Void GameManager::Win()
extern void GameManager_Win_mEE312032712783936AAC9B0EF0CED7BDB345C973 (void);
// 0x0000000F System.Void GameManager::DestroyAllEnemy()
extern void GameManager_DestroyAllEnemy_m3A3AFCD2EC6A82A49FAC55F527ACC6F9E91910EE (void);
// 0x00000010 System.Void GameManager::DestroyAllUnit()
extern void GameManager_DestroyAllUnit_m6BDE5206F29B58B5724BD590AA77466D6B545E97 (void);
// 0x00000011 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000012 System.Void GameManager::Click()
extern void GameManager_Click_mF51E03F3AF95EA64F1B3AED7315C42BE91F1A4C9 (void);
// 0x00000013 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000014 System.Void MainGameScene::Start()
extern void MainGameScene_Start_mD147BCC62885A9DD85408C33589F0260E2731B49 (void);
// 0x00000015 System.Void MainGameScene::.ctor()
extern void MainGameScene__ctor_mB30666241CA8E49251D2F6B83FDC3A35D010515A (void);
// 0x00000016 T MonoSingleton`1::get_Instance()
// 0x00000017 System.Void MonoSingleton`1::OnApplicationQuit()
// 0x00000018 System.Void MonoSingleton`1::OnDestroy()
// 0x00000019 System.Void MonoSingleton`1::.ctor()
// 0x0000001A System.Void MonoSingleton`1::.cctor()
// 0x0000001B System.Void Script.Utility.CatCharacterManager::Start()
extern void CatCharacterManager_Start_mF7570321D75F115A87A64CDE915E44377B325C59 (void);
// 0x0000001C System.Void Script.Utility.CatCharacterManager::Update()
extern void CatCharacterManager_Update_m0FC05CDF51AD4A32FA7F94BE52BF50D73CAC3D8B (void);
// 0x0000001D System.Void Script.Utility.CatCharacterManager::.ctor()
extern void CatCharacterManager__ctor_m9D51FC19D75231C828EF2DCF9EFDAAAA49908BCC (void);
// 0x0000001E System.Void Script.Utility.EnemyList::Start()
extern void EnemyList_Start_m95FEA6B01F792722641A11943E58996FCD08E8D0 (void);
// 0x0000001F System.Void Script.Utility.EnemyList::Update()
extern void EnemyList_Update_mFDE57CAB9C507EC2482B7AE4F32288A6EEC4169F (void);
// 0x00000020 System.Void Script.Utility.EnemyList::.ctor()
extern void EnemyList__ctor_m5EE9B37FB4E3A01813C4F201AFF7D9FC85715634 (void);
// 0x00000021 System.Void Script.Utility.LoadManager::Start()
extern void LoadManager_Start_mBEBED79E4C651C714EEB9A187492A6941210F606 (void);
// 0x00000022 System.Void Script.Utility.LoadManager::Update()
extern void LoadManager_Update_m5C6D8E807663A5938A7A442D8FC762F22ACC5933 (void);
// 0x00000023 System.Void Script.Utility.LoadManager::.ctor()
extern void LoadManager__ctor_m2D6202BAFF14E8DC356007DA86DE8EF58C6474B2 (void);
// 0x00000024 System.Void Script.Utility.MissionManager::Start()
extern void MissionManager_Start_mA02C8F3F85E450223FA4D69625B0572511A23275 (void);
// 0x00000025 System.Void Script.Utility.MissionManager::MissionUpdate()
extern void MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09 (void);
// 0x00000026 System.Void Script.Utility.MissionManager::EnemyScoreReset()
extern void MissionManager_EnemyScoreReset_m4CD561F1F4C88D55C232A092577228D956C4284C (void);
// 0x00000027 System.Void Script.Utility.MissionManager::EnemyScored(System.Int32)
extern void MissionManager_EnemyScored_m7C68CEFFF5713951EB7533DF47E4C617E04142D9 (void);
// 0x00000028 System.Void Script.Utility.MissionManager::.ctor()
extern void MissionManager__ctor_mD4375AA12B01D60C54E5E6C0314EC9DF7175FAF7 (void);
// 0x00000029 System.Void Script.Utility.WaveSystem::SetEnemyList(UnityEngine.GameObject,System.Int32)
extern void WaveSystem_SetEnemyList_mB48BB6F64C31F9F83D7EB0DA2222BE53446FB3FC (void);
// 0x0000002A System.Void Script.Utility.WaveSystem::.ctor()
extern void WaveSystem__ctor_m9AAE748E210C416A9AE4D6580C8E5EDE49E91278 (void);
// 0x0000002B System.Void Script.UI.EnemyLeftDisplay::Start()
extern void EnemyLeftDisplay_Start_m69CEFDCE50FC07642ABF22369424302680553E6D (void);
// 0x0000002C System.Void Script.UI.EnemyLeftDisplay::Update()
extern void EnemyLeftDisplay_Update_m080FE6FA1F785DC27F4DBDFAFF455ADC81E0D109 (void);
// 0x0000002D System.Void Script.UI.EnemyLeftDisplay::.ctor()
extern void EnemyLeftDisplay__ctor_m8F2B4D422373983AD5DE7EB8008ECD41A0C8C6D5 (void);
// 0x0000002E System.Void Script.UI.HpBar::Start()
extern void HpBar_Start_mD11553FFD1EC893692B5F48F7139A6691257FD0F (void);
// 0x0000002F System.Void Script.UI.HpBar::Update()
extern void HpBar_Update_mD7026F64E4A9A8E085372A76CFBF13AEEBDDC7FB (void);
// 0x00000030 System.Void Script.UI.HpBar::.ctor()
extern void HpBar__ctor_mAE6ED973236E66EE76BCEBB8058A3697174ED037 (void);
// 0x00000031 System.Void Script.UI.MainMenu::Start()
extern void MainMenu_Start_mFB01E91118351E6A180E4B35DD8208570B3423A9 (void);
// 0x00000032 System.Void Script.UI.MainMenu::ToMissionMenu()
extern void MainMenu_ToMissionMenu_m4E0B4F72D691101344ED04BE27C17144BA284075 (void);
// 0x00000033 System.Void Script.UI.MainMenu::BackToMainMenu()
extern void MainMenu_BackToMainMenu_m4A1EB4F827008EAAE312A42222C0DAB9F487E7CE (void);
// 0x00000034 System.Void Script.UI.MainMenu::LoadMission(System.String)
extern void MainMenu_LoadMission_m49E22F2C0751DD492EFB1D927E1322C45530F5DB (void);
// 0x00000035 System.Void Script.UI.MainMenu::ContinueGame()
extern void MainMenu_ContinueGame_m3FB0CD0540008DC130E94148E91E748DF5ED436F (void);
// 0x00000036 System.Void Script.UI.MainMenu::Unpause()
extern void MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE (void);
// 0x00000037 System.Void Script.UI.MainMenu::GameOverMenu()
extern void MainMenu_GameOverMenu_m1C3686D2523C552479A2FE8B38DD9C35754D16D2 (void);
// 0x00000038 System.Void Script.UI.MainMenu::ExitGame()
extern void MainMenu_ExitGame_mB79D76C015CA5ED89BEC1719CC6574065C77B1FE (void);
// 0x00000039 System.Void Script.UI.MainMenu::ToSelectMission()
extern void MainMenu_ToSelectMission_m819C84188FCE9AAEFC66A4104C1ACBACCB50C5CB (void);
// 0x0000003A System.Void Script.UI.MainMenu::QuitMission()
extern void MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898 (void);
// 0x0000003B System.Void Script.UI.MainMenu::RestartMission()
extern void MainMenu_RestartMission_m0587B90AEE8AC5E8868B75261CFAEA3F64ACF7D1 (void);
// 0x0000003C System.Void Script.UI.MainMenu::.ctor()
extern void MainMenu__ctor_mBA722739613EF4F7A5503DD127B61987270B02A1 (void);
// 0x0000003D System.Void Script.UI.MoneyDisplay::Start()
extern void MoneyDisplay_Start_mDED1D862FEE3D5C64D1E923EA86047A8A993099F (void);
// 0x0000003E System.Void Script.UI.MoneyDisplay::Update()
extern void MoneyDisplay_Update_m957EA6AF0036944C87FACD8546D25DCDFCEB6A21 (void);
// 0x0000003F System.Void Script.UI.MoneyDisplay::.ctor()
extern void MoneyDisplay__ctor_m2981E1A16DF68402A137D54AA91CE41AE0E19C38 (void);
// 0x00000040 System.Void Script.Map.ExitDoor::Start()
extern void ExitDoor_Start_m13891693B8E6565AAAB2C8BE7AFDF2CDEFE880F9 (void);
// 0x00000041 System.Void Script.Map.ExitDoor::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ExitDoor_OnTriggerEnter2D_mF52D19F36242CF772503CF35004E76C03805E371 (void);
// 0x00000042 System.Void Script.Map.ExitDoor::TakeHit(System.Single)
extern void ExitDoor_TakeHit_mC9D5DEAD44D641FD35E135CEA79308AD76EC897E (void);
// 0x00000043 System.Void Script.Map.ExitDoor::.ctor()
extern void ExitDoor__ctor_mF889A857E4FA662218AF656AA3FA847F100048DC (void);
// 0x00000044 System.Void Script.Map.Mission::Start()
extern void Mission_Start_m5A6A792E9D8375F70EAF79C321BC046E3AC515EA (void);
// 0x00000045 System.Void Script.Map.Mission::Update()
extern void Mission_Update_mC912E95F3235165B5335C184845D9B8986FBB89D (void);
// 0x00000046 System.Void Script.Map.Mission::.ctor()
extern void Mission__ctor_mDB11C1F9D50E4E4C77631DDD2CE4E265DCE6F804 (void);
// 0x00000047 System.Void Script.Map.Money::Update()
extern void Money_Update_m09ECD0552CCBBBDD222F24A0F19DD77956267C75 (void);
// 0x00000048 System.Void Script.Map.Money::GiveMoney(System.Int32)
extern void Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F (void);
// 0x00000049 System.Void Script.Map.Money::MoneyCheckOut(System.Int32)
extern void Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16 (void);
// 0x0000004A System.Void Script.Map.Money::.ctor()
extern void Money__ctor_m7E96BF51569536AE65915183551B6FCD4FB0D2D8 (void);
// 0x0000004B System.Void Script.Map.PlaceableArea::.ctor()
extern void PlaceableArea__ctor_m9A60E0D3BDEA111825956D4726867E016224C5C8 (void);
// 0x0000004C System.Void Script.Map.PlaceableAreaOnField::.ctor()
extern void PlaceableAreaOnField__ctor_m4BF8F23BF29C82BA7B527A6C5BF8B7AB01415FF1 (void);
// 0x0000004D System.Void Script.Map.SnapPoint::.ctor()
extern void SnapPoint__ctor_m66C9037DF5523C44D203EAE05060D7CA456F9FEA (void);
// 0x0000004E System.Void Script.Map.SnapPointField::.ctor()
extern void SnapPointField__ctor_m1AFC9AE0173236234CB51345D78FE9F7A7A54A13 (void);
// 0x0000004F System.Void Script.Map.Spawner::Start()
extern void Spawner_Start_m03C737B976E3908947711644C9A7F83DAE335585 (void);
// 0x00000050 System.Void Script.Map.Spawner::Update()
extern void Spawner_Update_m6DDDF7293967DCABBE1AA3BD7822047EC6CDBAC9 (void);
// 0x00000051 System.Void Script.Map.Spawner::Spawn()
extern void Spawner_Spawn_m04CCC86C5F5C3191376D7C37D63EA61685D94059 (void);
// 0x00000052 System.Void Script.Map.Spawner::.ctor()
extern void Spawner__ctor_m01E343F3FE121E9DEBABBE6E33281F89BF33930C (void);
// 0x00000053 System.Void Script.Enemy.Enemy::Start()
extern void Enemy_Start_m7A80DACA3BD97BCEFA962C6BC1B7072E6C41C63E (void);
// 0x00000054 System.Void Script.Enemy.Enemy::Update()
extern void Enemy_Update_mCE1C54D9179962C47BBB32BEDAE6EF9210984833 (void);
// 0x00000055 System.Void Script.Enemy.Enemy::MoveLeft()
extern void Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587 (void);
// 0x00000056 System.Void Script.Enemy.Enemy::MoveRight()
extern void Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E (void);
// 0x00000057 System.Void Script.Enemy.Enemy::MoveUp()
extern void Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461 (void);
// 0x00000058 System.Void Script.Enemy.Enemy::MoveDown()
extern void Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929 (void);
// 0x00000059 System.Void Script.Enemy.Enemy::Move()
extern void Enemy_Move_mDAC2F4D048EF5160585A1828E23D3CB92070BACD (void);
// 0x0000005A System.Void Script.Enemy.Enemy::TakeHit(System.Single)
extern void Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41 (void);
// 0x0000005B System.Void Script.Enemy.Enemy::Kill()
extern void Enemy_Kill_m15BEB3F4C334EF8225B15CC5C0077AF4A12D1A0B (void);
// 0x0000005C System.Void Script.Enemy.Enemy::Playsound(UnityEngine.AudioClip)
extern void Enemy_Playsound_m76AEE5C1AFDDEE80543917074E15791887E3E04F (void);
// 0x0000005D System.Void Script.Enemy.Enemy::ChangeMoveDir(UnityEngine.Vector3)
extern void Enemy_ChangeMoveDir_mC518ABB9948229742CDE15B74703B9490B7446E1 (void);
// 0x0000005E System.Void Script.Enemy.Enemy::Dead()
extern void Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C (void);
// 0x0000005F System.Void Script.Enemy.Enemy::FlipMoveDir()
extern void Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4 (void);
// 0x00000060 System.Void Script.Enemy.Enemy::Reflect()
extern void Enemy_Reflect_mB4E0614BD3165AF81B6A41A981B94BE441638629 (void);
// 0x00000061 System.Void Script.Enemy.Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Enemy_OnTriggerEnter2D_m06CBB2F737E9AD5016794003BC224EEC981BD0CA (void);
// 0x00000062 System.Void Script.Enemy.Enemy::ResetSpeedMult()
extern void Enemy_ResetSpeedMult_m419D80A63CFBA625803D6CEF30E00A397E7EE3FB (void);
// 0x00000063 System.Void Script.Enemy.Enemy::ResetTimeCtrl()
extern void Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156 (void);
// 0x00000064 System.Void Script.Enemy.Enemy::TimeCtrl()
extern void Enemy_TimeCtrl_mB4099D838835079D46AC4623CB8EEFE7AD6BAFFE (void);
// 0x00000065 System.Void Script.Enemy.Enemy::.ctor()
extern void Enemy__ctor_mD4DB24A77D2C2CB43C49F78F6529BAD6BE209523 (void);
// 0x00000066 System.Void Script.Enemy.IDamageable::TakeHit(System.Single)
// 0x00000067 System.Void Script.Enemy.ITurnDirection::ChangeMoveDir(UnityEngine.Vector3)
// 0x00000068 System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip)
extern void BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB (void);
// 0x00000069 System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip,System.Single)
extern void BaseCatChar_PlaySound_mEE619B3AC3B30E3EF1662D35568D1D6EB0B5B3DE (void);
// 0x0000006A System.Void Script.Character.BaseCatChar::OnMouseDown()
extern void BaseCatChar_OnMouseDown_m146DA045E35E7B491D25474995023F3E3C36FC8C (void);
// 0x0000006B System.Void Script.Character.BaseCatChar::OnMouseOver()
extern void BaseCatChar_OnMouseOver_m99DC651023EB7E8E1A4B9C685A0B037056D7CFAB (void);
// 0x0000006C System.Void Script.Character.BaseCatChar::OnMouseExit()
extern void BaseCatChar_OnMouseExit_mA499643F058B20D312A784080DE3AE3FA3041DF8 (void);
// 0x0000006D System.Void Script.Character.BaseCatChar::Sale()
extern void BaseCatChar_Sale_m6522FB1264AF59864A3C036862AF942B58546A7E (void);
// 0x0000006E System.Void Script.Character.BaseCatChar::OnDestroy()
extern void BaseCatChar_OnDestroy_m279698C4FACBDC8B48F423B7235B18557B1AAF8E (void);
// 0x0000006F System.Void Script.Character.BaseCatChar::Dead()
extern void BaseCatChar_Dead_mD1AFD0172EDA931DCB582B8A15DDAABA0F6A6DB7 (void);
// 0x00000070 System.Void Script.Character.BaseCatChar::.ctor()
extern void BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2 (void);
// 0x00000071 System.Void Script.Character.CatFire::Start()
extern void CatFire_Start_mAD268507DF199959584A11E7B58A208537931D37 (void);
// 0x00000072 System.Void Script.Character.CatFire::Attack(UnityEngine.Vector3)
extern void CatFire_Attack_m7A3717FD3FEB9C7740C813E7BCEC5A25E35DB5BE (void);
// 0x00000073 System.Void Script.Character.CatFire::.ctor()
extern void CatFire__ctor_m7C4BD2A2208600182772D0139B1BC272D8BE1EED (void);
// 0x00000074 System.Void Script.Character.CatMelee::Start()
extern void CatMelee_Start_mBF4CCF71EA5C83D00037C36055EA1F3A1D4F3D8C (void);
// 0x00000075 System.Void Script.Character.CatMelee::Update()
extern void CatMelee_Update_m49A31FE0C95BBE5A47F71937AC9BABE440F37596 (void);
// 0x00000076 System.Void Script.Character.CatMelee::CoolDownCtrl()
extern void CatMelee_CoolDownCtrl_m1E86186A97E010905E575D3113111246DE46A49D (void);
// 0x00000077 System.Void Script.Character.CatMelee::ResetCoolDownTime()
extern void CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78 (void);
// 0x00000078 System.Void Script.Character.CatMelee::StopAttack()
extern void CatMelee_StopAttack_m3B1DC4C3B791A1C9447F52DBD2740BC7BB1B6F1C (void);
// 0x00000079 System.Void Script.Character.CatMelee::Attack(UnityEngine.Vector3)
extern void CatMelee_Attack_m842B5023CE89B1923BC4B5DDEA768850C287E1A7 (void);
// 0x0000007A System.Void Script.Character.CatMelee::.ctor()
extern void CatMelee__ctor_m0D8A250217E8B9F387C4E5B1E817BA12A3CDA7B1 (void);
// 0x0000007B System.Void Script.Character.CatShield::Start()
extern void CatShield_Start_m51B4B394294A2EB0C10B7DC43F36B80D8E458E99 (void);
// 0x0000007C System.Void Script.Character.CatShield::TakeHit(System.Single)
extern void CatShield_TakeHit_m190ABC57FD09DBBBD24F172F2474883521BD74DB (void);
// 0x0000007D System.Void Script.Character.CatShield::StopTakeHit()
extern void CatShield_StopTakeHit_m046D02C37C82EF20951038E5F46FCB0854F08527 (void);
// 0x0000007E System.Void Script.Character.CatShield::Dead()
extern void CatShield_Dead_mABB1C0639062E66C49F5507D081676814FE23CAB (void);
// 0x0000007F System.Void Script.Character.CatShield::OnDestroy()
extern void CatShield_OnDestroy_m60C2795705F2CD68B2CB51B3416CEA6BDFDF0F81 (void);
// 0x00000080 System.Void Script.Character.CatShield::.ctor()
extern void CatShield__ctor_m43348309BD5DDBFB81D78555219E57593C2840F9 (void);
// 0x00000081 System.Void Script.Character.Hit.HitDamage::Start()
extern void HitDamage_Start_m029FDD15023CD5C6338E1CD6E7E8527E014A685F (void);
// 0x00000082 System.Void Script.Character.Hit.HitDamage::Destroy()
extern void HitDamage_Destroy_m03E81A19B5F3FCD5D9D4A004B47BE4FC29099CDC (void);
// 0x00000083 System.Void Script.Character.Hit.HitDamage::Update()
extern void HitDamage_Update_m31C09B2A062F93546465188264850678905EC0ED (void);
// 0x00000084 System.Void Script.Character.Hit.HitDamage::.ctor()
extern void HitDamage__ctor_m8543E46E21A1181502B34E95DC26096CB75A1294 (void);
// 0x00000085 System.Void Script.Character.Hit.HitDamageBullet::Start()
extern void HitDamageBullet_Start_m508247ADABE1094304D65D16388EDA06DDF5B215 (void);
// 0x00000086 System.Void Script.Character.Hit.HitDamageBullet::Update()
extern void HitDamageBullet_Update_m09441BB2E1761BEA6D9554C6BC9F2DBCD4751ABB (void);
// 0x00000087 System.Void Script.Character.Hit.HitDamageBullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HitDamageBullet_OnTriggerEnter2D_mAB6CA0EAC2978E765F261ABC6424B7FD3C3A38E8 (void);
// 0x00000088 System.Void Script.Character.Hit.HitDamageBullet::MoveTo(UnityEngine.GameObject)
extern void HitDamageBullet_MoveTo_mE6F6507496D23D4B3AB8D088CA6ED595909D21C6 (void);
// 0x00000089 System.Void Script.Character.Hit.HitDamageBullet::.ctor()
extern void HitDamageBullet__ctor_mF679D7ACFBDDC133C04164B0A8938F2835130DEA (void);
// 0x0000008A System.Void Script.Character.DamageArea.CharDamAreaFire::Start()
extern void CharDamAreaFire_Start_m69D2987102E6EE54495D51A3B78207ADBEF430D7 (void);
// 0x0000008B System.Void Script.Character.DamageArea.CharDamAreaFire::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CharDamAreaFire_OnTriggerStay2D_mFE1807824F7A4DC29C2B506CE5F456F29DC9D2C2 (void);
// 0x0000008C System.Void Script.Character.DamageArea.CharDamAreaFire::.ctor()
extern void CharDamAreaFire__ctor_m2B1A5FA185153797CD8811559BC5E6BEC6EA5E93 (void);
// 0x0000008D System.Void Script.Character.DamageArea.CharDamAreaShield::Start()
extern void CharDamAreaShield_Start_mEF4E19E58C0F9663A3410B5530DA0B94A8A662D8 (void);
// 0x0000008E System.Void Script.Character.DamageArea.CharDamAreaShield::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharDamAreaShield_OnTriggerEnter2D_m788D7669FDE2D51C6D3AA688CF459A7C0CCBC722 (void);
// 0x0000008F System.Void Script.Character.DamageArea.CharDamAreaShield::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CharDamAreaShield_OnTriggerStay2D_m51D3070F4F6E1879E6023E654E8E7B3035E7F0C8 (void);
// 0x00000090 System.Void Script.Character.DamageArea.CharDamAreaShield::.ctor()
extern void CharDamAreaShield__ctor_mFD4F110A1EA0276EA4682F362BF26589C9630127 (void);
// 0x00000091 System.Void Script.Character.DamageArea.CharacterDamageArea::Start()
extern void CharacterDamageArea_Start_mBCBDC010DE85B692B4FC94ABFEFF0989893B1D03 (void);
// 0x00000092 System.Void Script.Character.DamageArea.CharacterDamageArea::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterDamageArea_OnTriggerEnter2D_m326CB07289E875F3261BAB56EBEF657A464B0B3D (void);
// 0x00000093 System.Void Script.Character.DamageArea.CharacterDamageArea::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CharacterDamageArea_OnTriggerStay2D_m30BCB02FE4A7C16DB3468C26648CB2014174778F (void);
// 0x00000094 System.Void Script.Character.DamageArea.CharacterDamageArea::.ctor()
extern void CharacterDamageArea__ctor_mE6D4FC9021555B4999BC756B1CE1BA32CD64763C (void);
// 0x00000095 System.Void Script.CardScript.CardPlace::Awake()
extern void CardPlace_Awake_mB506C6BA06840036A4F6911F289FA7C32E746B03 (void);
// 0x00000096 System.Void Script.CardScript.CardPlace::Start()
extern void CardPlace_Start_m9A09AFE26C638B02FD13B455255911A141CCF688 (void);
// 0x00000097 System.Void Script.CardScript.CardPlace::Update()
extern void CardPlace_Update_m133A6E7711FBE0870CAA303C5AC05B53CF9B8A66 (void);
// 0x00000098 System.Void Script.CardScript.CardPlace::PlaceablePoint()
extern void CardPlace_PlaceablePoint_m876ADBC20542700119A161AB67426F68A68B9F50 (void);
// 0x00000099 System.Void Script.CardScript.CardPlace::Control()
extern void CardPlace_Control_m1F11D20D5023A43AFC131D84F72B6C66586DD078 (void);
// 0x0000009A System.Void Script.CardScript.CardPlace::OnTriggerEnter(UnityEngine.Collider)
extern void CardPlace_OnTriggerEnter_m6DE8C38EA41FDBB98005EBFAE816F1D790FB6AA9 (void);
// 0x0000009B System.Void Script.CardScript.CardPlace::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CardPlace_OnTriggerStay2D_m1E13AA5AF50D8A7DBB80B9247C47B8ABB5352FD5 (void);
// 0x0000009C System.Void Script.CardScript.CardPlace::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CardPlace_OnTriggerExit2D_m4913FD2C7F120F5D3384C27EC766CD3E7CB216CC (void);
// 0x0000009D System.Void Script.CardScript.CardPlace::OnDestroy()
extern void CardPlace_OnDestroy_m240CBD93208BA19EC3062B95767901816C3CE90A (void);
// 0x0000009E System.Void Script.CardScript.CardPlace::.ctor()
extern void CardPlace__ctor_m8B5EBD86BC0C1CDCF08A3933DB9E8E9F082070DB (void);
// 0x0000009F System.Void Script.CardScript.CardPlaceField::Awake()
extern void CardPlaceField_Awake_m0B101E734C546DFFABF3D6BC772E445C69C70E80 (void);
// 0x000000A0 System.Void Script.CardScript.CardPlaceField::Start()
extern void CardPlaceField_Start_mCEF5B7DFB101BF33649B2FB82480A0EA719C74AB (void);
// 0x000000A1 System.Void Script.CardScript.CardPlaceField::Update()
extern void CardPlaceField_Update_m58998057080D4C142A838FB4080A8284BD0AF9BC (void);
// 0x000000A2 System.Void Script.CardScript.CardPlaceField::PlaceablePoint()
extern void CardPlaceField_PlaceablePoint_m7F2678AC45B0DFD59A0B82B088D5AB1EA14A7C24 (void);
// 0x000000A3 System.Void Script.CardScript.CardPlaceField::Control()
extern void CardPlaceField_Control_m2A5DB9567CA0C4658F3835B0F66A3D9AA32C079A (void);
// 0x000000A4 System.Void Script.CardScript.CardPlaceField::OnTriggerEnter(UnityEngine.Collider)
extern void CardPlaceField_OnTriggerEnter_m6F8355779EDB12BC0300D7D1EB18BBE6A3567E72 (void);
// 0x000000A5 System.Void Script.CardScript.CardPlaceField::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CardPlaceField_OnTriggerStay2D_mF2295F593F933F9A7E897EF0FAB2C6EF16A6E55A (void);
// 0x000000A6 System.Void Script.CardScript.CardPlaceField::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CardPlaceField_OnTriggerExit2D_mB25654277398C5EA5969D25BD82B1932475B1C47 (void);
// 0x000000A7 System.Void Script.CardScript.CardPlaceField::OnDestroy()
extern void CardPlaceField_OnDestroy_m672F64D215A1658859D02EBCF0A98E1517E93A8D (void);
// 0x000000A8 System.Void Script.CardScript.CardPlaceField::.ctor()
extern void CardPlaceField__ctor_mADD90E41A41C23C4A93EAB7AD8951414CEB85C7B (void);
static Il2CppMethodPointer s_methodPointers[168] = 
{
	CatCard_Start_mC241CE6745BC8FD82EC3AE235240B2A174789C5E,
	CatCard_PickCard_m5FA425EBE1D19368F1BEF2AF0AEBFEB628C3B120,
	CatCard__ctor_mA8DB16F69268E6DFF800BFBDC376F21E81FD860C,
	TurnDirPoint_Start_m0DD9DDD9AB0645EEBCE50C32708737663701F2AA,
	TurnDirPoint_OnTriggerEnter2D_mAB3275449BFFC89D2363BAD76749066A7596D8CB,
	TurnDirPoint__ctor_m65113737944B0D2DFF22C8E643B13E9A3E0C3757,
	PauseBotton_Pause_m7EACECE09CBE86C991D2E9E09B10EA654D2A3025,
	PauseBotton__ctor_mF41F008D32F714DFABAFF5B445B275F3E847851F,
	DontDestoryOnLoad_Start_mF1ECB298CFB40940BA1402634E7553F04350C70D,
	DontDestoryOnLoad_Update_mDBA5350D6EECD070FBFA9A32867F7F736AE23BC9,
	DontDestoryOnLoad__ctor_m684596B308FCF9D95C6C3FE10FE0799361B41C7F,
	GameManager_StartPlayGame_m7BBCED5DB288F74C0CD49F9F943D854C381E23E0,
	GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A,
	GameManager_Win_mEE312032712783936AAC9B0EF0CED7BDB345C973,
	GameManager_DestroyAllEnemy_m3A3AFCD2EC6A82A49FAC55F527ACC6F9E91910EE,
	GameManager_DestroyAllUnit_m6BDE5206F29B58B5724BD590AA77466D6B545E97,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_Click_mF51E03F3AF95EA64F1B3AED7315C42BE91F1A4C9,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	MainGameScene_Start_mD147BCC62885A9DD85408C33589F0260E2731B49,
	MainGameScene__ctor_mB30666241CA8E49251D2F6B83FDC3A35D010515A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CatCharacterManager_Start_mF7570321D75F115A87A64CDE915E44377B325C59,
	CatCharacterManager_Update_m0FC05CDF51AD4A32FA7F94BE52BF50D73CAC3D8B,
	CatCharacterManager__ctor_m9D51FC19D75231C828EF2DCF9EFDAAAA49908BCC,
	EnemyList_Start_m95FEA6B01F792722641A11943E58996FCD08E8D0,
	EnemyList_Update_mFDE57CAB9C507EC2482B7AE4F32288A6EEC4169F,
	EnemyList__ctor_m5EE9B37FB4E3A01813C4F201AFF7D9FC85715634,
	LoadManager_Start_mBEBED79E4C651C714EEB9A187492A6941210F606,
	LoadManager_Update_m5C6D8E807663A5938A7A442D8FC762F22ACC5933,
	LoadManager__ctor_m2D6202BAFF14E8DC356007DA86DE8EF58C6474B2,
	MissionManager_Start_mA02C8F3F85E450223FA4D69625B0572511A23275,
	MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09,
	MissionManager_EnemyScoreReset_m4CD561F1F4C88D55C232A092577228D956C4284C,
	MissionManager_EnemyScored_m7C68CEFFF5713951EB7533DF47E4C617E04142D9,
	MissionManager__ctor_mD4375AA12B01D60C54E5E6C0314EC9DF7175FAF7,
	WaveSystem_SetEnemyList_mB48BB6F64C31F9F83D7EB0DA2222BE53446FB3FC,
	WaveSystem__ctor_m9AAE748E210C416A9AE4D6580C8E5EDE49E91278,
	EnemyLeftDisplay_Start_m69CEFDCE50FC07642ABF22369424302680553E6D,
	EnemyLeftDisplay_Update_m080FE6FA1F785DC27F4DBDFAFF455ADC81E0D109,
	EnemyLeftDisplay__ctor_m8F2B4D422373983AD5DE7EB8008ECD41A0C8C6D5,
	HpBar_Start_mD11553FFD1EC893692B5F48F7139A6691257FD0F,
	HpBar_Update_mD7026F64E4A9A8E085372A76CFBF13AEEBDDC7FB,
	HpBar__ctor_mAE6ED973236E66EE76BCEBB8058A3697174ED037,
	MainMenu_Start_mFB01E91118351E6A180E4B35DD8208570B3423A9,
	MainMenu_ToMissionMenu_m4E0B4F72D691101344ED04BE27C17144BA284075,
	MainMenu_BackToMainMenu_m4A1EB4F827008EAAE312A42222C0DAB9F487E7CE,
	MainMenu_LoadMission_m49E22F2C0751DD492EFB1D927E1322C45530F5DB,
	MainMenu_ContinueGame_m3FB0CD0540008DC130E94148E91E748DF5ED436F,
	MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE,
	MainMenu_GameOverMenu_m1C3686D2523C552479A2FE8B38DD9C35754D16D2,
	MainMenu_ExitGame_mB79D76C015CA5ED89BEC1719CC6574065C77B1FE,
	MainMenu_ToSelectMission_m819C84188FCE9AAEFC66A4104C1ACBACCB50C5CB,
	MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898,
	MainMenu_RestartMission_m0587B90AEE8AC5E8868B75261CFAEA3F64ACF7D1,
	MainMenu__ctor_mBA722739613EF4F7A5503DD127B61987270B02A1,
	MoneyDisplay_Start_mDED1D862FEE3D5C64D1E923EA86047A8A993099F,
	MoneyDisplay_Update_m957EA6AF0036944C87FACD8546D25DCDFCEB6A21,
	MoneyDisplay__ctor_m2981E1A16DF68402A137D54AA91CE41AE0E19C38,
	ExitDoor_Start_m13891693B8E6565AAAB2C8BE7AFDF2CDEFE880F9,
	ExitDoor_OnTriggerEnter2D_mF52D19F36242CF772503CF35004E76C03805E371,
	ExitDoor_TakeHit_mC9D5DEAD44D641FD35E135CEA79308AD76EC897E,
	ExitDoor__ctor_mF889A857E4FA662218AF656AA3FA847F100048DC,
	Mission_Start_m5A6A792E9D8375F70EAF79C321BC046E3AC515EA,
	Mission_Update_mC912E95F3235165B5335C184845D9B8986FBB89D,
	Mission__ctor_mDB11C1F9D50E4E4C77631DDD2CE4E265DCE6F804,
	Money_Update_m09ECD0552CCBBBDD222F24A0F19DD77956267C75,
	Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F,
	Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16,
	Money__ctor_m7E96BF51569536AE65915183551B6FCD4FB0D2D8,
	PlaceableArea__ctor_m9A60E0D3BDEA111825956D4726867E016224C5C8,
	PlaceableAreaOnField__ctor_m4BF8F23BF29C82BA7B527A6C5BF8B7AB01415FF1,
	SnapPoint__ctor_m66C9037DF5523C44D203EAE05060D7CA456F9FEA,
	SnapPointField__ctor_m1AFC9AE0173236234CB51345D78FE9F7A7A54A13,
	Spawner_Start_m03C737B976E3908947711644C9A7F83DAE335585,
	Spawner_Update_m6DDDF7293967DCABBE1AA3BD7822047EC6CDBAC9,
	Spawner_Spawn_m04CCC86C5F5C3191376D7C37D63EA61685D94059,
	Spawner__ctor_m01E343F3FE121E9DEBABBE6E33281F89BF33930C,
	Enemy_Start_m7A80DACA3BD97BCEFA962C6BC1B7072E6C41C63E,
	Enemy_Update_mCE1C54D9179962C47BBB32BEDAE6EF9210984833,
	Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587,
	Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E,
	Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461,
	Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929,
	Enemy_Move_mDAC2F4D048EF5160585A1828E23D3CB92070BACD,
	Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41,
	Enemy_Kill_m15BEB3F4C334EF8225B15CC5C0077AF4A12D1A0B,
	Enemy_Playsound_m76AEE5C1AFDDEE80543917074E15791887E3E04F,
	Enemy_ChangeMoveDir_mC518ABB9948229742CDE15B74703B9490B7446E1,
	Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C,
	Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4,
	Enemy_Reflect_mB4E0614BD3165AF81B6A41A981B94BE441638629,
	Enemy_OnTriggerEnter2D_m06CBB2F737E9AD5016794003BC224EEC981BD0CA,
	Enemy_ResetSpeedMult_m419D80A63CFBA625803D6CEF30E00A397E7EE3FB,
	Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156,
	Enemy_TimeCtrl_mB4099D838835079D46AC4623CB8EEFE7AD6BAFFE,
	Enemy__ctor_mD4DB24A77D2C2CB43C49F78F6529BAD6BE209523,
	NULL,
	NULL,
	BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB,
	BaseCatChar_PlaySound_mEE619B3AC3B30E3EF1662D35568D1D6EB0B5B3DE,
	BaseCatChar_OnMouseDown_m146DA045E35E7B491D25474995023F3E3C36FC8C,
	BaseCatChar_OnMouseOver_m99DC651023EB7E8E1A4B9C685A0B037056D7CFAB,
	BaseCatChar_OnMouseExit_mA499643F058B20D312A784080DE3AE3FA3041DF8,
	BaseCatChar_Sale_m6522FB1264AF59864A3C036862AF942B58546A7E,
	BaseCatChar_OnDestroy_m279698C4FACBDC8B48F423B7235B18557B1AAF8E,
	BaseCatChar_Dead_mD1AFD0172EDA931DCB582B8A15DDAABA0F6A6DB7,
	BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2,
	CatFire_Start_mAD268507DF199959584A11E7B58A208537931D37,
	CatFire_Attack_m7A3717FD3FEB9C7740C813E7BCEC5A25E35DB5BE,
	CatFire__ctor_m7C4BD2A2208600182772D0139B1BC272D8BE1EED,
	CatMelee_Start_mBF4CCF71EA5C83D00037C36055EA1F3A1D4F3D8C,
	CatMelee_Update_m49A31FE0C95BBE5A47F71937AC9BABE440F37596,
	CatMelee_CoolDownCtrl_m1E86186A97E010905E575D3113111246DE46A49D,
	CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78,
	CatMelee_StopAttack_m3B1DC4C3B791A1C9447F52DBD2740BC7BB1B6F1C,
	CatMelee_Attack_m842B5023CE89B1923BC4B5DDEA768850C287E1A7,
	CatMelee__ctor_m0D8A250217E8B9F387C4E5B1E817BA12A3CDA7B1,
	CatShield_Start_m51B4B394294A2EB0C10B7DC43F36B80D8E458E99,
	CatShield_TakeHit_m190ABC57FD09DBBBD24F172F2474883521BD74DB,
	CatShield_StopTakeHit_m046D02C37C82EF20951038E5F46FCB0854F08527,
	CatShield_Dead_mABB1C0639062E66C49F5507D081676814FE23CAB,
	CatShield_OnDestroy_m60C2795705F2CD68B2CB51B3416CEA6BDFDF0F81,
	CatShield__ctor_m43348309BD5DDBFB81D78555219E57593C2840F9,
	HitDamage_Start_m029FDD15023CD5C6338E1CD6E7E8527E014A685F,
	HitDamage_Destroy_m03E81A19B5F3FCD5D9D4A004B47BE4FC29099CDC,
	HitDamage_Update_m31C09B2A062F93546465188264850678905EC0ED,
	HitDamage__ctor_m8543E46E21A1181502B34E95DC26096CB75A1294,
	HitDamageBullet_Start_m508247ADABE1094304D65D16388EDA06DDF5B215,
	HitDamageBullet_Update_m09441BB2E1761BEA6D9554C6BC9F2DBCD4751ABB,
	HitDamageBullet_OnTriggerEnter2D_mAB6CA0EAC2978E765F261ABC6424B7FD3C3A38E8,
	HitDamageBullet_MoveTo_mE6F6507496D23D4B3AB8D088CA6ED595909D21C6,
	HitDamageBullet__ctor_mF679D7ACFBDDC133C04164B0A8938F2835130DEA,
	CharDamAreaFire_Start_m69D2987102E6EE54495D51A3B78207ADBEF430D7,
	CharDamAreaFire_OnTriggerStay2D_mFE1807824F7A4DC29C2B506CE5F456F29DC9D2C2,
	CharDamAreaFire__ctor_m2B1A5FA185153797CD8811559BC5E6BEC6EA5E93,
	CharDamAreaShield_Start_mEF4E19E58C0F9663A3410B5530DA0B94A8A662D8,
	CharDamAreaShield_OnTriggerEnter2D_m788D7669FDE2D51C6D3AA688CF459A7C0CCBC722,
	CharDamAreaShield_OnTriggerStay2D_m51D3070F4F6E1879E6023E654E8E7B3035E7F0C8,
	CharDamAreaShield__ctor_mFD4F110A1EA0276EA4682F362BF26589C9630127,
	CharacterDamageArea_Start_mBCBDC010DE85B692B4FC94ABFEFF0989893B1D03,
	CharacterDamageArea_OnTriggerEnter2D_m326CB07289E875F3261BAB56EBEF657A464B0B3D,
	CharacterDamageArea_OnTriggerStay2D_m30BCB02FE4A7C16DB3468C26648CB2014174778F,
	CharacterDamageArea__ctor_mE6D4FC9021555B4999BC756B1CE1BA32CD64763C,
	CardPlace_Awake_mB506C6BA06840036A4F6911F289FA7C32E746B03,
	CardPlace_Start_m9A09AFE26C638B02FD13B455255911A141CCF688,
	CardPlace_Update_m133A6E7711FBE0870CAA303C5AC05B53CF9B8A66,
	CardPlace_PlaceablePoint_m876ADBC20542700119A161AB67426F68A68B9F50,
	CardPlace_Control_m1F11D20D5023A43AFC131D84F72B6C66586DD078,
	CardPlace_OnTriggerEnter_m6DE8C38EA41FDBB98005EBFAE816F1D790FB6AA9,
	CardPlace_OnTriggerStay2D_m1E13AA5AF50D8A7DBB80B9247C47B8ABB5352FD5,
	CardPlace_OnTriggerExit2D_m4913FD2C7F120F5D3384C27EC766CD3E7CB216CC,
	CardPlace_OnDestroy_m240CBD93208BA19EC3062B95767901816C3CE90A,
	CardPlace__ctor_m8B5EBD86BC0C1CDCF08A3933DB9E8E9F082070DB,
	CardPlaceField_Awake_m0B101E734C546DFFABF3D6BC772E445C69C70E80,
	CardPlaceField_Start_mCEF5B7DFB101BF33649B2FB82480A0EA719C74AB,
	CardPlaceField_Update_m58998057080D4C142A838FB4080A8284BD0AF9BC,
	CardPlaceField_PlaceablePoint_m7F2678AC45B0DFD59A0B82B088D5AB1EA14A7C24,
	CardPlaceField_Control_m2A5DB9567CA0C4658F3835B0F66A3D9AA32C079A,
	CardPlaceField_OnTriggerEnter_m6F8355779EDB12BC0300D7D1EB18BBE6A3567E72,
	CardPlaceField_OnTriggerStay2D_mF2295F593F933F9A7E897EF0FAB2C6EF16A6E55A,
	CardPlaceField_OnTriggerExit2D_mB25654277398C5EA5969D25BD82B1932475B1C47,
	CardPlaceField_OnDestroy_m672F64D215A1658859D02EBCF0A98E1517E93A8D,
	CardPlaceField__ctor_mADD90E41A41C23C4A93EAB7AD8951414CEB85C7B,
};
static const int32_t s_InvokerIndices[168] = 
{
	1129,
	1129,
	1129,
	1129,
	970,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	-1,
	-1,
	-1,
	-1,
	-1,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	961,
	1129,
	624,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	970,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	970,
	988,
	1129,
	1129,
	1129,
	1129,
	1129,
	961,
	961,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	988,
	1129,
	970,
	997,
	1129,
	1129,
	1129,
	970,
	1129,
	1129,
	1129,
	1129,
	988,
	997,
	970,
	630,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	997,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	997,
	1129,
	1129,
	988,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	970,
	970,
	1129,
	1129,
	970,
	1129,
	1129,
	970,
	970,
	1129,
	1129,
	970,
	970,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	970,
	970,
	970,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	1129,
	970,
	970,
	970,
	1129,
	1129,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000008, { 0, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 1084 },
	{ (Il2CppRGCTXDataType)1, 191 },
	{ (Il2CppRGCTXDataType)2, 191 },
	{ (Il2CppRGCTXDataType)3, 5625 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	168,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
