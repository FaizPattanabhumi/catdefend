﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// MonoSingleton`1<Script.Utility.CatCharacterManager>
struct MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8;
// MonoSingleton`1<GameManager>
struct MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E;
// MonoSingleton`1<Script.UI.MainMenu>
struct MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A;
// MonoSingleton`1<Script.Utility.MissionManager>
struct MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02;
// MonoSingleton`1<Script.Map.Money>
struct MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0;
// MonoSingleton`1<System.Object>
struct MonoSingleton_1_tCB0740E1A0FBE7F99B80A6DFD8CD948FFAECF3D2;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Animation
struct Animation_t8C4FD9513E57F59E8737AD03938AAAF9EFF2F0D8;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// Script.Character.BaseCatChar
struct BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// Script.CardScript.CardPlace
struct CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F;
// Script.CardScript.CardPlaceField
struct CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B;
// CatCard
struct CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6;
// Script.Utility.CatCharacterManager
struct CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E;
// Script.Character.CatFire
struct CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5;
// Script.Character.CatMelee
struct CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241;
// Script.Character.CatShield
struct CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E;
// Script.Character.DamageArea.CharDamAreaFire
struct CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60;
// Script.Character.DamageArea.CharDamAreaShield
struct CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89;
// Script.Character.DamageArea.CharacterDamageArea
struct CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// DontDestoryOnLoad
struct DontDestoryOnLoad_t9542A540955D6487F0538CCD634920D37129DEF1;
// Script.Enemy.Enemy
struct Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE;
// Script.UI.EnemyLeftDisplay
struct EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5;
// Script.Utility.EnemyList
struct EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8;
// Script.Map.ExitDoor
struct ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// Script.Character.Hit.HitDamage
struct HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD;
// Script.Character.Hit.HitDamageBullet
struct HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD;
// Script.UI.HpBar
struct HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// Script.Utility.LoadManager
struct LoadManager_t166D027CB2E4C1D1C702ECB86A25F71C8D834D91;
// MainGameScene
struct MainGameScene_tCF91F64FDE86C11BE551FC8E1B32A9DB67761DB7;
// Script.UI.MainMenu
struct MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// Script.Map.Mission
struct Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51;
// Script.Utility.MissionManager
struct MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC;
// Script.Map.Money
struct Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4;
// Script.UI.MoneyDisplay
struct MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// PauseBotton
struct PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF;
// Script.Map.PlaceableArea
struct PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD;
// Script.Map.PlaceableAreaOnField
struct PlaceableAreaOnField_tCD6B5829E4DD09D90E5B138E48ECBD87E9510A7B;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// Script.Map.SnapPoint
struct SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD;
// Script.Map.SnapPointField
struct SnapPointField_t7D8385A372032D364F54551D890308C67872B555;
// Script.Map.Spawner
struct Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// TurnDirPoint
struct TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Script.Utility.WaveSystem
struct WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305;
IL2CPP_EXTERN_C String_t* _stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94;
IL2CPP_EXTERN_C String_t* _stringLiteral3260331AF5DA53ABC7CA7BAF659CF8D9FC93DEC7;
IL2CPP_EXTERN_C String_t* _stringLiteral38DB4D59B61D0EAF394CBD85005F176490748E0E;
IL2CPP_EXTERN_C String_t* _stringLiteral42EA15876C33949F765AE605B9DFCCDC4E9BE311;
IL2CPP_EXTERN_C String_t* _stringLiteral4FFE5C21412E3B6B065B9F27BCD4E95FED8914A8;
IL2CPP_EXTERN_C String_t* _stringLiteral5092D61AADF5226DE949D8590D5BE79F89AF8618;
IL2CPP_EXTERN_C String_t* _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4;
IL2CPP_EXTERN_C String_t* _stringLiteral586825A85BD3ABBC2F448E88E81F8DFBBE3A1EC4;
IL2CPP_EXTERN_C String_t* _stringLiteral594DC94B47F9965C6B26B6579E1E586C3151254E;
IL2CPP_EXTERN_C String_t* _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2;
IL2CPP_EXTERN_C String_t* _stringLiteralA149D0A68B12DEDE1102E7900A7FCA36A18F064D;
IL2CPP_EXTERN_C String_t* _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF;
IL2CPP_EXTERN_C String_t* _stringLiteralB6D795FBD58CC7592D955A219374339A323801A9;
IL2CPP_EXTERN_C String_t* _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_mE27BBB6D62139C6AABDF5EFE59917F8844F43C23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_m2641F635A37E4ABBD38D51ADC3F77F630EA3BC1D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSlider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_mD501A27463515FA99A5A93A10E37F913696D20C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_mD32E8767D2CBEC7A80462E7CB2D68D0CA04EC35C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_m71F728D48A4F2F4F8D42693E252648983D2542B3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisHitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_m9423AEAA8918C1D985BAAC26FC23B35CAC5EA9EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1__ctor_m016CCA4BA41740634C0C66CCE7D7FF46B3ADD926_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1__ctor_m1C4CAEFF311D0D70F5075E5936B923F272D2787E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1__ctor_m20F70D38D8139FF5AB0E960955FFF561F93D0710_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1__ctor_m5B8CECC53ED806262D94900D9F9E760C09839370_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1__ctor_m714FDB833649A832A7113836EC9F35AF4ADFF368_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Mathematics.float4
struct float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883 
{
public:
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};


// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86, ___m_completeCallback_1)); }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_tC1348BEB2C677FD60E4B65764CA3A1CAFF6DFB31 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_completeCallback_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.SceneManagement.LoadSceneMode
struct LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Ray
struct Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Origin_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Direction_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// Unity.Mathematics.quaternion
struct quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F 
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.quaternion::value
	float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883  ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F, ___value_0)); }
	inline float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883  get_value_0() const { return ___value_0; }
	inline float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float4_t866F9B9B0DB07AE8949D4ACE92A0A84C88769883  value)
	{
		___value_0 = value;
	}
};

struct quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields
{
public:
	// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::identity
	quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  ___identity_1;

public:
	inline static int32_t get_offset_of_identity_1() { return static_cast<int32_t>(offsetof(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields, ___identity_1)); }
	inline quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  get_identity_1() const { return ___identity_1; }
	inline quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F * get_address_of_identity_1() { return &___identity_1; }
	inline void set_identity_1(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  value)
	{
		___identity_1 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/Direction
struct Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// MonoSingleton`1<Script.Utility.CatCharacterManager>
struct MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_StaticFields
{
public:
	// System.Boolean MonoSingleton`1::m_ShuttingDown
	bool ___m_ShuttingDown_4;
	// System.Object MonoSingleton`1::m_Lock
	RuntimeObject * ___m_Lock_5;
	// T MonoSingleton`1::m_Instance
	CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * ___m_Instance_6;

public:
	inline static int32_t get_offset_of_m_ShuttingDown_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_StaticFields, ___m_ShuttingDown_4)); }
	inline bool get_m_ShuttingDown_4() const { return ___m_ShuttingDown_4; }
	inline bool* get_address_of_m_ShuttingDown_4() { return &___m_ShuttingDown_4; }
	inline void set_m_ShuttingDown_4(bool value)
	{
		___m_ShuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_m_Lock_5() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_StaticFields, ___m_Lock_5)); }
	inline RuntimeObject * get_m_Lock_5() const { return ___m_Lock_5; }
	inline RuntimeObject ** get_address_of_m_Lock_5() { return &___m_Lock_5; }
	inline void set_m_Lock_5(RuntimeObject * value)
	{
		___m_Lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Lock_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Instance_6() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_StaticFields, ___m_Instance_6)); }
	inline CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * get_m_Instance_6() const { return ___m_Instance_6; }
	inline CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E ** get_address_of_m_Instance_6() { return &___m_Instance_6; }
	inline void set_m_Instance_6(CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * value)
	{
		___m_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_6), (void*)value);
	}
};


// MonoSingleton`1<GameManager>
struct MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_StaticFields
{
public:
	// System.Boolean MonoSingleton`1::m_ShuttingDown
	bool ___m_ShuttingDown_4;
	// System.Object MonoSingleton`1::m_Lock
	RuntimeObject * ___m_Lock_5;
	// T MonoSingleton`1::m_Instance
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ___m_Instance_6;

public:
	inline static int32_t get_offset_of_m_ShuttingDown_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_StaticFields, ___m_ShuttingDown_4)); }
	inline bool get_m_ShuttingDown_4() const { return ___m_ShuttingDown_4; }
	inline bool* get_address_of_m_ShuttingDown_4() { return &___m_ShuttingDown_4; }
	inline void set_m_ShuttingDown_4(bool value)
	{
		___m_ShuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_m_Lock_5() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_StaticFields, ___m_Lock_5)); }
	inline RuntimeObject * get_m_Lock_5() const { return ___m_Lock_5; }
	inline RuntimeObject ** get_address_of_m_Lock_5() { return &___m_Lock_5; }
	inline void set_m_Lock_5(RuntimeObject * value)
	{
		___m_Lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Lock_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Instance_6() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_StaticFields, ___m_Instance_6)); }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * get_m_Instance_6() const { return ___m_Instance_6; }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 ** get_address_of_m_Instance_6() { return &___m_Instance_6; }
	inline void set_m_Instance_6(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * value)
	{
		___m_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_6), (void*)value);
	}
};


// MonoSingleton`1<Script.UI.MainMenu>
struct MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_StaticFields
{
public:
	// System.Boolean MonoSingleton`1::m_ShuttingDown
	bool ___m_ShuttingDown_4;
	// System.Object MonoSingleton`1::m_Lock
	RuntimeObject * ___m_Lock_5;
	// T MonoSingleton`1::m_Instance
	MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * ___m_Instance_6;

public:
	inline static int32_t get_offset_of_m_ShuttingDown_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_StaticFields, ___m_ShuttingDown_4)); }
	inline bool get_m_ShuttingDown_4() const { return ___m_ShuttingDown_4; }
	inline bool* get_address_of_m_ShuttingDown_4() { return &___m_ShuttingDown_4; }
	inline void set_m_ShuttingDown_4(bool value)
	{
		___m_ShuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_m_Lock_5() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_StaticFields, ___m_Lock_5)); }
	inline RuntimeObject * get_m_Lock_5() const { return ___m_Lock_5; }
	inline RuntimeObject ** get_address_of_m_Lock_5() { return &___m_Lock_5; }
	inline void set_m_Lock_5(RuntimeObject * value)
	{
		___m_Lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Lock_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Instance_6() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_StaticFields, ___m_Instance_6)); }
	inline MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * get_m_Instance_6() const { return ___m_Instance_6; }
	inline MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 ** get_address_of_m_Instance_6() { return &___m_Instance_6; }
	inline void set_m_Instance_6(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * value)
	{
		___m_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_6), (void*)value);
	}
};


// MonoSingleton`1<Script.Utility.MissionManager>
struct MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_StaticFields
{
public:
	// System.Boolean MonoSingleton`1::m_ShuttingDown
	bool ___m_ShuttingDown_4;
	// System.Object MonoSingleton`1::m_Lock
	RuntimeObject * ___m_Lock_5;
	// T MonoSingleton`1::m_Instance
	MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * ___m_Instance_6;

public:
	inline static int32_t get_offset_of_m_ShuttingDown_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_StaticFields, ___m_ShuttingDown_4)); }
	inline bool get_m_ShuttingDown_4() const { return ___m_ShuttingDown_4; }
	inline bool* get_address_of_m_ShuttingDown_4() { return &___m_ShuttingDown_4; }
	inline void set_m_ShuttingDown_4(bool value)
	{
		___m_ShuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_m_Lock_5() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_StaticFields, ___m_Lock_5)); }
	inline RuntimeObject * get_m_Lock_5() const { return ___m_Lock_5; }
	inline RuntimeObject ** get_address_of_m_Lock_5() { return &___m_Lock_5; }
	inline void set_m_Lock_5(RuntimeObject * value)
	{
		___m_Lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Lock_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Instance_6() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_StaticFields, ___m_Instance_6)); }
	inline MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * get_m_Instance_6() const { return ___m_Instance_6; }
	inline MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC ** get_address_of_m_Instance_6() { return &___m_Instance_6; }
	inline void set_m_Instance_6(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * value)
	{
		___m_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_6), (void*)value);
	}
};


// MonoSingleton`1<Script.Map.Money>
struct MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_StaticFields
{
public:
	// System.Boolean MonoSingleton`1::m_ShuttingDown
	bool ___m_ShuttingDown_4;
	// System.Object MonoSingleton`1::m_Lock
	RuntimeObject * ___m_Lock_5;
	// T MonoSingleton`1::m_Instance
	Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * ___m_Instance_6;

public:
	inline static int32_t get_offset_of_m_ShuttingDown_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_StaticFields, ___m_ShuttingDown_4)); }
	inline bool get_m_ShuttingDown_4() const { return ___m_ShuttingDown_4; }
	inline bool* get_address_of_m_ShuttingDown_4() { return &___m_ShuttingDown_4; }
	inline void set_m_ShuttingDown_4(bool value)
	{
		___m_ShuttingDown_4 = value;
	}

	inline static int32_t get_offset_of_m_Lock_5() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_StaticFields, ___m_Lock_5)); }
	inline RuntimeObject * get_m_Lock_5() const { return ___m_Lock_5; }
	inline RuntimeObject ** get_address_of_m_Lock_5() { return &___m_Lock_5; }
	inline void set_m_Lock_5(RuntimeObject * value)
	{
		___m_Lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Lock_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Instance_6() { return static_cast<int32_t>(offsetof(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_StaticFields, ___m_Instance_6)); }
	inline Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * get_m_Instance_6() const { return ___m_Instance_6; }
	inline Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 ** get_address_of_m_Instance_6() { return &___m_Instance_6; }
	inline void set_m_Instance_6(Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * value)
	{
		___m_Instance_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_6), (void*)value);
	}
};


// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// Script.Character.BaseCatChar
struct BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Script.Character.BaseCatChar::price
	int32_t ___price_4;
	// System.Boolean Script.Character.BaseCatChar::isPlaced
	bool ___isPlaced_5;
	// UnityEngine.Animator Script.Character.BaseCatChar::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_6;
	// UnityEngine.AudioSource Script.Character.BaseCatChar::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_7;
	// Script.Map.SnapPoint Script.Character.BaseCatChar::snappoint
	SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * ___snappoint_8;
	// Script.Map.SnapPointField Script.Character.BaseCatChar::snapPointField
	SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * ___snapPointField_9;
	// UnityEngine.SpriteRenderer Script.Character.BaseCatChar::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_10;
	// UnityEngine.Color Script.Character.BaseCatChar::normalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___normalColor_11;
	// UnityEngine.Color Script.Character.BaseCatChar::selectColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___selectColor_12;

public:
	inline static int32_t get_offset_of_price_4() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___price_4)); }
	inline int32_t get_price_4() const { return ___price_4; }
	inline int32_t* get_address_of_price_4() { return &___price_4; }
	inline void set_price_4(int32_t value)
	{
		___price_4 = value;
	}

	inline static int32_t get_offset_of_isPlaced_5() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___isPlaced_5)); }
	inline bool get_isPlaced_5() const { return ___isPlaced_5; }
	inline bool* get_address_of_isPlaced_5() { return &___isPlaced_5; }
	inline void set_isPlaced_5(bool value)
	{
		___isPlaced_5 = value;
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___animator_6)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_6() const { return ___animator_6; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_6), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_7() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___audioSource_7)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_7() const { return ___audioSource_7; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_7() { return &___audioSource_7; }
	inline void set_audioSource_7(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_7), (void*)value);
	}

	inline static int32_t get_offset_of_snappoint_8() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___snappoint_8)); }
	inline SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * get_snappoint_8() const { return ___snappoint_8; }
	inline SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD ** get_address_of_snappoint_8() { return &___snappoint_8; }
	inline void set_snappoint_8(SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * value)
	{
		___snappoint_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snappoint_8), (void*)value);
	}

	inline static int32_t get_offset_of_snapPointField_9() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___snapPointField_9)); }
	inline SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * get_snapPointField_9() const { return ___snapPointField_9; }
	inline SnapPointField_t7D8385A372032D364F54551D890308C67872B555 ** get_address_of_snapPointField_9() { return &___snapPointField_9; }
	inline void set_snapPointField_9(SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * value)
	{
		___snapPointField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snapPointField_9), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRenderer_10() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___spriteRenderer_10)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_10() const { return ___spriteRenderer_10; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_10() { return &___spriteRenderer_10; }
	inline void set_spriteRenderer_10(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_10), (void*)value);
	}

	inline static int32_t get_offset_of_normalColor_11() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___normalColor_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_normalColor_11() const { return ___normalColor_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_normalColor_11() { return &___normalColor_11; }
	inline void set_normalColor_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___normalColor_11 = value;
	}

	inline static int32_t get_offset_of_selectColor_12() { return static_cast<int32_t>(offsetof(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4, ___selectColor_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_selectColor_12() const { return ___selectColor_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_selectColor_12() { return &___selectColor_12; }
	inline void set_selectColor_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___selectColor_12 = value;
	}
};


// Script.CardScript.CardPlace
struct CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Script.CardScript.CardPlace::mousePosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mousePosition_4;
	// Script.Character.BaseCatChar Script.CardScript.CardPlace::baseCatChar
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * ___baseCatChar_5;
	// Script.Map.SnapPoint Script.CardScript.CardPlace::snappoint
	SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * ___snappoint_6;
	// UnityEngine.SpriteRenderer Script.CardScript.CardPlace::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_7;
	// UnityEngine.Color Script.CardScript.CardPlace::placeableColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___placeableColor_8;
	// UnityEngine.Color Script.CardScript.CardPlace::unplaceableColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___unplaceableColor_9;
	// Script.Map.PlaceableArea Script.CardScript.CardPlace::placeableArea
	PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * ___placeableArea_10;
	// UnityEngine.AudioSource Script.CardScript.CardPlace::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_11;
	// System.Boolean Script.CardScript.CardPlace::placeable
	bool ___placeable_12;
	// System.Boolean Script.CardScript.CardPlace::isPlace
	bool ___isPlace_13;

public:
	inline static int32_t get_offset_of_mousePosition_4() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___mousePosition_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mousePosition_4() const { return ___mousePosition_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mousePosition_4() { return &___mousePosition_4; }
	inline void set_mousePosition_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mousePosition_4 = value;
	}

	inline static int32_t get_offset_of_baseCatChar_5() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___baseCatChar_5)); }
	inline BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * get_baseCatChar_5() const { return ___baseCatChar_5; }
	inline BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 ** get_address_of_baseCatChar_5() { return &___baseCatChar_5; }
	inline void set_baseCatChar_5(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * value)
	{
		___baseCatChar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseCatChar_5), (void*)value);
	}

	inline static int32_t get_offset_of_snappoint_6() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___snappoint_6)); }
	inline SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * get_snappoint_6() const { return ___snappoint_6; }
	inline SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD ** get_address_of_snappoint_6() { return &___snappoint_6; }
	inline void set_snappoint_6(SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * value)
	{
		___snappoint_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snappoint_6), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRenderer_7() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___spriteRenderer_7)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_7() const { return ___spriteRenderer_7; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_7() { return &___spriteRenderer_7; }
	inline void set_spriteRenderer_7(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of_placeableColor_8() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___placeableColor_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_placeableColor_8() const { return ___placeableColor_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_placeableColor_8() { return &___placeableColor_8; }
	inline void set_placeableColor_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___placeableColor_8 = value;
	}

	inline static int32_t get_offset_of_unplaceableColor_9() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___unplaceableColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_unplaceableColor_9() const { return ___unplaceableColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_unplaceableColor_9() { return &___unplaceableColor_9; }
	inline void set_unplaceableColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___unplaceableColor_9 = value;
	}

	inline static int32_t get_offset_of_placeableArea_10() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___placeableArea_10)); }
	inline PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * get_placeableArea_10() const { return ___placeableArea_10; }
	inline PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD ** get_address_of_placeableArea_10() { return &___placeableArea_10; }
	inline void set_placeableArea_10(PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * value)
	{
		___placeableArea_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placeableArea_10), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_11() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___audioSource_11)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_11() const { return ___audioSource_11; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_11() { return &___audioSource_11; }
	inline void set_audioSource_11(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_11), (void*)value);
	}

	inline static int32_t get_offset_of_placeable_12() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___placeable_12)); }
	inline bool get_placeable_12() const { return ___placeable_12; }
	inline bool* get_address_of_placeable_12() { return &___placeable_12; }
	inline void set_placeable_12(bool value)
	{
		___placeable_12 = value;
	}

	inline static int32_t get_offset_of_isPlace_13() { return static_cast<int32_t>(offsetof(CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F, ___isPlace_13)); }
	inline bool get_isPlace_13() const { return ___isPlace_13; }
	inline bool* get_address_of_isPlace_13() { return &___isPlace_13; }
	inline void set_isPlace_13(bool value)
	{
		___isPlace_13 = value;
	}
};


// Script.CardScript.CardPlaceField
struct CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Script.CardScript.CardPlaceField::mousePosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___mousePosition_4;
	// Script.Character.BaseCatChar Script.CardScript.CardPlaceField::baseCatChar
	BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * ___baseCatChar_5;
	// Script.Map.SnapPointField Script.CardScript.CardPlaceField::snapPointField
	SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * ___snapPointField_6;
	// UnityEngine.SpriteRenderer Script.CardScript.CardPlaceField::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_7;
	// UnityEngine.Color Script.CardScript.CardPlaceField::placeableColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___placeableColor_8;
	// UnityEngine.Color Script.CardScript.CardPlaceField::unplaceableColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___unplaceableColor_9;
	// Script.Map.PlaceableArea Script.CardScript.CardPlaceField::placeableArea
	PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * ___placeableArea_10;
	// UnityEngine.AudioSource Script.CardScript.CardPlaceField::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_11;
	// System.Boolean Script.CardScript.CardPlaceField::placeable
	bool ___placeable_12;
	// System.Boolean Script.CardScript.CardPlaceField::isPlace
	bool ___isPlace_13;

public:
	inline static int32_t get_offset_of_mousePosition_4() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___mousePosition_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_mousePosition_4() const { return ___mousePosition_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_mousePosition_4() { return &___mousePosition_4; }
	inline void set_mousePosition_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___mousePosition_4 = value;
	}

	inline static int32_t get_offset_of_baseCatChar_5() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___baseCatChar_5)); }
	inline BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * get_baseCatChar_5() const { return ___baseCatChar_5; }
	inline BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 ** get_address_of_baseCatChar_5() { return &___baseCatChar_5; }
	inline void set_baseCatChar_5(BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * value)
	{
		___baseCatChar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseCatChar_5), (void*)value);
	}

	inline static int32_t get_offset_of_snapPointField_6() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___snapPointField_6)); }
	inline SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * get_snapPointField_6() const { return ___snapPointField_6; }
	inline SnapPointField_t7D8385A372032D364F54551D890308C67872B555 ** get_address_of_snapPointField_6() { return &___snapPointField_6; }
	inline void set_snapPointField_6(SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * value)
	{
		___snapPointField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___snapPointField_6), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRenderer_7() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___spriteRenderer_7)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_7() const { return ___spriteRenderer_7; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_7() { return &___spriteRenderer_7; }
	inline void set_spriteRenderer_7(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of_placeableColor_8() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___placeableColor_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_placeableColor_8() const { return ___placeableColor_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_placeableColor_8() { return &___placeableColor_8; }
	inline void set_placeableColor_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___placeableColor_8 = value;
	}

	inline static int32_t get_offset_of_unplaceableColor_9() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___unplaceableColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_unplaceableColor_9() const { return ___unplaceableColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_unplaceableColor_9() { return &___unplaceableColor_9; }
	inline void set_unplaceableColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___unplaceableColor_9 = value;
	}

	inline static int32_t get_offset_of_placeableArea_10() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___placeableArea_10)); }
	inline PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * get_placeableArea_10() const { return ___placeableArea_10; }
	inline PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD ** get_address_of_placeableArea_10() { return &___placeableArea_10; }
	inline void set_placeableArea_10(PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * value)
	{
		___placeableArea_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placeableArea_10), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_11() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___audioSource_11)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_11() const { return ___audioSource_11; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_11() { return &___audioSource_11; }
	inline void set_audioSource_11(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_11), (void*)value);
	}

	inline static int32_t get_offset_of_placeable_12() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___placeable_12)); }
	inline bool get_placeable_12() const { return ___placeable_12; }
	inline bool* get_address_of_placeable_12() { return &___placeable_12; }
	inline void set_placeable_12(bool value)
	{
		___placeable_12 = value;
	}

	inline static int32_t get_offset_of_isPlace_13() { return static_cast<int32_t>(offsetof(CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B, ___isPlace_13)); }
	inline bool get_isPlace_13() const { return ___isPlace_13; }
	inline bool* get_address_of_isPlace_13() { return &___isPlace_13; }
	inline void set_isPlace_13(bool value)
	{
		___isPlace_13 = value;
	}
};


// CatCard
struct CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CatCard::character
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___character_4;
	// UnityEngine.AudioSource CatCard::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_5;

public:
	inline static int32_t get_offset_of_character_4() { return static_cast<int32_t>(offsetof(CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6, ___character_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_character_4() const { return ___character_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_character_4() { return &___character_4; }
	inline void set_character_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___character_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___character_4), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_5() { return static_cast<int32_t>(offsetof(CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6, ___audioSource_5)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_5() const { return ___audioSource_5; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_5() { return &___audioSource_5; }
	inline void set_audioSource_5(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_5), (void*)value);
	}
};


// Script.Character.DamageArea.CharDamAreaFire
struct CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.CircleCollider2D Script.Character.DamageArea.CharDamAreaFire::hitBox
	CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * ___hitBox_4;
	// Script.Character.CatFire Script.Character.DamageArea.CharDamAreaFire::catFire
	CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * ___catFire_5;
	// System.Single Script.Character.DamageArea.CharDamAreaFire::radius
	float ___radius_6;
	// Script.Enemy.Enemy Script.Character.DamageArea.CharDamAreaFire::target
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___target_7;

public:
	inline static int32_t get_offset_of_hitBox_4() { return static_cast<int32_t>(offsetof(CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60, ___hitBox_4)); }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * get_hitBox_4() const { return ___hitBox_4; }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 ** get_address_of_hitBox_4() { return &___hitBox_4; }
	inline void set_hitBox_4(CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * value)
	{
		___hitBox_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitBox_4), (void*)value);
	}

	inline static int32_t get_offset_of_catFire_5() { return static_cast<int32_t>(offsetof(CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60, ___catFire_5)); }
	inline CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * get_catFire_5() const { return ___catFire_5; }
	inline CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 ** get_address_of_catFire_5() { return &___catFire_5; }
	inline void set_catFire_5(CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * value)
	{
		___catFire_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___catFire_5), (void*)value);
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60, ___target_7)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_target_7() const { return ___target_7; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}
};


// Script.Character.DamageArea.CharDamAreaShield
struct CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Script.Character.CatShield Script.Character.DamageArea.CharDamAreaShield::catShield
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * ___catShield_4;
	// Script.Enemy.Enemy Script.Character.DamageArea.CharDamAreaShield::target
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___target_5;

public:
	inline static int32_t get_offset_of_catShield_4() { return static_cast<int32_t>(offsetof(CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89, ___catShield_4)); }
	inline CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * get_catShield_4() const { return ___catShield_4; }
	inline CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E ** get_address_of_catShield_4() { return &___catShield_4; }
	inline void set_catShield_4(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * value)
	{
		___catShield_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___catShield_4), (void*)value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89, ___target_5)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_target_5() const { return ___target_5; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_5), (void*)value);
	}
};


// Script.Character.DamageArea.CharacterDamageArea
struct CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.CircleCollider2D Script.Character.DamageArea.CharacterDamageArea::hitBox
	CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * ___hitBox_4;
	// Script.Character.CatMelee Script.Character.DamageArea.CharacterDamageArea::catMelee
	CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * ___catMelee_5;
	// System.Single Script.Character.DamageArea.CharacterDamageArea::radius
	float ___radius_6;
	// Script.Enemy.Enemy Script.Character.DamageArea.CharacterDamageArea::target
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___target_7;

public:
	inline static int32_t get_offset_of_hitBox_4() { return static_cast<int32_t>(offsetof(CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE, ___hitBox_4)); }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * get_hitBox_4() const { return ___hitBox_4; }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 ** get_address_of_hitBox_4() { return &___hitBox_4; }
	inline void set_hitBox_4(CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * value)
	{
		___hitBox_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitBox_4), (void*)value);
	}

	inline static int32_t get_offset_of_catMelee_5() { return static_cast<int32_t>(offsetof(CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE, ___catMelee_5)); }
	inline CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * get_catMelee_5() const { return ___catMelee_5; }
	inline CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 ** get_address_of_catMelee_5() { return &___catMelee_5; }
	inline void set_catMelee_5(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * value)
	{
		___catMelee_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___catMelee_5), (void*)value);
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE, ___target_7)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_target_7() const { return ___target_7; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}
};


// UnityEngine.CircleCollider2D
struct CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913  : public Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722
{
public:

public:
};


// DontDestoryOnLoad
struct DontDestoryOnLoad_t9542A540955D6487F0538CCD634920D37129DEF1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Script.Enemy.Enemy
struct Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Script.Enemy.Enemy::hp
	float ___hp_4;
	// System.Int32 Script.Enemy.Enemy::moneyDrop
	int32_t ___moneyDrop_5;
	// UnityEngine.Vector3 Script.Enemy.Enemy::moveDir
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___moveDir_6;
	// System.Single Script.Enemy.Enemy::speed
	float ___speed_7;
	// System.Single Script.Enemy.Enemy::multSpeed
	float ___multSpeed_8;
	// System.Single Script.Enemy.Enemy::damage
	float ___damage_9;
	// UnityEngine.Rigidbody2D Script.Enemy.Enemy::rb
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___rb_10;
	// UnityEngine.GameObject Script.Enemy.Enemy::hitType
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hitType_11;
	// Script.Character.CatShield Script.Enemy.Enemy::target
	CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * ___target_12;
	// Script.Map.ExitDoor Script.Enemy.Enemy::exitDoor
	ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * ___exitDoor_13;
	// System.String Script.Enemy.Enemy::moveStatus
	String_t* ___moveStatus_14;
	// System.Single Script.Enemy.Enemy::cdt
	float ___cdt_15;
	// System.Single Script.Enemy.Enemy::cdtMax
	float ___cdtMax_16;
	// System.Single Script.Enemy.Enemy::knockBackForce
	float ___knockBackForce_17;
	// UnityEngine.AudioSource Script.Enemy.Enemy::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_18;
	// UnityEngine.AudioClip Script.Enemy.Enemy::aud_Takehit
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___aud_Takehit_19;

public:
	inline static int32_t get_offset_of_hp_4() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___hp_4)); }
	inline float get_hp_4() const { return ___hp_4; }
	inline float* get_address_of_hp_4() { return &___hp_4; }
	inline void set_hp_4(float value)
	{
		___hp_4 = value;
	}

	inline static int32_t get_offset_of_moneyDrop_5() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___moneyDrop_5)); }
	inline int32_t get_moneyDrop_5() const { return ___moneyDrop_5; }
	inline int32_t* get_address_of_moneyDrop_5() { return &___moneyDrop_5; }
	inline void set_moneyDrop_5(int32_t value)
	{
		___moneyDrop_5 = value;
	}

	inline static int32_t get_offset_of_moveDir_6() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___moveDir_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_moveDir_6() const { return ___moveDir_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_moveDir_6() { return &___moveDir_6; }
	inline void set_moveDir_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___moveDir_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}

	inline static int32_t get_offset_of_multSpeed_8() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___multSpeed_8)); }
	inline float get_multSpeed_8() const { return ___multSpeed_8; }
	inline float* get_address_of_multSpeed_8() { return &___multSpeed_8; }
	inline void set_multSpeed_8(float value)
	{
		___multSpeed_8 = value;
	}

	inline static int32_t get_offset_of_damage_9() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___damage_9)); }
	inline float get_damage_9() const { return ___damage_9; }
	inline float* get_address_of_damage_9() { return &___damage_9; }
	inline void set_damage_9(float value)
	{
		___damage_9 = value;
	}

	inline static int32_t get_offset_of_rb_10() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___rb_10)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_rb_10() const { return ___rb_10; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_rb_10() { return &___rb_10; }
	inline void set_rb_10(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___rb_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_10), (void*)value);
	}

	inline static int32_t get_offset_of_hitType_11() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___hitType_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hitType_11() const { return ___hitType_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hitType_11() { return &___hitType_11; }
	inline void set_hitType_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hitType_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitType_11), (void*)value);
	}

	inline static int32_t get_offset_of_target_12() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___target_12)); }
	inline CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * get_target_12() const { return ___target_12; }
	inline CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E ** get_address_of_target_12() { return &___target_12; }
	inline void set_target_12(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * value)
	{
		___target_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_12), (void*)value);
	}

	inline static int32_t get_offset_of_exitDoor_13() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___exitDoor_13)); }
	inline ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * get_exitDoor_13() const { return ___exitDoor_13; }
	inline ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E ** get_address_of_exitDoor_13() { return &___exitDoor_13; }
	inline void set_exitDoor_13(ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * value)
	{
		___exitDoor_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exitDoor_13), (void*)value);
	}

	inline static int32_t get_offset_of_moveStatus_14() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___moveStatus_14)); }
	inline String_t* get_moveStatus_14() const { return ___moveStatus_14; }
	inline String_t** get_address_of_moveStatus_14() { return &___moveStatus_14; }
	inline void set_moveStatus_14(String_t* value)
	{
		___moveStatus_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moveStatus_14), (void*)value);
	}

	inline static int32_t get_offset_of_cdt_15() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___cdt_15)); }
	inline float get_cdt_15() const { return ___cdt_15; }
	inline float* get_address_of_cdt_15() { return &___cdt_15; }
	inline void set_cdt_15(float value)
	{
		___cdt_15 = value;
	}

	inline static int32_t get_offset_of_cdtMax_16() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___cdtMax_16)); }
	inline float get_cdtMax_16() const { return ___cdtMax_16; }
	inline float* get_address_of_cdtMax_16() { return &___cdtMax_16; }
	inline void set_cdtMax_16(float value)
	{
		___cdtMax_16 = value;
	}

	inline static int32_t get_offset_of_knockBackForce_17() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___knockBackForce_17)); }
	inline float get_knockBackForce_17() const { return ___knockBackForce_17; }
	inline float* get_address_of_knockBackForce_17() { return &___knockBackForce_17; }
	inline void set_knockBackForce_17(float value)
	{
		___knockBackForce_17 = value;
	}

	inline static int32_t get_offset_of_audioSource_18() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___audioSource_18)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_18() const { return ___audioSource_18; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_18() { return &___audioSource_18; }
	inline void set_audioSource_18(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_18), (void*)value);
	}

	inline static int32_t get_offset_of_aud_Takehit_19() { return static_cast<int32_t>(offsetof(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE, ___aud_Takehit_19)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_aud_Takehit_19() const { return ___aud_Takehit_19; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_aud_Takehit_19() { return &___aud_Takehit_19; }
	inline void set_aud_Takehit_19(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___aud_Takehit_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aud_Takehit_19), (void*)value);
	}
};


// Script.UI.EnemyLeftDisplay
struct EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text Script.UI.EnemyLeftDisplay::enemyLeftText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___enemyLeftText_4;

public:
	inline static int32_t get_offset_of_enemyLeftText_4() { return static_cast<int32_t>(offsetof(EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5, ___enemyLeftText_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_enemyLeftText_4() const { return ___enemyLeftText_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_enemyLeftText_4() { return &___enemyLeftText_4; }
	inline void set_enemyLeftText_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___enemyLeftText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyLeftText_4), (void*)value);
	}
};


// Script.Utility.EnemyList
struct EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] Script.Utility.EnemyList::allEnemies
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___allEnemies_4;

public:
	inline static int32_t get_offset_of_allEnemies_4() { return static_cast<int32_t>(offsetof(EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8, ___allEnemies_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_allEnemies_4() const { return ___allEnemies_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_allEnemies_4() { return &___allEnemies_4; }
	inline void set_allEnemies_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___allEnemies_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allEnemies_4), (void*)value);
	}
};


// Script.Map.ExitDoor
struct ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Script.Map.ExitDoor::hpMax
	float ___hpMax_4;
	// System.Single Script.Map.ExitDoor::hp
	float ___hp_5;
	// Script.Enemy.Enemy Script.Map.ExitDoor::enemy
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___enemy_6;

public:
	inline static int32_t get_offset_of_hpMax_4() { return static_cast<int32_t>(offsetof(ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E, ___hpMax_4)); }
	inline float get_hpMax_4() const { return ___hpMax_4; }
	inline float* get_address_of_hpMax_4() { return &___hpMax_4; }
	inline void set_hpMax_4(float value)
	{
		___hpMax_4 = value;
	}

	inline static int32_t get_offset_of_hp_5() { return static_cast<int32_t>(offsetof(ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E, ___hp_5)); }
	inline float get_hp_5() const { return ___hp_5; }
	inline float* get_address_of_hp_5() { return &___hp_5; }
	inline void set_hp_5(float value)
	{
		___hp_5 = value;
	}

	inline static int32_t get_offset_of_enemy_6() { return static_cast<int32_t>(offsetof(ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E, ___enemy_6)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_enemy_6() const { return ___enemy_6; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_enemy_6() { return &___enemy_6; }
	inline void set_enemy_6(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___enemy_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemy_6), (void*)value);
	}
};


// Script.Character.Hit.HitDamage
struct HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Script.Character.Hit.HitDamage::timeToDestroy
	float ___timeToDestroy_4;

public:
	inline static int32_t get_offset_of_timeToDestroy_4() { return static_cast<int32_t>(offsetof(HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD, ___timeToDestroy_4)); }
	inline float get_timeToDestroy_4() const { return ___timeToDestroy_4; }
	inline float* get_address_of_timeToDestroy_4() { return &___timeToDestroy_4; }
	inline void set_timeToDestroy_4(float value)
	{
		___timeToDestroy_4 = value;
	}
};


// Script.UI.HpBar
struct HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Script.Map.ExitDoor Script.UI.HpBar::exitDoorSript
	ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * ___exitDoorSript_4;
	// UnityEngine.UI.Slider Script.UI.HpBar::hpSlider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___hpSlider_5;

public:
	inline static int32_t get_offset_of_exitDoorSript_4() { return static_cast<int32_t>(offsetof(HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB, ___exitDoorSript_4)); }
	inline ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * get_exitDoorSript_4() const { return ___exitDoorSript_4; }
	inline ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E ** get_address_of_exitDoorSript_4() { return &___exitDoorSript_4; }
	inline void set_exitDoorSript_4(ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * value)
	{
		___exitDoorSript_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exitDoorSript_4), (void*)value);
	}

	inline static int32_t get_offset_of_hpSlider_5() { return static_cast<int32_t>(offsetof(HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB, ___hpSlider_5)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_hpSlider_5() const { return ___hpSlider_5; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_hpSlider_5() { return &___hpSlider_5; }
	inline void set_hpSlider_5(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___hpSlider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hpSlider_5), (void*)value);
	}
};


// Script.Utility.LoadManager
struct LoadManager_t166D027CB2E4C1D1C702ECB86A25F71C8D834D91  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MainGameScene
struct MainGameScene_tCF91F64FDE86C11BE551FC8E1B32A9DB67761DB7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Script.Map.Mission
struct Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] Script.Map.Mission::enemyLists
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___enemyLists_4;
	// System.Int32 Script.Map.Mission::moneyStart
	int32_t ___moneyStart_5;

public:
	inline static int32_t get_offset_of_enemyLists_4() { return static_cast<int32_t>(offsetof(Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51, ___enemyLists_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_enemyLists_4() const { return ___enemyLists_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_enemyLists_4() { return &___enemyLists_4; }
	inline void set_enemyLists_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___enemyLists_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyLists_4), (void*)value);
	}

	inline static int32_t get_offset_of_moneyStart_5() { return static_cast<int32_t>(offsetof(Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51, ___moneyStart_5)); }
	inline int32_t get_moneyStart_5() const { return ___moneyStart_5; }
	inline int32_t* get_address_of_moneyStart_5() { return &___moneyStart_5; }
	inline void set_moneyStart_5(int32_t value)
	{
		___moneyStart_5 = value;
	}
};


// Script.UI.MoneyDisplay
struct MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text Script.UI.MoneyDisplay::moneyText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___moneyText_4;

public:
	inline static int32_t get_offset_of_moneyText_4() { return static_cast<int32_t>(offsetof(MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D, ___moneyText_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_moneyText_4() const { return ___moneyText_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_moneyText_4() { return &___moneyText_4; }
	inline void set_moneyText_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___moneyText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moneyText_4), (void*)value);
	}
};


// PauseBotton
struct PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource PauseBotton::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_4;

public:
	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF, ___audioSource_4)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_4), (void*)value);
	}
};


// Script.Map.PlaceableArea
struct PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Script.Map.PlaceableAreaOnField
struct PlaceableAreaOnField_tCD6B5829E4DD09D90E5B138E48ECBD87E9510A7B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Script.Map.SnapPoint
struct SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Script.Map.SnapPoint::isEmpty
	bool ___isEmpty_4;

public:
	inline static int32_t get_offset_of_isEmpty_4() { return static_cast<int32_t>(offsetof(SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD, ___isEmpty_4)); }
	inline bool get_isEmpty_4() const { return ___isEmpty_4; }
	inline bool* get_address_of_isEmpty_4() { return &___isEmpty_4; }
	inline void set_isEmpty_4(bool value)
	{
		___isEmpty_4 = value;
	}
};


// Script.Map.Spawner
struct Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Script.Map.Spawner::enemy
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___enemy_4;
	// System.Single Script.Map.Spawner::cooldown
	float ___cooldown_5;
	// System.Single Script.Map.Spawner::cooldownMax
	float ___cooldownMax_6;
	// Script.Utility.WaveSystem Script.Map.Spawner::waveSystem
	WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * ___waveSystem_7;
	// System.Int32 Script.Map.Spawner::enemyListsIndex
	int32_t ___enemyListsIndex_8;

public:
	inline static int32_t get_offset_of_enemy_4() { return static_cast<int32_t>(offsetof(Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C, ___enemy_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_enemy_4() const { return ___enemy_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_enemy_4() { return &___enemy_4; }
	inline void set_enemy_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___enemy_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemy_4), (void*)value);
	}

	inline static int32_t get_offset_of_cooldown_5() { return static_cast<int32_t>(offsetof(Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C, ___cooldown_5)); }
	inline float get_cooldown_5() const { return ___cooldown_5; }
	inline float* get_address_of_cooldown_5() { return &___cooldown_5; }
	inline void set_cooldown_5(float value)
	{
		___cooldown_5 = value;
	}

	inline static int32_t get_offset_of_cooldownMax_6() { return static_cast<int32_t>(offsetof(Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C, ___cooldownMax_6)); }
	inline float get_cooldownMax_6() const { return ___cooldownMax_6; }
	inline float* get_address_of_cooldownMax_6() { return &___cooldownMax_6; }
	inline void set_cooldownMax_6(float value)
	{
		___cooldownMax_6 = value;
	}

	inline static int32_t get_offset_of_waveSystem_7() { return static_cast<int32_t>(offsetof(Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C, ___waveSystem_7)); }
	inline WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * get_waveSystem_7() const { return ___waveSystem_7; }
	inline WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA ** get_address_of_waveSystem_7() { return &___waveSystem_7; }
	inline void set_waveSystem_7(WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * value)
	{
		___waveSystem_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waveSystem_7), (void*)value);
	}

	inline static int32_t get_offset_of_enemyListsIndex_8() { return static_cast<int32_t>(offsetof(Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C, ___enemyListsIndex_8)); }
	inline int32_t get_enemyListsIndex_8() const { return ___enemyListsIndex_8; }
	inline int32_t* get_address_of_enemyListsIndex_8() { return &___enemyListsIndex_8; }
	inline void set_enemyListsIndex_8(int32_t value)
	{
		___enemyListsIndex_8 = value;
	}
};


// TurnDirPoint
struct TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 TurnDirPoint::newDir
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newDir_4;
	// Script.Enemy.Enemy TurnDirPoint::enemyScript
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___enemyScript_5;
	// System.String TurnDirPoint::DirStatus
	String_t* ___DirStatus_6;

public:
	inline static int32_t get_offset_of_newDir_4() { return static_cast<int32_t>(offsetof(TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B, ___newDir_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_newDir_4() const { return ___newDir_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_newDir_4() { return &___newDir_4; }
	inline void set_newDir_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___newDir_4 = value;
	}

	inline static int32_t get_offset_of_enemyScript_5() { return static_cast<int32_t>(offsetof(TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B, ___enemyScript_5)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_enemyScript_5() const { return ___enemyScript_5; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_enemyScript_5() { return &___enemyScript_5; }
	inline void set_enemyScript_5(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___enemyScript_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyScript_5), (void*)value);
	}

	inline static int32_t get_offset_of_DirStatus_6() { return static_cast<int32_t>(offsetof(TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B, ___DirStatus_6)); }
	inline String_t* get_DirStatus_6() const { return ___DirStatus_6; }
	inline String_t** get_address_of_DirStatus_6() { return &___DirStatus_6; }
	inline void set_DirStatus_6(String_t* value)
	{
		___DirStatus_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DirStatus_6), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Script.Utility.WaveSystem
struct WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Script.Utility.EnemyList Script.Utility.WaveSystem::enemyList
	EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * ___enemyList_4;
	// UnityEngine.GameObject[] Script.Utility.WaveSystem::enemyLists
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___enemyLists_5;

public:
	inline static int32_t get_offset_of_enemyList_4() { return static_cast<int32_t>(offsetof(WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA, ___enemyList_4)); }
	inline EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * get_enemyList_4() const { return ___enemyList_4; }
	inline EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 ** get_address_of_enemyList_4() { return &___enemyList_4; }
	inline void set_enemyList_4(EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * value)
	{
		___enemyList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyList_4), (void*)value);
	}

	inline static int32_t get_offset_of_enemyLists_5() { return static_cast<int32_t>(offsetof(WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA, ___enemyLists_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_enemyLists_5() const { return ___enemyLists_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_enemyLists_5() { return &___enemyLists_5; }
	inline void set_enemyLists_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___enemyLists_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyLists_5), (void*)value);
	}
};


// Script.Utility.CatCharacterManager
struct CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E  : public MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8
{
public:
	// UnityEngine.GameObject Script.Utility.CatCharacterManager::cat1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cat1_7;
	// UnityEngine.GameObject Script.Utility.CatCharacterManager::enemy1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___enemy1_8;

public:
	inline static int32_t get_offset_of_cat1_7() { return static_cast<int32_t>(offsetof(CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E, ___cat1_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cat1_7() const { return ___cat1_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cat1_7() { return &___cat1_7; }
	inline void set_cat1_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cat1_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cat1_7), (void*)value);
	}

	inline static int32_t get_offset_of_enemy1_8() { return static_cast<int32_t>(offsetof(CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E, ___enemy1_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_enemy1_8() const { return ___enemy1_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_enemy1_8() { return &___enemy1_8; }
	inline void set_enemy1_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___enemy1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemy1_8), (void*)value);
	}
};


// Script.Character.CatMelee
struct CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241  : public BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4
{
public:
	// System.Single Script.Character.CatMelee::damage
	float ___damage_13;
	// System.Single Script.Character.CatMelee::lookRadius
	float ___lookRadius_14;
	// System.Single Script.Character.CatMelee::attackCoolDown
	float ___attackCoolDown_15;
	// System.Single Script.Character.CatMelee::attackCoolDownMax
	float ___attackCoolDownMax_16;
	// UnityEngine.GameObject Script.Character.CatMelee::hitType
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hitType_17;
	// UnityEngine.SpriteRenderer Script.Character.CatMelee::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_18;
	// Script.Enemy.Enemy Script.Character.CatMelee::target
	Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * ___target_19;
	// UnityEngine.Animation Script.Character.CatMelee::animation
	Animation_t8C4FD9513E57F59E8737AD03938AAAF9EFF2F0D8 * ___animation_20;
	// UnityEngine.AudioClip Script.Character.CatMelee::aud_Attack
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___aud_Attack_21;

public:
	inline static int32_t get_offset_of_damage_13() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___damage_13)); }
	inline float get_damage_13() const { return ___damage_13; }
	inline float* get_address_of_damage_13() { return &___damage_13; }
	inline void set_damage_13(float value)
	{
		___damage_13 = value;
	}

	inline static int32_t get_offset_of_lookRadius_14() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___lookRadius_14)); }
	inline float get_lookRadius_14() const { return ___lookRadius_14; }
	inline float* get_address_of_lookRadius_14() { return &___lookRadius_14; }
	inline void set_lookRadius_14(float value)
	{
		___lookRadius_14 = value;
	}

	inline static int32_t get_offset_of_attackCoolDown_15() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___attackCoolDown_15)); }
	inline float get_attackCoolDown_15() const { return ___attackCoolDown_15; }
	inline float* get_address_of_attackCoolDown_15() { return &___attackCoolDown_15; }
	inline void set_attackCoolDown_15(float value)
	{
		___attackCoolDown_15 = value;
	}

	inline static int32_t get_offset_of_attackCoolDownMax_16() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___attackCoolDownMax_16)); }
	inline float get_attackCoolDownMax_16() const { return ___attackCoolDownMax_16; }
	inline float* get_address_of_attackCoolDownMax_16() { return &___attackCoolDownMax_16; }
	inline void set_attackCoolDownMax_16(float value)
	{
		___attackCoolDownMax_16 = value;
	}

	inline static int32_t get_offset_of_hitType_17() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___hitType_17)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hitType_17() const { return ___hitType_17; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hitType_17() { return &___hitType_17; }
	inline void set_hitType_17(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hitType_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitType_17), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRenderer_18() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___spriteRenderer_18)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_18() const { return ___spriteRenderer_18; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_18() { return &___spriteRenderer_18; }
	inline void set_spriteRenderer_18(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_18), (void*)value);
	}

	inline static int32_t get_offset_of_target_19() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___target_19)); }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * get_target_19() const { return ___target_19; }
	inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE ** get_address_of_target_19() { return &___target_19; }
	inline void set_target_19(Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * value)
	{
		___target_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_19), (void*)value);
	}

	inline static int32_t get_offset_of_animation_20() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___animation_20)); }
	inline Animation_t8C4FD9513E57F59E8737AD03938AAAF9EFF2F0D8 * get_animation_20() const { return ___animation_20; }
	inline Animation_t8C4FD9513E57F59E8737AD03938AAAF9EFF2F0D8 ** get_address_of_animation_20() { return &___animation_20; }
	inline void set_animation_20(Animation_t8C4FD9513E57F59E8737AD03938AAAF9EFF2F0D8 * value)
	{
		___animation_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animation_20), (void*)value);
	}

	inline static int32_t get_offset_of_aud_Attack_21() { return static_cast<int32_t>(offsetof(CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241, ___aud_Attack_21)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_aud_Attack_21() const { return ___aud_Attack_21; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_aud_Attack_21() { return &___aud_Attack_21; }
	inline void set_aud_Attack_21(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___aud_Attack_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aud_Attack_21), (void*)value);
	}
};


// Script.Character.CatShield
struct CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E  : public BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4
{
public:
	// System.Single Script.Character.CatShield::hp
	float ___hp_13;
	// System.Single Script.Character.CatShield::damage
	float ___damage_14;
	// System.Single Script.Character.CatShield::lookRadius
	float ___lookRadius_15;
	// UnityEngine.GameObject Script.Character.CatShield::hitType
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___hitType_16;
	// UnityEngine.SpriteRenderer Script.Character.CatShield::spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRenderer_17;
	// UnityEngine.AudioClip Script.Character.CatShield::aud_Takehit
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___aud_Takehit_18;

public:
	inline static int32_t get_offset_of_hp_13() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___hp_13)); }
	inline float get_hp_13() const { return ___hp_13; }
	inline float* get_address_of_hp_13() { return &___hp_13; }
	inline void set_hp_13(float value)
	{
		___hp_13 = value;
	}

	inline static int32_t get_offset_of_damage_14() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___damage_14)); }
	inline float get_damage_14() const { return ___damage_14; }
	inline float* get_address_of_damage_14() { return &___damage_14; }
	inline void set_damage_14(float value)
	{
		___damage_14 = value;
	}

	inline static int32_t get_offset_of_lookRadius_15() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___lookRadius_15)); }
	inline float get_lookRadius_15() const { return ___lookRadius_15; }
	inline float* get_address_of_lookRadius_15() { return &___lookRadius_15; }
	inline void set_lookRadius_15(float value)
	{
		___lookRadius_15 = value;
	}

	inline static int32_t get_offset_of_hitType_16() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___hitType_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_hitType_16() const { return ___hitType_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_hitType_16() { return &___hitType_16; }
	inline void set_hitType_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___hitType_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitType_16), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRenderer_17() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___spriteRenderer_17)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRenderer_17() const { return ___spriteRenderer_17; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRenderer_17() { return &___spriteRenderer_17; }
	inline void set_spriteRenderer_17(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRenderer_17), (void*)value);
	}

	inline static int32_t get_offset_of_aud_Takehit_18() { return static_cast<int32_t>(offsetof(CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E, ___aud_Takehit_18)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_aud_Takehit_18() const { return ___aud_Takehit_18; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_aud_Takehit_18() { return &___aud_Takehit_18; }
	inline void set_aud_Takehit_18(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___aud_Takehit_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aud_Takehit_18), (void*)value);
	}
};


// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1  : public MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E
{
public:
	// System.Boolean GameManager::isCardOnHand
	bool ___isCardOnHand_7;
	// UnityEngine.GameObject GameManager::gameOverMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameOverMenu_8;
	// UnityEngine.GameObject GameManager::mainMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mainMenu_9;
	// UnityEngine.GameObject GameManager::missionMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___missionMenu_10;
	// UnityEngine.GameObject[] GameManager::allEnemys
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___allEnemys_11;
	// UnityEngine.GameObject[] GameManager::allCharacters
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___allCharacters_12;
	// UnityEngine.AudioSource GameManager::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_13;

public:
	inline static int32_t get_offset_of_isCardOnHand_7() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___isCardOnHand_7)); }
	inline bool get_isCardOnHand_7() const { return ___isCardOnHand_7; }
	inline bool* get_address_of_isCardOnHand_7() { return &___isCardOnHand_7; }
	inline void set_isCardOnHand_7(bool value)
	{
		___isCardOnHand_7 = value;
	}

	inline static int32_t get_offset_of_gameOverMenu_8() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___gameOverMenu_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameOverMenu_8() const { return ___gameOverMenu_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameOverMenu_8() { return &___gameOverMenu_8; }
	inline void set_gameOverMenu_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameOverMenu_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameOverMenu_8), (void*)value);
	}

	inline static int32_t get_offset_of_mainMenu_9() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___mainMenu_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mainMenu_9() const { return ___mainMenu_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mainMenu_9() { return &___mainMenu_9; }
	inline void set_mainMenu_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mainMenu_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainMenu_9), (void*)value);
	}

	inline static int32_t get_offset_of_missionMenu_10() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___missionMenu_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_missionMenu_10() const { return ___missionMenu_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_missionMenu_10() { return &___missionMenu_10; }
	inline void set_missionMenu_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___missionMenu_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___missionMenu_10), (void*)value);
	}

	inline static int32_t get_offset_of_allEnemys_11() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___allEnemys_11)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_allEnemys_11() const { return ___allEnemys_11; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_allEnemys_11() { return &___allEnemys_11; }
	inline void set_allEnemys_11(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___allEnemys_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allEnemys_11), (void*)value);
	}

	inline static int32_t get_offset_of_allCharacters_12() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___allCharacters_12)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_allCharacters_12() const { return ___allCharacters_12; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_allCharacters_12() { return &___allCharacters_12; }
	inline void set_allCharacters_12(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___allCharacters_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___allCharacters_12), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_13() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___audioSource_13)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_13() const { return ___audioSource_13; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_13() { return &___audioSource_13; }
	inline void set_audioSource_13(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_13), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// Script.Character.Hit.HitDamageBullet
struct HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD  : public HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD
{
public:
	// UnityEngine.GameObject Script.Character.Hit.HitDamageBullet::targetOBJ
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___targetOBJ_5;
	// Script.Character.CatFire Script.Character.Hit.HitDamageBullet::catFire
	CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * ___catFire_6;
	// System.Single Script.Character.Hit.HitDamageBullet::speed
	float ___speed_7;

public:
	inline static int32_t get_offset_of_targetOBJ_5() { return static_cast<int32_t>(offsetof(HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD, ___targetOBJ_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_targetOBJ_5() const { return ___targetOBJ_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_targetOBJ_5() { return &___targetOBJ_5; }
	inline void set_targetOBJ_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___targetOBJ_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetOBJ_5), (void*)value);
	}

	inline static int32_t get_offset_of_catFire_6() { return static_cast<int32_t>(offsetof(HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD, ___catFire_6)); }
	inline CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * get_catFire_6() const { return ___catFire_6; }
	inline CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 ** get_address_of_catFire_6() { return &___catFire_6; }
	inline void set_catFire_6(CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * value)
	{
		___catFire_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___catFire_6), (void*)value);
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}
};


// Script.UI.MainMenu
struct MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712  : public MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A
{
public:
	// UnityEngine.GameObject Script.UI.MainMenu::mainMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mainMenu_7;
	// UnityEngine.GameObject Script.UI.MainMenu::gameOverMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameOverMenu_8;
	// UnityEngine.GameObject Script.UI.MainMenu::missionMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___missionMenu_9;
	// UnityEngine.GameObject Script.UI.MainMenu::pauseMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___pauseMenu_10;
	// UnityEngine.AudioSource Script.UI.MainMenu::audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___audioSource_11;
	// System.String Script.UI.MainMenu::sceneNow
	String_t* ___sceneNow_12;

public:
	inline static int32_t get_offset_of_mainMenu_7() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___mainMenu_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mainMenu_7() const { return ___mainMenu_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mainMenu_7() { return &___mainMenu_7; }
	inline void set_mainMenu_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mainMenu_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainMenu_7), (void*)value);
	}

	inline static int32_t get_offset_of_gameOverMenu_8() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___gameOverMenu_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameOverMenu_8() const { return ___gameOverMenu_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameOverMenu_8() { return &___gameOverMenu_8; }
	inline void set_gameOverMenu_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameOverMenu_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameOverMenu_8), (void*)value);
	}

	inline static int32_t get_offset_of_missionMenu_9() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___missionMenu_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_missionMenu_9() const { return ___missionMenu_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_missionMenu_9() { return &___missionMenu_9; }
	inline void set_missionMenu_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___missionMenu_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___missionMenu_9), (void*)value);
	}

	inline static int32_t get_offset_of_pauseMenu_10() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___pauseMenu_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_pauseMenu_10() const { return ___pauseMenu_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_pauseMenu_10() { return &___pauseMenu_10; }
	inline void set_pauseMenu_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___pauseMenu_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pauseMenu_10), (void*)value);
	}

	inline static int32_t get_offset_of_audioSource_11() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___audioSource_11)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_audioSource_11() const { return ___audioSource_11; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_audioSource_11() { return &___audioSource_11; }
	inline void set_audioSource_11(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___audioSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioSource_11), (void*)value);
	}

	inline static int32_t get_offset_of_sceneNow_12() { return static_cast<int32_t>(offsetof(MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712, ___sceneNow_12)); }
	inline String_t* get_sceneNow_12() const { return ___sceneNow_12; }
	inline String_t** get_address_of_sceneNow_12() { return &___sceneNow_12; }
	inline void set_sceneNow_12(String_t* value)
	{
		___sceneNow_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneNow_12), (void*)value);
	}
};


// Script.Utility.MissionManager
struct MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC  : public MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02
{
public:
	// System.Int32 Script.Utility.MissionManager::allEnemyThisMission
	int32_t ___allEnemyThisMission_7;
	// System.Int32 Script.Utility.MissionManager::enemyLeft
	int32_t ___enemyLeft_8;
	// System.Int32 Script.Utility.MissionManager::wave
	int32_t ___wave_9;
	// Script.Utility.EnemyList Script.Utility.MissionManager::enemyList
	EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * ___enemyList_10;
	// Script.Utility.WaveSystem Script.Utility.MissionManager::waveSystem
	WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * ___waveSystem_11;

public:
	inline static int32_t get_offset_of_allEnemyThisMission_7() { return static_cast<int32_t>(offsetof(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC, ___allEnemyThisMission_7)); }
	inline int32_t get_allEnemyThisMission_7() const { return ___allEnemyThisMission_7; }
	inline int32_t* get_address_of_allEnemyThisMission_7() { return &___allEnemyThisMission_7; }
	inline void set_allEnemyThisMission_7(int32_t value)
	{
		___allEnemyThisMission_7 = value;
	}

	inline static int32_t get_offset_of_enemyLeft_8() { return static_cast<int32_t>(offsetof(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC, ___enemyLeft_8)); }
	inline int32_t get_enemyLeft_8() const { return ___enemyLeft_8; }
	inline int32_t* get_address_of_enemyLeft_8() { return &___enemyLeft_8; }
	inline void set_enemyLeft_8(int32_t value)
	{
		___enemyLeft_8 = value;
	}

	inline static int32_t get_offset_of_wave_9() { return static_cast<int32_t>(offsetof(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC, ___wave_9)); }
	inline int32_t get_wave_9() const { return ___wave_9; }
	inline int32_t* get_address_of_wave_9() { return &___wave_9; }
	inline void set_wave_9(int32_t value)
	{
		___wave_9 = value;
	}

	inline static int32_t get_offset_of_enemyList_10() { return static_cast<int32_t>(offsetof(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC, ___enemyList_10)); }
	inline EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * get_enemyList_10() const { return ___enemyList_10; }
	inline EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 ** get_address_of_enemyList_10() { return &___enemyList_10; }
	inline void set_enemyList_10(EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * value)
	{
		___enemyList_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyList_10), (void*)value);
	}

	inline static int32_t get_offset_of_waveSystem_11() { return static_cast<int32_t>(offsetof(MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC, ___waveSystem_11)); }
	inline WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * get_waveSystem_11() const { return ___waveSystem_11; }
	inline WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA ** get_address_of_waveSystem_11() { return &___waveSystem_11; }
	inline void set_waveSystem_11(WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * value)
	{
		___waveSystem_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waveSystem_11), (void*)value);
	}
};


// Script.Map.Money
struct Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4  : public MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0
{
public:
	// System.Int32 Script.Map.Money::money
	int32_t ___money_7;

public:
	inline static int32_t get_offset_of_money_7() { return static_cast<int32_t>(offsetof(Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4, ___money_7)); }
	inline int32_t get_money_7() const { return ___money_7; }
	inline int32_t* get_address_of_money_7() { return &___money_7; }
	inline void set_money_7(int32_t value)
	{
		___money_7 = value;
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// Script.Map.SnapPointField
struct SnapPointField_t7D8385A372032D364F54551D890308C67872B555  : public SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD
{
public:

public:
};


// Script.Character.CatFire
struct CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5  : public CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241
{
public:

public:
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillRect_20;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_21;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_22;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_23;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_24;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_25;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_26;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * ___m_OnValueChanged_27;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_FillImage_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_FillTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillContainerRect_30;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_HandleTransform_31;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleContainerRect_32;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_33;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_34;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_35;

public:
	inline static int32_t get_offset_of_m_FillRect_20() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillRect_20() const { return ___m_FillRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillRect_20() { return &___m_FillRect_20; }
	inline void set_m_FillRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_21() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleRect_21)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_21() const { return ___m_HandleRect_21; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_21() { return &___m_HandleRect_21; }
	inline void set_m_HandleRect_21(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_22() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Direction_22)); }
	inline int32_t get_m_Direction_22() const { return ___m_Direction_22; }
	inline int32_t* get_address_of_m_Direction_22() { return &___m_Direction_22; }
	inline void set_m_Direction_22(int32_t value)
	{
		___m_Direction_22 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_23() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MinValue_23)); }
	inline float get_m_MinValue_23() const { return ___m_MinValue_23; }
	inline float* get_address_of_m_MinValue_23() { return &___m_MinValue_23; }
	inline void set_m_MinValue_23(float value)
	{
		___m_MinValue_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_24() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MaxValue_24)); }
	inline float get_m_MaxValue_24() const { return ___m_MaxValue_24; }
	inline float* get_address_of_m_MaxValue_24() { return &___m_MaxValue_24; }
	inline void set_m_MaxValue_24(float value)
	{
		___m_MaxValue_24 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_25() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_WholeNumbers_25)); }
	inline bool get_m_WholeNumbers_25() const { return ___m_WholeNumbers_25; }
	inline bool* get_address_of_m_WholeNumbers_25() { return &___m_WholeNumbers_25; }
	inline void set_m_WholeNumbers_25(bool value)
	{
		___m_WholeNumbers_25 = value;
	}

	inline static int32_t get_offset_of_m_Value_26() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Value_26)); }
	inline float get_m_Value_26() const { return ___m_Value_26; }
	inline float* get_address_of_m_Value_26() { return &___m_Value_26; }
	inline void set_m_Value_26(float value)
	{
		___m_Value_26 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_OnValueChanged_27)); }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_28() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillImage_28)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_FillImage_28() const { return ___m_FillImage_28; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_FillImage_28() { return &___m_FillImage_28; }
	inline void set_m_FillImage_28(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_FillImage_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_29() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillTransform_29)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_FillTransform_29() const { return ___m_FillTransform_29; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_FillTransform_29() { return &___m_FillTransform_29; }
	inline void set_m_FillTransform_29(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_FillTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillContainerRect_30)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillContainerRect_30() const { return ___m_FillContainerRect_30; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillContainerRect_30() { return &___m_FillContainerRect_30; }
	inline void set_m_FillContainerRect_30(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_31() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleTransform_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_HandleTransform_31() const { return ___m_HandleTransform_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_HandleTransform_31() { return &___m_HandleTransform_31; }
	inline void set_m_HandleTransform_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_HandleTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_32() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleContainerRect_32)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleContainerRect_32() const { return ___m_HandleContainerRect_32; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleContainerRect_32() { return &___m_HandleContainerRect_32; }
	inline void set_m_HandleContainerRect_32(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleContainerRect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_33() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Offset_33)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_33() const { return ___m_Offset_33; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_33() { return &___m_Offset_33; }
	inline void set_m_Offset_33(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_33 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_34() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Tracker_34)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_34() const { return ___m_Tracker_34; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_34() { return &___m_Tracker_34; }
	inline void set_m_Tracker_34(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_34 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_35() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_DelayedUpdateVisuals_35)); }
	inline bool get_m_DelayedUpdateVisuals_35() const { return ___m_DelayedUpdateVisuals_35; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_35() { return &___m_DelayedUpdateVisuals_35; }
	inline void set_m_DelayedUpdateVisuals_35(bool value)
	{
		___m_DelayedUpdateVisuals_35 = value;
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T MonoSingleton`1<System.Object>::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MonoSingleton_1_get_Instance_mCE2B57A6C26AF0A4EB6651167B44367A4F4AFCDF_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// System.Void MonoSingleton`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared (MonoSingleton_1_tCB0740E1A0FBE7F99B80A6DFD8CD948FFAECF3D2 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.AudioSource::set_volume(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_volume_m37B6B2EACA7C2C18ABEE55EE5EA404085E94EE58 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, const RuntimeMethod* method);
// System.Void Script.Character.BaseCatChar::Sale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_Sale_m6522FB1264AF59864A3C036862AF942B58546A7E (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// T MonoSingleton`1<Script.Map.Money>::get_Instance()
inline Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03 (const RuntimeMethod* method)
{
	return ((  Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * (*) (const RuntimeMethod*))MonoSingleton_1_get_Instance_mCE2B57A6C26AF0A4EB6651167B44367A4F4AFCDF_gshared)(method);
}
// System.Void Script.Map.Money::GiveMoney(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, int32_t ___money0, const RuntimeMethod* method);
// System.Void Script.Character.BaseCatChar::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_Dead_mD1AFD0172EDA931DCB582B8A15DDAABA0F6A6DB7 (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Script.Character.BaseCatChar>()
inline BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Script.CardScript.CardPlace::Control()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_Control_m1F11D20D5023A43AFC131D84F72B6C66586DD078 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Camera_ScreenToWorldPoint_m7EE1C8665F9BB089581FA225BB4BFF411B5301B6 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3 (int32_t ___button0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Script.Map.Money::MoneyCheckOut(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16 (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, int32_t ___money0, const RuntimeMethod* method);
// T MonoSingleton`1<GameManager>::get_Instance()
inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC (const RuntimeMethod* method)
{
	return ((  GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * (*) (const RuntimeMethod*))MonoSingleton_1_get_Instance_mCE2B57A6C26AF0A4EB6651167B44367A4F4AFCDF_gshared)(method);
}
// !!0 UnityEngine.GameObject::GetComponent<Script.CardScript.CardPlace>()
inline CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * GameObject_GetComponent_TisCardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_m71F728D48A4F2F4F8D42693E252648983D2542B3 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Script.Map.SnapPoint>()
inline SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (const RuntimeMethod* method);
// System.Void Script.CardScript.CardPlaceField::Control()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_Control_m2A5DB9567CA0C4658F3835B0F66A3D9AA32C079A (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Script.CardScript.CardPlaceField>()
inline CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * GameObject_GetComponent_TisCardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_mD32E8767D2CBEC7A80462E7CB2D68D0CA04EC35C (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Script.Map.SnapPointField>()
inline SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Quaternion Unity.Mathematics.quaternion::op_Implicit(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093 (quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  ___q0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Void MonoSingleton`1<Script.Utility.CatCharacterManager>::.ctor()
inline void MonoSingleton_1__ctor_m5B8CECC53ED806262D94900D9F9E760C09839370 (MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8 * __this, const RuntimeMethod* method)
{
	((  void (*) (MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8 *, const RuntimeMethod*))MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared)(__this, method);
}
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Script.Character.Hit.HitDamageBullet>()
inline HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * GameObject_GetComponent_TisHitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_m9423AEAA8918C1D985BAAC26FC23B35CAC5EA9EF (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Void Script.Character.CatMelee::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee__ctor_m0D8A250217E8B9F387C4E5B1E817BA12A3CDA7B1 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method);
// System.Void Script.Character.CatMelee::CoolDownCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_CoolDownCtrl_m1E86186A97E010905E575D3113111246DE46A49D (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void Script.Character.CatMelee::ResetCoolDownTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method);
// System.Void Script.Character.BaseCatChar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2 (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method);
// System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_PlaySound_mEE619B3AC3B30E3EF1662D35568D1D6EB0B5B3DE (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, float ___volume1, const RuntimeMethod* method);
// System.Void Script.Character.CatShield::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_Dead_mABB1C0639062E66C49F5507D081676814FE23CAB (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CircleCollider2D::set_radius(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CircleCollider2D_set_radius_m0E86B81A48B46983E4CF6688F903CBDBD9681ABF (CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * __this, float ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Script.Enemy.Enemy>()
inline Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Script.Character.CatFire::Attack(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatFire_Attack_m7A3717FD3FEB9C7740C813E7BCEC5A25E35DB5BE (CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___atkPosision0, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, float ___damage0, const RuntimeMethod* method);
// System.Void Script.Character.CatMelee::Attack(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_Attack_m842B5023CE89B1923BC4B5DDEA768850C287E1A7 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___enemyPosision0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::ResetTimeCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::MoveLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Move_mDAC2F4D048EF5160585A1828E23D3CB92070BACD (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::TimeCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_TimeCtrl_mB4099D838835079D46AC4623CB8EEFE7AD6BAFFE (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::Playsound(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Playsound_m76AEE5C1AFDDEE80543917074E15791887E3E04F (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// T MonoSingleton`1<Script.Utility.MissionManager>::get_Instance()
inline MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33 (const RuntimeMethod* method)
{
	return ((  MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * (*) (const RuntimeMethod*))MonoSingleton_1_get_Instance_mCE2B57A6C26AF0A4EB6651167B44367A4F4AFCDF_gshared)(method);
}
// System.Void Script.Utility.MissionManager::EnemyScored(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_EnemyScored_m7C68CEFFF5713951EB7533DF47E4C617E04142D9 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, int32_t ___quantity0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::MoveDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::MoveUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::MoveRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::FlipMoveDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Script.Character.CatShield>()
inline CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * Component_GetComponent_TisCatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_mE27BBB6D62139C6AABDF5EFE59917F8844F43C23 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Script.Map.ExitDoor>()
inline ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * Component_GetComponent_TisExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_m2641F635A37E4ABBD38D51ADC3F77F630EA3BC1D (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Script.Enemy.Enemy::Reflect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Reflect_mB4E0614BD3165AF81B6A41A981B94BE441638629 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void Script.Character.CatShield::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_TakeHit_m190ABC57FD09DBBBD24F172F2474883521BD74DB (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, float ___damage0, const RuntimeMethod* method);
// System.Void Script.Map.ExitDoor::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitDoor_TakeHit_mC9D5DEAD44D641FD35E135CEA79308AD76EC897E (ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * __this, float ___damage0, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::ResetSpeedMult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_ResetSpeedMult_m419D80A63CFBA625803D6CEF30E00A397E7EE3FB (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void Script.Enemy.Enemy::Kill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Kill_m15BEB3F4C334EF8225B15CC5C0077AF4A12D1A0B (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method);
// System.Void GameManager::GameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m6B3C9B5DDE6CDE2A041D05C4F3BE4A3D3D745B70 (String_t* ___sceneName0, int32_t ___mode1, const RuntimeMethod* method);
// T MonoSingleton`1<Script.UI.MainMenu>::get_Instance()
inline MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97 (const RuntimeMethod* method)
{
	return ((  MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * (*) (const RuntimeMethod*))MonoSingleton_1_get_Instance_mCE2B57A6C26AF0A4EB6651167B44367A4F4AFCDF_gshared)(method);
}
// System.Void Script.UI.MainMenu::GameOverMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_GameOverMenu_m1C3686D2523C552479A2FE8B38DD9C35754D16D2 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186 (String_t* ___tag0, const RuntimeMethod* method);
// System.Void GameManager::Click()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Click_mF51E03F3AF95EA64F1B3AED7315C42BE91F1A4C9 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4 (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  ___ray0, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo1, float ___maxDistance2, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Void MonoSingleton`1<GameManager>::.ctor()
inline void MonoSingleton_1__ctor_m1C4CAEFF311D0D70F5075E5936B923F272D2787E (MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E * __this, const RuntimeMethod* method)
{
	((  void (*) (MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E *, const RuntimeMethod*))MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void Script.Character.Hit.HitDamageBullet::MoveTo(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet_MoveTo_mE6F6507496D23D4B3AB8D088CA6ED595909D21C6 (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___target0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___translation0, const RuntimeMethod* method);
// System.Void Script.Character.Hit.HitDamage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamage__ctor_m8543E46E21A1181502B34E95DC26096CB75A1294 (HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Slider>()
inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * Component_GetComponent_TisSlider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_mD501A27463515FA99A5A93A10E37F913696D20C4 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Slider_set_maxValue_m5CDA3D451B68CF2D3FCFF43D1738D1DCC1C6425B (Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (float ___value0, const RuntimeMethod* method);
// System.Void Script.UI.MainMenu::Unpause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// System.Void Script.UI.MainMenu::QuitMission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method);
// System.Void GameManager::DestroyAllEnemy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_DestroyAllEnemy_m3A3AFCD2EC6A82A49FAC55F527ACC6F9E91910EE (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Void GameManager::DestroyAllUnit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_DestroyAllUnit_m6BDE5206F29B58B5724BD590AA77466D6B545E97 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsync(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * SceneManager_UnloadSceneAsync_mF564BF92447F58313A518206EE15E5DEED0448EF (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void Script.Utility.MissionManager::EnemyScoreReset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_EnemyScoreReset_m4CD561F1F4C88D55C232A092577228D956C4284C (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method);
// System.Void MonoSingleton`1<Script.UI.MainMenu>::.ctor()
inline void MonoSingleton_1__ctor_m714FDB833649A832A7113836EC9F35AF4ADFF368 (MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A * __this, const RuntimeMethod* method)
{
	((  void (*) (MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A *, const RuntimeMethod*))MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared)(__this, method);
}
// System.Void Script.Utility.MissionManager::MissionUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method);
// System.Void GameManager::Win()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Win_mEE312032712783936AAC9B0EF0CED7BDB345C973 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Void MonoSingleton`1<Script.Utility.MissionManager>::.ctor()
inline void MonoSingleton_1__ctor_m016CCA4BA41740634C0C66CCE7D7FF46B3ADD926 (MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02 * __this, const RuntimeMethod* method)
{
	((  void (*) (MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02 *, const RuntimeMethod*))MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared)(__this, method);
}
// System.Void MonoSingleton`1<Script.Map.Money>::.ctor()
inline void MonoSingleton_1__ctor_m20F70D38D8139FF5AB0E960955FFF561F93D0710 (MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0 * __this, const RuntimeMethod* method)
{
	((  void (*) (MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0 *, const RuntimeMethod*))MonoSingleton_1__ctor_m61403EFFF67FCC44C5204C87188AABECE7128139_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_timeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4 (const RuntimeMethod* method);
// System.Void Script.Map.SnapPoint::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SnapPoint__ctor_m66C9037DF5523C44D203EAE05060D7CA456F9FEA (SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * __this, const RuntimeMethod* method);
// System.Void Script.Map.Spawner::Spawn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Spawn_m04CCC86C5F5C3191376D7C37D63EA61685D94059 (Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C * __this, const RuntimeMethod* method);
// System.String System.String::ToLower()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D (String_t* __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, const RuntimeMethod* method)
{
	{
		// audioSource.volume = 1;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_7();
		AudioSource_set_volume_m37B6B2EACA7C2C18ABEE55EE5EA404085E94EE58(L_0, (1.0f), /*hidden argument*/NULL);
		// audioSource.clip = sound;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_1 = __this->get_audioSource_7();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_2 = ___sound0;
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_1, L_2, /*hidden argument*/NULL);
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_3 = __this->get_audioSource_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::PlaySound(UnityEngine.AudioClip,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_PlaySound_mEE619B3AC3B30E3EF1662D35568D1D6EB0B5B3DE (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, float ___volume1, const RuntimeMethod* method)
{
	{
		// audioSource.clip = sound;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_7();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_1 = ___sound0;
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_0, L_1, /*hidden argument*/NULL);
		// audioSource.volume = volume;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSource_7();
		float L_3 = ___volume1;
		AudioSource_set_volume_m37B6B2EACA7C2C18ABEE55EE5EA404085E94EE58(L_2, L_3, /*hidden argument*/NULL);
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_4 = __this->get_audioSource_7();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_OnMouseDown_m146DA045E35E7B491D25474995023F3E3C36FC8C (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	{
		// if (isPlaced)
		bool L_0 = __this->get_isPlaced_5();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Sale();
		BaseCatChar_Sale_m6522FB1264AF59864A3C036862AF942B58546A7E(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::OnMouseOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_OnMouseOver_m99DC651023EB7E8E1A4B9C685A0B037056D7CFAB (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	{
		// if (isPlaced)
		bool L_0 = __this->get_isPlaced_5();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// spriteRenderer.color = selectColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2 = __this->get_selectColor_12();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::OnMouseExit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_OnMouseExit_mA499643F058B20D312A784080DE3AE3FA3041DF8 (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	{
		// if (isPlaced)
		bool L_0 = __this->get_isPlaced_5();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// spriteRenderer.color = normalColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2 = __this->get_normalColor_11();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::Sale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_Sale_m6522FB1264AF59864A3C036862AF942B58546A7E (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Money.Instance.GiveMoney(price/2);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_1 = __this->get_price_4();
		Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F(L_0, ((int32_t)((int32_t)L_1/(int32_t)2)), /*hidden argument*/NULL);
		// Dead();
		BaseCatChar_Dead_mD1AFD0172EDA931DCB582B8A15DDAABA0F6A6DB7(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_OnDestroy_m279698C4FACBDC8B48F423B7235B18557B1AAF8E (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	{
		// snappoint.isEmpty = true;
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_0 = __this->get_snappoint_8();
		L_0->set_isEmpty_4((bool)1);
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar_Dead_mD1AFD0172EDA931DCB582B8A15DDAABA0F6A6DB7 (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.BaseCatChar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2 (BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.CardScript.CardPlace::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_Awake_mB506C6BA06840036A4F6911F289FA7C32E746B03 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// baseCatChar = GetComponent<BaseCatChar>();
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_0;
		L_0 = Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2(__this, /*hidden argument*/Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2_RuntimeMethod_var);
		__this->set_baseCatChar_5(L_0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_Start_m9A09AFE26C638B02FD13B455255911A141CCF688 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (spriteRenderer ==null)
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRenderer_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// spriteRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRenderer_7(L_2);
	}

IL_001a:
	{
		// isPlace = false;
		__this->set_isPlace_13((bool)0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_Update_m133A6E7711FBE0870CAA303C5AC05B53CF9B8A66 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	{
		// Control();
		CardPlace_Control_m1F11D20D5023A43AFC131D84F72B6C66586DD078(__this, /*hidden argument*/NULL);
		// if (placeable)
		bool L_0 = __this->get_placeable_12();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		// spriteRenderer.color = placeableColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2 = __this->get_placeableColor_8();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
		// }
		goto IL_0032;
	}

IL_0021:
	{
		// spriteRenderer.color = unplaceableColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = __this->get_unplaceableColor_9();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_5;
		L_5 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Camera_ScreenToWorldPoint_m7EE1C8665F9BB089581FA225BB4BFF411B5301B6(L_5, L_6, /*hidden argument*/NULL);
		__this->set_mousePosition_4(L_7);
		// if (!isPlace)
		bool L_8 = __this->get_isPlace_13();
		if (L_8)
		{
			goto IL_007a;
		}
	}
	{
		// transform.position = new Vector3(mousePosition.x, mousePosition.y, 1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_mousePosition_4();
		float L_11 = L_10->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of_mousePosition_4();
		float L_13 = L_12->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_14), L_11, L_13, (1.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_9, L_14, /*hidden argument*/NULL);
	}

IL_007a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::PlaceablePoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_PlaceablePoint_m876ADBC20542700119A161AB67426F68A68B9F50 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	{
		// placeable = enabled;
		bool L_0;
		L_0 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(__this, /*hidden argument*/NULL);
		__this->set_placeable_12(L_0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::Control()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_Control_m1F11D20D5023A43AFC131D84F72B6C66586DD078 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_m71F728D48A4F2F4F8D42693E252648983D2542B3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetMouseButtonDown(0))
		bool L_0;
		L_0 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0098;
		}
	}
	{
		// if (placeable && Money.Instance.money >= baseCatChar.price && snappoint != null)
		bool L_1 = __this->get_placeable_12();
		if (!L_1)
		{
			goto IL_0098;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_2;
		L_2 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_3 = L_2->get_money_7();
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_4 = __this->get_baseCatChar_5();
		int32_t L_5 = L_4->get_price_4();
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0098;
		}
	}
	{
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_6 = __this->get_snappoint_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0098;
		}
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = __this->get_audioSource_11();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_8, /*hidden argument*/NULL);
		// Debug.Log("hello");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9, /*hidden argument*/NULL);
		// Money.Instance.MoneyCheckOut(baseCatChar.price);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_9;
		L_9 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_10 = __this->get_baseCatChar_5();
		int32_t L_11 = L_10->get_price_4();
		Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16(L_9, L_11, /*hidden argument*/NULL);
		// baseCatChar.isPlaced = true;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_12 = __this->get_baseCatChar_5();
		L_12->set_isPlaced_5((bool)1);
		// GameManager.Instance.isCardOnHand = false;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_13;
		L_13 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_13->set_isCardOnHand_7((bool)0);
		// snappoint.isEmpty = false;
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_14 = __this->get_snappoint_6();
		L_14->set_isEmpty_4((bool)0);
		// Destroy(gameObject.GetComponent<CardPlace>());
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * L_16;
		L_16 = GameObject_GetComponent_TisCardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_m71F728D48A4F2F4F8D42693E252648983D2542B3(L_15, /*hidden argument*/GameObject_GetComponent_TisCardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F_m71F728D48A4F2F4F8D42693E252648983D2542B3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_16, /*hidden argument*/NULL);
	}

IL_0098:
	{
		// if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
		bool L_17;
		L_17 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)27), /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00a9;
		}
	}
	{
		bool L_18;
		L_18 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(1, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b4;
		}
	}

IL_00a9:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_19, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_OnTriggerEnter_m6DE8C38EA41FDBB98005EBFAE816F1D790FB6AA9 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snappoint = other.GetComponent<SnapPoint>();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_1;
		L_1 = Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026(L_0, /*hidden argument*/Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		__this->set_snappoint_6(L_1);
		// if (snappoint != null && snappoint.isEmpty)
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_2 = __this->get_snappoint_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_4 = __this->get_snappoint_6();
		bool L_5 = L_4->get_isEmpty_4();
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		// transform.position = snappoint.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_7 = __this->get_snappoint_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_6, L_9, /*hidden argument*/NULL);
		// baseCatChar.snappoint = snappoint;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_10 = __this->get_baseCatChar_5();
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_11 = __this->get_snappoint_6();
		L_10->set_snappoint_8(L_11);
		// placeable = true;
		__this->set_placeable_12((bool)1);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_OnTriggerStay2D_m1E13AA5AF50D8A7DBB80B9247C47B8ABB5352FD5 (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snappoint = other.GetComponent<SnapPoint>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_1;
		L_1 = Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026(L_0, /*hidden argument*/Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		__this->set_snappoint_6(L_1);
		// baseCatChar.snappoint = snappoint;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_2 = __this->get_baseCatChar_5();
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_3 = __this->get_snappoint_6();
		L_2->set_snappoint_8(L_3);
		// if (snappoint != null && snappoint.isEmpty)
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_4 = __this->get_snappoint_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_6 = __this->get_snappoint_6();
		bool L_7 = L_6->get_isEmpty_4();
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		// transform.position = snappoint.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_9 = __this->get_snappoint_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_8, L_11, /*hidden argument*/NULL);
		// placeable = true;
		__this->set_placeable_12((bool)1);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::OnTriggerExit2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_OnTriggerExit2D_m4913FD2C7F120F5D3384C27EC766CD3E7CB216CC (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snappoint = other.GetComponent<SnapPoint>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_1;
		L_1 = Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026(L_0, /*hidden argument*/Component_GetComponent_TisSnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD_m4441E724620048C878E5B4492BFDF42D47F42026_RuntimeMethod_var);
		__this->set_snappoint_6(L_1);
		// if (snappoint != null)
		SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * L_2 = __this->get_snappoint_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		// placeable = false;
		__this->set_placeable_12((bool)0);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace_OnDestroy_m240CBD93208BA19EC3062B95767901816C3CE90A (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance.isCardOnHand = false;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_0->set_isCardOnHand_7((bool)0);
		// spriteRenderer.color = Color.white;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2;
		L_2 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlace::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlace__ctor_m8B5EBD86BC0C1CDCF08A3933DB9E8E9F082070DB (CardPlace_tF343237FAA00B1EA152C5ED3BA8A69E60B18EB4F * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.CardScript.CardPlaceField::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_Awake_m0B101E734C546DFFABF3D6BC772E445C69C70E80 (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// baseCatChar = GetComponent<BaseCatChar>();
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_0;
		L_0 = Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2(__this, /*hidden argument*/Component_GetComponent_TisBaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4_mA0AD6B09C1EEE763E4166AD1234DBC92096C54D2_RuntimeMethod_var);
		__this->set_baseCatChar_5(L_0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_Start_mCEF5B7DFB101BF33649B2FB82480A0EA719C74AB (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (spriteRenderer ==null)
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_spriteRenderer_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// spriteRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		__this->set_spriteRenderer_7(L_2);
	}

IL_001a:
	{
		// isPlace = false;
		__this->set_isPlace_13((bool)0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_Update_m58998057080D4C142A838FB4080A8284BD0AF9BC (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	{
		// Control();
		CardPlaceField_Control_m2A5DB9567CA0C4658F3835B0F66A3D9AA32C079A(__this, /*hidden argument*/NULL);
		// if (placeable)
		bool L_0 = __this->get_placeable_12();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		// spriteRenderer.color = placeableColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2 = __this->get_placeableColor_8();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
		// }
		goto IL_0032;
	}

IL_0021:
	{
		// spriteRenderer.color = unplaceableColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = __this->get_unplaceableColor_9();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_5;
		L_5 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Camera_ScreenToWorldPoint_m7EE1C8665F9BB089581FA225BB4BFF411B5301B6(L_5, L_6, /*hidden argument*/NULL);
		__this->set_mousePosition_4(L_7);
		// if (!isPlace)
		bool L_8 = __this->get_isPlace_13();
		if (L_8)
		{
			goto IL_007a;
		}
	}
	{
		// transform.position = new Vector3(mousePosition.x, mousePosition.y, 1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_mousePosition_4();
		float L_11 = L_10->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of_mousePosition_4();
		float L_13 = L_12->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_14), L_11, L_13, (1.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_9, L_14, /*hidden argument*/NULL);
	}

IL_007a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::PlaceablePoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_PlaceablePoint_m7F2678AC45B0DFD59A0B82B088D5AB1EA14A7C24 (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	{
		// placeable = enabled;
		bool L_0;
		L_0 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(__this, /*hidden argument*/NULL);
		__this->set_placeable_12(L_0);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::Control()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_Control_m2A5DB9567CA0C4658F3835B0F66A3D9AA32C079A (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_mD32E8767D2CBEC7A80462E7CB2D68D0CA04EC35C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetMouseButtonDown(0))
		bool L_0;
		L_0 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0098;
		}
	}
	{
		// if (placeable && Money.Instance.money >= baseCatChar.price && snapPointField != null)
		bool L_1 = __this->get_placeable_12();
		if (!L_1)
		{
			goto IL_0098;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_2;
		L_2 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_3 = L_2->get_money_7();
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_4 = __this->get_baseCatChar_5();
		int32_t L_5 = L_4->get_price_4();
		if ((((int32_t)L_3) < ((int32_t)L_5)))
		{
			goto IL_0098;
		}
	}
	{
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_6 = __this->get_snapPointField_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0098;
		}
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_8 = __this->get_audioSource_11();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_8, /*hidden argument*/NULL);
		// Debug.Log("hello");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9, /*hidden argument*/NULL);
		// Money.Instance.MoneyCheckOut(baseCatChar.price);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_9;
		L_9 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_10 = __this->get_baseCatChar_5();
		int32_t L_11 = L_10->get_price_4();
		Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16(L_9, L_11, /*hidden argument*/NULL);
		// baseCatChar.isPlaced = true;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_12 = __this->get_baseCatChar_5();
		L_12->set_isPlaced_5((bool)1);
		// GameManager.Instance.isCardOnHand = false;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_13;
		L_13 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_13->set_isCardOnHand_7((bool)0);
		// snapPointField.isEmpty = false;
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_14 = __this->get_snapPointField_6();
		((SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD *)L_14)->set_isEmpty_4((bool)0);
		// Destroy(gameObject.GetComponent<CardPlaceField>());
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * L_16;
		L_16 = GameObject_GetComponent_TisCardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_mD32E8767D2CBEC7A80462E7CB2D68D0CA04EC35C(L_15, /*hidden argument*/GameObject_GetComponent_TisCardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B_mD32E8767D2CBEC7A80462E7CB2D68D0CA04EC35C_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_16, /*hidden argument*/NULL);
	}

IL_0098:
	{
		// if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
		bool L_17;
		L_17 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)27), /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00a9;
		}
	}
	{
		bool L_18;
		L_18 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(1, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b4;
		}
	}

IL_00a9:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_19, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_OnTriggerEnter_m6F8355779EDB12BC0300D7D1EB18BBE6A3567E72 (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snapPointField = other.GetComponent<SnapPointField>();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_1;
		L_1 = Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5(L_0, /*hidden argument*/Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		__this->set_snapPointField_6(L_1);
		// if (snapPointField != null && snapPointField.isEmpty)
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_2 = __this->get_snapPointField_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005a;
		}
	}
	{
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_4 = __this->get_snapPointField_6();
		bool L_5 = ((SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD *)L_4)->get_isEmpty_4();
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		// transform.position = snapPointField.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_7 = __this->get_snapPointField_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_6, L_9, /*hidden argument*/NULL);
		// baseCatChar.snapPointField = snapPointField;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_10 = __this->get_baseCatChar_5();
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_11 = __this->get_snapPointField_6();
		L_10->set_snapPointField_9(L_11);
		// placeable = true;
		__this->set_placeable_12((bool)1);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_OnTriggerStay2D_mF2295F593F933F9A7E897EF0FAB2C6EF16A6E55A (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snapPointField = other.GetComponent<SnapPointField>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_1;
		L_1 = Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5(L_0, /*hidden argument*/Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		__this->set_snapPointField_6(L_1);
		// baseCatChar.snapPointField = snapPointField;
		BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 * L_2 = __this->get_baseCatChar_5();
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_3 = __this->get_snapPointField_6();
		L_2->set_snapPointField_9(L_3);
		// if (snapPointField != null && snapPointField.isEmpty)
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_4 = __this->get_snapPointField_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_6 = __this->get_snapPointField_6();
		bool L_7 = ((SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD *)L_6)->get_isEmpty_4();
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		// transform.position = snapPointField.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_9 = __this->get_snapPointField_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_8, L_11, /*hidden argument*/NULL);
		// placeable = true;
		__this->set_placeable_12((bool)1);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::OnTriggerExit2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_OnTriggerExit2D_mB25654277398C5EA5969D25BD82B1932475B1C47 (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// snapPointField = other.GetComponent<SnapPointField>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_1;
		L_1 = Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5(L_0, /*hidden argument*/Component_GetComponent_TisSnapPointField_t7D8385A372032D364F54551D890308C67872B555_m07AA6A4646807D1E49B9BDF0F4531DD464C267C5_RuntimeMethod_var);
		__this->set_snapPointField_6(L_1);
		// if (snapPointField != null)
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_2 = __this->get_snapPointField_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		// placeable = false;
		__this->set_placeable_12((bool)0);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField_OnDestroy_m672F64D215A1658859D02EBCF0A98E1517E93A8D (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance.isCardOnHand = false;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_0->set_isCardOnHand_7((bool)0);
		// spriteRenderer.color = Color.white;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2;
		L_2 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.CardScript.CardPlaceField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CardPlaceField__ctor_mADD90E41A41C23C4A93EAB7AD8951414CEB85C7B (CardPlaceField_t5CCA98C542A0FEFA1D1DEAD8B572493746DAD74B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CatCard::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCard_Start_mC241CE6745BC8FD82EC3AE235240B2A174789C5E (CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void CatCard::PickCard()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCard_PickCard_m5FA425EBE1D19368F1BEF2AF0AEBFEB628C3B120 (CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!GameManager.Instance.isCardOnHand)
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		bool L_1 = L_0->get_isCardOnHand_7();
		if (L_1)
		{
			goto IL_0041;
		}
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSource_5();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_2, /*hidden argument*/NULL);
		// Instantiate(character, new Vector3(0,0,0), quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_character_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  L_5 = ((quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var))->get_identity_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6;
		L_6 = quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_3, L_4, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
	}

IL_0041:
	{
		// }
		return;
	}
}
// System.Void CatCard::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCard__ctor_mA8DB16F69268E6DFF800BFBDC376F21E81FD860C (CatCard_t2CE1FE877D247C3F448BDB5713BEF49FAC4517C6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Utility.CatCharacterManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCharacterManager_Start_mF7570321D75F115A87A64CDE915E44377B325C59 (CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Utility.CatCharacterManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCharacterManager_Update_m0FC05CDF51AD4A32FA7F94BE52BF50D73CAC3D8B (CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Utility.CatCharacterManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatCharacterManager__ctor_m9D51FC19D75231C828EF2DCF9EFDAAAA49908BCC (CatCharacterManager_t98A9CB08A8E59F70BA2212172CD07A9CFEEEFC0E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1__ctor_m5B8CECC53ED806262D94900D9F9E760C09839370_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tDFE24B8BDA57AB177EEA30B5C4E551F7C1C8F7D8_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m5B8CECC53ED806262D94900D9F9E760C09839370(__this, /*hidden argument*/MonoSingleton_1__ctor_m5B8CECC53ED806262D94900D9F9E760C09839370_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.CatFire::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatFire_Start_mAD268507DF199959584A11E7B58A208537931D37 (CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// attackCoolDown = attackCoolDownMax;
		float L_0 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_attackCoolDownMax_16();
		((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->set_attackCoolDown_15(L_0);
		// GameManager.Instance.isCardOnHand = true;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1;
		L_1 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_1->set_isCardOnHand_7((bool)1);
		// isPlaced = false;
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_isPlaced_5((bool)0);
		// normalColor = spriteRenderer.color;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_spriteRenderer_18();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3;
		L_3 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_2, /*hidden argument*/NULL);
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_normalColor_11(L_3);
		// }
		return;
	}
}
// System.Void Script.Character.CatFire::Attack(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatFire_Attack_m7A3717FD3FEB9C7740C813E7BCEC5A25E35DB5BE (CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___atkPosision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisHitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_m9423AEAA8918C1D985BAAC26FC23B35CAC5EA9EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// hitType.GetComponent<HitDamageBullet>().targetOBJ = target.gameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_hitType_17();
		HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * L_1;
		L_1 = GameObject_GetComponent_TisHitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_m9423AEAA8918C1D985BAAC26FC23B35CAC5EA9EF(L_0, /*hidden argument*/GameObject_GetComponent_TisHitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD_m9423AEAA8918C1D985BAAC26FC23B35CAC5EA9EF_RuntimeMethod_var);
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_2 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_target_19();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		L_1->set_targetOBJ_5(L_3);
		// var enemyDir = target.gameObject.transform.position;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_4 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_target_19();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// if (enemyDir.x > transform.position.x)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_0;
		float L_9 = L_8.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		float L_12 = L_11.get_x_2();
		if ((!(((float)L_9) > ((float)L_12))))
		{
			goto IL_0057;
		}
	}
	{
		// spriteRenderer.flipX = false;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_13 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_spriteRenderer_18();
		SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E(L_13, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_007b;
	}

IL_0057:
	{
		// else if (enemyDir.x <= transform.position.x)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = V_0;
		float L_15 = L_14.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		float L_18 = L_17.get_x_2();
		if ((!(((float)L_15) <= ((float)L_18))))
		{
			goto IL_007b;
		}
	}
	{
		// spriteRenderer.flipX = true;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_19 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_spriteRenderer_18();
		SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E(L_19, (bool)1, /*hidden argument*/NULL);
	}

IL_007b:
	{
		// PlaySound(aud_Attack);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_20 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_aud_Attack_21();
		BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB(__this, L_20, /*hidden argument*/NULL);
		// animator.SetBool("IsAttack",true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_21 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_animator_6();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_21, _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673, (bool)1, /*hidden argument*/NULL);
		// Instantiate(hitType,atkPosision,quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)__this)->get_hitType_17();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = ___atkPosision0;
		IL2CPP_RUNTIME_CLASS_INIT(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  L_24 = ((quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var))->get_identity_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_25;
		L_25 = quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26;
		L_26 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_22, L_23, L_25, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Script.Character.CatFire::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatFire__ctor_m7C4BD2A2208600182772D0139B1BC272D8BE1EED (CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * __this, const RuntimeMethod* method)
{
	{
		CatMelee__ctor_m0D8A250217E8B9F387C4E5B1E817BA12A3CDA7B1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.CatMelee::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_Start_mBF4CCF71EA5C83D00037C36055EA1F3A1D4F3D8C (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// attackCoolDown = attackCoolDownMax;
		float L_0 = __this->get_attackCoolDownMax_16();
		__this->set_attackCoolDown_15(L_0);
		// GameManager.Instance.isCardOnHand = true;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1;
		L_1 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_1->set_isCardOnHand_7((bool)1);
		// isPlaced = false;
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_isPlaced_5((bool)0);
		// normalColor = spriteRenderer.color;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2 = __this->get_spriteRenderer_18();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3;
		L_3 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_2, /*hidden argument*/NULL);
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_normalColor_11(L_3);
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_Update_m49A31FE0C95BBE5A47F71937AC9BABE440F37596 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	{
		// if (isPlaced)
		bool L_0 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_isPlaced_5();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// CoolDownCtrl();
		CatMelee_CoolDownCtrl_m1E86186A97E010905E575D3113111246DE46A49D(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::CoolDownCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_CoolDownCtrl_m1E86186A97E010905E575D3113111246DE46A49D (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	{
		// attackCoolDown -= Time.deltaTime;
		float L_0 = __this->get_attackCoolDown_15();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_attackCoolDown_15(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if (attackCoolDown <= -1)
		float L_2 = __this->get_attackCoolDown_15();
		if ((!(((float)L_2) <= ((float)(-1.0f)))))
		{
			goto IL_0025;
		}
	}
	{
		// ResetCoolDownTime();
		CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::ResetCoolDownTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	{
		// attackCoolDown = attackCoolDownMax;
		float L_0 = __this->get_attackCoolDownMax_16();
		__this->set_attackCoolDown_15(L_0);
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::StopAttack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_StopAttack_m3B1DC4C3B791A1C9447F52DBD2740BC7BB1B6F1C (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.SetBool("IsAttack",false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_animator_6();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::Attack(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee_Attack_m842B5023CE89B1923BC4B5DDEA768850C287E1A7 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___enemyPosision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var enemyDir = target.gameObject.transform.position;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_0 = __this->get_target_19();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (enemyDir.x > transform.position.x)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		float L_5 = L_4.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		if ((!(((float)L_5) > ((float)L_8))))
		{
			goto IL_003c;
		}
	}
	{
		// spriteRenderer.flipX = false;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_9 = __this->get_spriteRenderer_18();
		SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E(L_9, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_0060;
	}

IL_003c:
	{
		// else if (enemyDir.x <= transform.position.x)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		float L_11 = L_10.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_12, /*hidden argument*/NULL);
		float L_14 = L_13.get_x_2();
		if ((!(((float)L_11) <= ((float)L_14))))
		{
			goto IL_0060;
		}
	}
	{
		// spriteRenderer.flipX = true;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_15 = __this->get_spriteRenderer_18();
		SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E(L_15, (bool)1, /*hidden argument*/NULL);
	}

IL_0060:
	{
		// PlaySound(aud_Attack);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_16 = __this->get_aud_Attack_21();
		BaseCatChar_PlaySound_mBA500812807DB0DE5D12B23F59738FBD12AFECFB(__this, L_16, /*hidden argument*/NULL);
		// animator.SetBool("IsAttack",true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_17 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_animator_6();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_17, _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673, (bool)1, /*hidden argument*/NULL);
		// Instantiate(hitType,enemyPosision,quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_hitType_17();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = ___enemyPosision0;
		IL2CPP_RUNTIME_CLASS_INIT(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  L_20 = ((quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var))->get_identity_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_21;
		L_21 = quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22;
		L_22 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_18, L_19, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Script.Character.CatMelee::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatMelee__ctor_m0D8A250217E8B9F387C4E5B1E817BA12A3CDA7B1 (CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * __this, const RuntimeMethod* method)
{
	{
		BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.CatShield::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_Start_m51B4B394294A2EB0C10B7DC43F36B80D8E458E99 (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance.isCardOnHand = true;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		L_0->set_isCardOnHand_7((bool)1);
		// isPlaced = false;
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_isPlaced_5((bool)0);
		// normalColor = spriteRenderer.color;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1 = __this->get_spriteRenderer_17();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2;
		L_2 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_1, /*hidden argument*/NULL);
		((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->set_normalColor_11(L_2);
		// }
		return;
	}
}
// System.Void Script.Character.CatShield::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_TakeHit_m190ABC57FD09DBBBD24F172F2474883521BD74DB (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, float ___damage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PlaySound(aud_Takehit,0.4f);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_0 = __this->get_aud_Takehit_18();
		BaseCatChar_PlaySound_mEE619B3AC3B30E3EF1662D35568D1D6EB0B5B3DE(__this, L_0, (0.400000006f), /*hidden argument*/NULL);
		// animator.SetBool("IsAttack",true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_animator_6();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_1, _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673, (bool)1, /*hidden argument*/NULL);
		// hp -= damage;
		float L_2 = __this->get_hp_13();
		float L_3 = ___damage0;
		__this->set_hp_13(((float)il2cpp_codegen_subtract((float)L_2, (float)L_3)));
		// if (hp <= 0)
		float L_4 = __this->get_hp_13();
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_0043;
		}
	}
	{
		// Dead();
		CatShield_Dead_mABB1C0639062E66C49F5507D081676814FE23CAB(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		// Instantiate(hitType, transform.position, quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_hitType_16();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  L_8 = ((quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var))->get_identity_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9;
		L_9 = quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_5, L_7, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Script.Character.CatShield::StopTakeHit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_StopTakeHit_m046D02C37C82EF20951038E5F46FCB0854F08527 (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.SetBool("IsAttack",false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_animator_6();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, _stringLiteralE9757F2E44D7A39BB09D118B3C14D487E25FC673, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.CatShield::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_Dead_mABB1C0639062E66C49F5507D081676814FE23CAB (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.CatShield::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield_OnDestroy_m60C2795705F2CD68B2CB51B3416CEA6BDFDF0F81 (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method)
{
	{
		// snapPointField.isEmpty = true;
		SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * L_0 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)__this)->get_snapPointField_9();
		((SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD *)L_0)->set_isEmpty_4((bool)1);
		// }
		return;
	}
}
// System.Void Script.Character.CatShield::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CatShield__ctor_m43348309BD5DDBFB81D78555219E57593C2840F9 (CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * __this, const RuntimeMethod* method)
{
	{
		BaseCatChar__ctor_mEEE9D01E2C776C54FA4F2B1007469730FAAC4FA2(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.DamageArea.CharDamAreaFire::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaFire_Start_m69D2987102E6EE54495D51A3B78207ADBEF430D7 (CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60 * __this, const RuntimeMethod* method)
{
	{
		// hitBox.radius = radius;
		CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * L_0 = __this->get_hitBox_4();
		float L_1 = __this->get_radius_6();
		CircleCollider2D_set_radius_m0E86B81A48B46983E4CF6688F903CBDBD9681ABF(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharDamAreaFire::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaFire_OnTriggerStay2D_mFE1807824F7A4DC29C2B506CE5F456F29DC9D2C2 (CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral42EA15876C33949F765AE605B9DFCCDC4E9BE311);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = other.GetComponent<Enemy.Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_1;
		L_1 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_0, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		__this->set_target_7(L_1);
		// catFire.target = target;
		CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * L_2 = __this->get_catFire_5();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_3 = __this->get_target_7();
		((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)L_2)->set_target_19(L_3);
		// if (catFire.attackCoolDown <= 0 && target != null)
		CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * L_4 = __this->get_catFire_5();
		float L_5 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)L_4)->get_attackCoolDown_15();
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0068;
		}
	}
	{
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_6 = __this->get_target_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		// catFire.Attack(transform.position);
		CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * L_8 = __this->get_catFire_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_9, /*hidden argument*/NULL);
		CatFire_Attack_m7A3717FD3FEB9C7740C813E7BCEC5A25E35DB5BE(L_8, L_10, /*hidden argument*/NULL);
		// Debug.Log("Attack!!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral42EA15876C33949F765AE605B9DFCCDC4E9BE311, /*hidden argument*/NULL);
		// catFire.ResetCoolDownTime();
		CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * L_11 = __this->get_catFire_5();
		CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78(L_11, /*hidden argument*/NULL);
	}

IL_0068:
	{
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharDamAreaFire::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaFire__ctor_m2B1A5FA185153797CD8811559BC5E6BEC6EA5E93 (CharDamAreaFire_t2D186D0D4BDD45E85B79B9873CA296FA46BC5C60 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.DamageArea.CharDamAreaShield::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaShield_Start_mEF4E19E58C0F9663A3410B5530DA0B94A8A662D8 (CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharDamAreaShield::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaShield_OnTriggerEnter2D_m788D7669FDE2D51C6D3AA688CF459A7C0CCBC722 (CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharDamAreaShield::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaShield_OnTriggerStay2D_m51D3070F4F6E1879E6023E654E8E7B3035E7F0C8 (CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA149D0A68B12DEDE1102E7900A7FCA36A18F064D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = other.GetComponent<Enemy.Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_1;
		L_1 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_0, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		__this->set_target_5(L_1);
		// target.TakeHit(catShield.damage);
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_2 = __this->get_target_5();
		CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * L_3 = __this->get_catShield_4();
		float L_4 = L_3->get_damage_14();
		Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41(L_2, L_4, /*hidden argument*/NULL);
		// Debug.Log("Royal Guard!!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralA149D0A68B12DEDE1102E7900A7FCA36A18F064D, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharDamAreaShield::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharDamAreaShield__ctor_mFD4F110A1EA0276EA4682F362BF26589C9630127 (CharDamAreaShield_t126A322885278112189A9C72F68F83CD1269BA89 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.DamageArea.CharacterDamageArea::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterDamageArea_Start_mBCBDC010DE85B692B4FC94ABFEFF0989893B1D03 (CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE * __this, const RuntimeMethod* method)
{
	{
		// hitBox.radius = radius;
		CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * L_0 = __this->get_hitBox_4();
		float L_1 = __this->get_radius_6();
		CircleCollider2D_set_radius_m0E86B81A48B46983E4CF6688F903CBDBD9681ABF(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharacterDamageArea::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterDamageArea_OnTriggerEnter2D_m326CB07289E875F3261BAB56EBEF657A464B0B3D (CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharacterDamageArea::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterDamageArea_OnTriggerStay2D_m30BCB02FE4A7C16DB3468C26648CB2014174778F (CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral42EA15876C33949F765AE605B9DFCCDC4E9BE311);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = other.GetComponent<Enemy.Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_1;
		L_1 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_0, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		__this->set_target_7(L_1);
		// catMelee.target = target;
		CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * L_2 = __this->get_catMelee_5();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_3 = __this->get_target_7();
		L_2->set_target_19(L_3);
		// if (catMelee.attackCoolDown <= 0)
		CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * L_4 = __this->get_catMelee_5();
		float L_5 = L_4->get_attackCoolDown_15();
		if ((!(((float)L_5) <= ((float)(0.0f)))))
		{
			goto IL_0075;
		}
	}
	{
		// catMelee.Attack(target.transform.position);
		CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * L_6 = __this->get_catMelee_5();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_7 = __this->get_target_7();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		CatMelee_Attack_m842B5023CE89B1923BC4B5DDEA768850C287E1A7(L_6, L_9, /*hidden argument*/NULL);
		// target.TakeHit(catMelee.damage);
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_10 = __this->get_target_7();
		CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * L_11 = __this->get_catMelee_5();
		float L_12 = L_11->get_damage_13();
		Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41(L_10, L_12, /*hidden argument*/NULL);
		// Debug.Log("Attack!!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral42EA15876C33949F765AE605B9DFCCDC4E9BE311, /*hidden argument*/NULL);
		// catMelee.ResetCoolDownTime();
		CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 * L_13 = __this->get_catMelee_5();
		CatMelee_ResetCoolDownTime_m5D0502998F055E6C26BF5A21FA49AA6A86CF8F78(L_13, /*hidden argument*/NULL);
	}

IL_0075:
	{
		// }
		return;
	}
}
// System.Void Script.Character.DamageArea.CharacterDamageArea::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterDamageArea__ctor_mE6D4FC9021555B4999BC756B1CE1BA32CD64763C (CharacterDamageArea_tB9235C7210B5BBCFC4AF682FA56CF41B9DAF01EE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DontDestoryOnLoad::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontDestoryOnLoad_Start_mF1ECB298CFB40940BA1402634E7553F04350C70D (DontDestoryOnLoad_t9542A540955D6487F0538CCD634920D37129DEF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DontDestroyOnLoad(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DontDestoryOnLoad::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontDestoryOnLoad_Update_mDBA5350D6EECD070FBFA9A32867F7F736AE23BC9 (DontDestoryOnLoad_t9542A540955D6487F0538CCD634920D37129DEF1 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void DontDestoryOnLoad::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DontDestoryOnLoad__ctor_m684596B308FCF9D95C6C3FE10FE0799361B41C7F (DontDestoryOnLoad_t9542A540955D6487F0538CCD634920D37129DEF1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Enemy.Enemy::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Start_m7A80DACA3BD97BCEFA962C6BC1B7072E6C41C63E (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// ResetTimeCtrl();
		Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156(__this, /*hidden argument*/NULL);
		// MoveLeft();
		Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Update_mCE1C54D9179962C47BBB32BEDAE6EF9210984833 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// Move();
		Enemy_Move_mDAC2F4D048EF5160585A1828E23D3CB92070BACD(__this, /*hidden argument*/NULL);
		// TimeCtrl();
		Enemy_TimeCtrl_mB4099D838835079D46AC4623CB8EEFE7AD6BAFFE(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::MoveLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moveDir = new Vector3(-speed,0,0);
		float L_0 = __this->get_speed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), ((-L_0)), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_moveDir_6(L_1);
		// moveStatus = "left";
		__this->set_moveStatus_14(_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::MoveRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moveDir = new Vector3(speed,0,0);
		float L_0 = __this->get_speed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_moveDir_6(L_1);
		// moveStatus = "right";
		__this->set_moveStatus_14(_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::MoveUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moveDir = new Vector3(0,speed,0);
		float L_0 = __this->get_speed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), L_0, (0.0f), /*hidden argument*/NULL);
		__this->set_moveDir_6(L_1);
		// moveStatus = "up";
		__this->set_moveStatus_14(_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::MoveDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moveDir = new Vector3(0,-speed,0);
		float L_0 = __this->get_speed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), ((-L_0)), (0.0f), /*hidden argument*/NULL);
		__this->set_moveDir_6(L_1);
		// moveStatus = "down";
		__this->set_moveStatus_14(_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Move()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Move_mDAC2F4D048EF5160585A1828E23D3CB92070BACD (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// transform.position += moveDir * Time.deltaTime* multSpeed;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = L_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_moveDir_6();
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_3, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_multSpeed_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_5, L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_7, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_1, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, float ___damage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral594DC94B47F9965C6B26B6579E1E586C3151254E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Take hit!!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral594DC94B47F9965C6B26B6579E1E586C3151254E, /*hidden argument*/NULL);
		// Playsound(aud_Takehit);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_0 = __this->get_aud_Takehit_19();
		Enemy_Playsound_m76AEE5C1AFDDEE80543917074E15791887E3E04F(__this, L_0, /*hidden argument*/NULL);
		// hp -= damage;
		float L_1 = __this->get_hp_4();
		float L_2 = ___damage0;
		__this->set_hp_4(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		// if (hp <= 0)
		float L_3 = __this->get_hp_4();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_0037;
		}
	}
	{
		// Dead();
		Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C(__this, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Kill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Kill_m15BEB3F4C334EF8225B15CC5C0077AF4A12D1A0B (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// Dead();
		Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Playsound(UnityEngine.AudioClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Playsound_m76AEE5C1AFDDEE80543917074E15791887E3E04F (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___sound0, const RuntimeMethod* method)
{
	{
		// audioSource.clip = sound;
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_18();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_1 = ___sound0;
		AudioSource_set_clip_mD1F50F7BA6EA3AF25B4922473352C5180CFF7B2B(L_0, L_1, /*hidden argument*/NULL);
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_2 = __this->get_audioSource_18();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::ChangeMoveDir(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_ChangeMoveDir_mC518ABB9948229742CDE15B74703B9490B7446E1 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method)
{
	{
		// moveDir = direction;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___direction0;
		__this->set_moveDir_6(L_0);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Dead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Dead_mB3163FED2547678FAA2F9A638237EBAE9C13EE9C (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Money.Instance.GiveMoney(moneyDrop);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_1 = __this->get_moneyDrop_5();
		Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F(L_0, L_1, /*hidden argument*/NULL);
		// MissionManager.Instance.EnemyScored(1);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_2;
		L_2 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		MissionManager_EnemyScored_m7C68CEFFF5713951EB7533DF47E4C617E04142D9(L_2, 1, /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::FlipMoveDir()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// switch (moveStatus)
		String_t* L_0 = __this->get_moveStatus_14();
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_2 = V_0;
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_2, _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_4 = V_0;
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_6 = V_0;
		bool L_7;
		L_7 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = V_0;
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_8, _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_003f:
	{
		// MoveDown();
		Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929(__this, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0046:
	{
		// MoveUp();
		Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461(__this, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_004d:
	{
		// MoveRight();
		Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E(__this, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0054:
	{
		// MoveLeft();
		Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587(__this, /*hidden argument*/NULL);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::Reflect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_Reflect_mB4E0614BD3165AF81B6A41A981B94BE441638629 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// FlipMoveDir();
		Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4(__this, /*hidden argument*/NULL);
		// multSpeed = knockBackForce;
		float L_0 = __this->get_knockBackForce_17();
		__this->set_multSpeed_8(L_0);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_OnTriggerEnter2D_m06CBB2F737E9AD5016794003BC224EEC981BD0CA (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_mE27BBB6D62139C6AABDF5EFE59917F8844F43C23_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_m2641F635A37E4ABBD38D51ADC3F77F630EA3BC1D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = other.GetComponent<CatShield>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * L_1;
		L_1 = Component_GetComponent_TisCatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_mE27BBB6D62139C6AABDF5EFE59917F8844F43C23(L_0, /*hidden argument*/Component_GetComponent_TisCatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E_mE27BBB6D62139C6AABDF5EFE59917F8844F43C23_RuntimeMethod_var);
		__this->set_target_12(L_1);
		// exitDoor = other.GetComponent<ExitDoor>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_2 = ___other0;
		ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * L_3;
		L_3 = Component_GetComponent_TisExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_m2641F635A37E4ABBD38D51ADC3F77F630EA3BC1D(L_2, /*hidden argument*/Component_GetComponent_TisExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E_m2641F635A37E4ABBD38D51ADC3F77F630EA3BC1D_RuntimeMethod_var);
		__this->set_exitDoor_13(L_3);
		// if (target != null && target.isPlaced)
		CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * L_4 = __this->get_target_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * L_6 = __this->get_target_12();
		bool L_7 = ((BaseCatChar_t482429F66AA72EF4FDCEC1B7B4E5EAB2FAFD3FC4 *)L_6)->get_isPlaced_5();
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		// Reflect();
		Enemy_Reflect_mB4E0614BD3165AF81B6A41A981B94BE441638629(__this, /*hidden argument*/NULL);
		// ResetTimeCtrl();
		Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156(__this, /*hidden argument*/NULL);
		// target.TakeHit(damage);
		CatShield_tB22A3BA3D3DAC873DB9497C86385A7899C92178E * L_8 = __this->get_target_12();
		float L_9 = __this->get_damage_9();
		CatShield_TakeHit_m190ABC57FD09DBBBD24F172F2474883521BD74DB(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0050:
	{
		// if (exitDoor != null)
		ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * L_10 = __this->get_exitDoor_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_10, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006f;
		}
	}
	{
		// exitDoor.TakeHit(damage);
		ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * L_12 = __this->get_exitDoor_13();
		float L_13 = __this->get_damage_9();
		ExitDoor_TakeHit_mC9D5DEAD44D641FD35E135CEA79308AD76EC897E(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006f:
	{
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::ResetSpeedMult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_ResetSpeedMult_m419D80A63CFBA625803D6CEF30E00A397E7EE3FB (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// multSpeed = 1;
		__this->set_multSpeed_8((1.0f));
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::ResetTimeCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_ResetTimeCtrl_mD568A6E0C5FBAA38D98C8E361C3873AEDB710156 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// cdt = cdtMax;
		float L_0 = __this->get_cdtMax_16();
		__this->set_cdt_15(L_0);
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::TimeCtrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy_TimeCtrl_mB4099D838835079D46AC4623CB8EEFE7AD6BAFFE (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		// if (cdt > 0)
		float L_0 = __this->get_cdt_15();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_001f;
		}
	}
	{
		// cdt -= Time.deltaTime;
		float L_1 = __this->get_cdt_15();
		float L_2;
		L_2 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_cdt_15(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
	}

IL_001f:
	{
		// if (cdt <= 0 && multSpeed != 1)
		float L_3 = __this->get_cdt_15();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		float L_4 = __this->get_multSpeed_8();
		if ((((float)L_4) == ((float)(1.0f))))
		{
			goto IL_0045;
		}
	}
	{
		// FlipMoveDir();
		Enemy_FlipMoveDir_m947D4E3B674E57D0EAF7F7D8A502340273F527E4(__this, /*hidden argument*/NULL);
		// ResetSpeedMult();
		Enemy_ResetSpeedMult_m419D80A63CFBA625803D6CEF30E00A397E7EE3FB(__this, /*hidden argument*/NULL);
	}

IL_0045:
	{
		// }
		return;
	}
}
// System.Void Script.Enemy.Enemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enemy__ctor_mD4DB24A77D2C2CB43C49F78F6529BAD6BE209523 (Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.UI.EnemyLeftDisplay::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyLeftDisplay_Start_m69CEFDCE50FC07642ABF22369424302680553E6D (EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (enemyLeftText == null)
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_enemyLeftText_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		// GetComponent<Text>();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_2;
		L_2 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(__this, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void Script.UI.EnemyLeftDisplay::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyLeftDisplay_Update_m080FE6FA1F785DC27F4DBDFAFF455ADC81E0D109 (EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38DB4D59B61D0EAF394CBD85005F176490748E0E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemyLeftText.text = $"Enemy Left : {MissionManager.Instance.enemyLeft}";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_enemyLeftText_4();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_1;
		L_1 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		int32_t L_2 = L_1->get_enemyLeft_8();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5;
		L_5 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral38DB4D59B61D0EAF394CBD85005F176490748E0E, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// }
		return;
	}
}
// System.Void Script.UI.EnemyLeftDisplay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyLeftDisplay__ctor_m8F2B4D422373983AD5DE7EB8008ECD41A0C8C6D5 (EnemyLeftDisplay_t427DF91F1168AD8106A89DE3BA0EF63ACCB618C5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Utility.EnemyList::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyList_Start_m95FEA6B01F792722641A11943E58996FCD08E8D0 (EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Utility.EnemyList::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyList_Update_mFDE57CAB9C507EC2482B7AE4F32288A6EEC4169F (EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Utility.EnemyList::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnemyList__ctor_m5EE9B37FB4E3A01813C4F201AFF7D9FC85715634 (EnemyList_t7F7E866E4BC4FAE3E5A2F7939814113510994EC8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.ExitDoor::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitDoor_Start_m13891693B8E6565AAAB2C8BE7AFDF2CDEFE880F9 (ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * __this, const RuntimeMethod* method)
{
	{
		// hp = hpMax;
		float L_0 = __this->get_hpMax_4();
		__this->set_hp_5(L_0);
		// }
		return;
	}
}
// System.Void Script.Map.ExitDoor::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitDoor_OnTriggerEnter2D_mF52D19F36242CF772503CF35004E76C03805E371 (ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemy = other.GetComponent<Enemy.Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_1;
		L_1 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_0, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		__this->set_enemy_6(L_1);
		// if (enemy != null)
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_2 = __this->get_enemy_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		// enemy.Kill();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_4 = __this->get_enemy_6();
		Enemy_Kill_m15BEB3F4C334EF8225B15CC5C0077AF4A12D1A0B(L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void Script.Map.ExitDoor::TakeHit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitDoor_TakeHit_mC9D5DEAD44D641FD35E135CEA79308AD76EC897E (ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * __this, float ___damage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hp -= damage;
		float L_0 = __this->get_hp_5();
		float L_1 = ___damage0;
		__this->set_hp_5(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if (hp <= 0)
		float L_2 = __this->get_hp_5();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_0025;
		}
	}
	{
		// GameManager.Instance.GameOver();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_3;
		L_3 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A(L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		// }
		return;
	}
}
// System.Void Script.Map.ExitDoor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitDoor__ctor_mF889A857E4FA662218AF656AA3FA847F100048DC (ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameManager::StartPlayGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_StartPlayGame_m7BBCED5DB288F74C0CD49F9F943D854C381E23E0 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4FFE5C21412E3B6B065B9F27BCD4E95FED8914A8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_13();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_0, /*hidden argument*/NULL);
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_mainMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// SceneManager.LoadScene("Mission1", LoadSceneMode.Additive);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m6B3C9B5DDE6CDE2A041D05C4F3BE4A3D3D745B70(_stringLiteral4FFE5C21412E3B6B065B9F27BCD4E95FED8914A8, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::GameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// gameOverMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::Win()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Win_mEE312032712783936AAC9B0EF0CED7BDB345C973 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MainMenu.Instance.GameOverMenu();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97(/*hidden argument*/MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var);
		MainMenu_GameOverMenu_m1C3686D2523C552479A2FE8B38DD9C35754D16D2(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::DestroyAllEnemy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_DestroyAllEnemy_m3A3AFCD2EC6A82A49FAC55F527ACC6F9E91910EE (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3260331AF5DA53ABC7CA7BAF659CF8D9FC93DEC7);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// allEnemys = GameObject.FindGameObjectsWithTag("Enemy");
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0;
		L_0 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(_stringLiteral3260331AF5DA53ABC7CA7BAF659CF8D9FC93DEC7, /*hidden argument*/NULL);
		__this->set_allEnemys_11(L_0);
		// foreach (GameObject enemy in allEnemys)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1 = __this->get_allEnemys_11();
		V_0 = L_1;
		V_1 = 0;
		goto IL_0027;
	}

IL_001b:
	{
		// foreach (GameObject enemy in allEnemys)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		// Destroy(enemy);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0027:
	{
		// foreach (GameObject enemy in allEnemys)
		int32_t L_7 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameManager::DestroyAllUnit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_DestroyAllUnit_m6BDE5206F29B58B5724BD590AA77466D6B545E97 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral586825A85BD3ABBC2F448E88E81F8DFBBE3A1EC4);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// allCharacters = GameObject.FindGameObjectsWithTag("Character");
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0;
		L_0 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(_stringLiteral586825A85BD3ABBC2F448E88E81F8DFBBE3A1EC4, /*hidden argument*/NULL);
		__this->set_allCharacters_12(L_0);
		// foreach (GameObject character in allCharacters)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1 = __this->get_allCharacters_12();
		V_0 = L_1;
		V_1 = 0;
		goto IL_0027;
	}

IL_001b:
	{
		// foreach (GameObject character in allCharacters)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		// Destroy(character);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0027:
	{
		// foreach (GameObject character in allCharacters)
		int32_t L_7 = V_1;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// Click();
		GameManager_Click_mF51E03F3AF95EA64F1B3AED7315C42BE91F1A4C9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::Click()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Click_mF51E03F3AF95EA64F1B3AED7315C42BE91F1A4C9 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_2;
		L_2 = Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB(L_0, L_1, /*hidden argument*/NULL);
		// if (Physics.Raycast(camRay, out hit ,100.0f))
		bool L_3;
		L_3 = Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4(L_2, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), (100.0f), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		// if (hit.transform != null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		// Debug.Log("hello");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteralB6D795FBD58CC7592D955A219374339A323801A9, /*hidden argument*/NULL);
	}

IL_0036:
	{
		// }
		return;
	}
}
// System.Void GameManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1__ctor_m1C4CAEFF311D0D70F5075E5936B923F272D2787E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m1C4CAEFF311D0D70F5075E5936B923F272D2787E(__this, /*hidden argument*/MonoSingleton_1__ctor_m1C4CAEFF311D0D70F5075E5936B923F272D2787E_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.Hit.HitDamage::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamage_Start_m029FDD15023CD5C6338E1CD6E7E8527E014A685F (HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamage::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamage_Destroy_m03E81A19B5F3FCD5D9D4A004B47BE4FC29099CDC (HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamage::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamage_Update_m31C09B2A062F93546465188264850678905EC0ED (HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamage__ctor_m8543E46E21A1181502B34E95DC26096CB75A1294 (HitDamage_t5D317AC5A34715FDD627DDADCC8190A03770A1CD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Character.Hit.HitDamageBullet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet_Start_m508247ADABE1094304D65D16388EDA06DDF5B215 (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 enemyDir = targetOBJ.transform.position.normalized;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_targetOBJ_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamageBullet::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet_Update_m09441BB2E1761BEA6D9554C6BC9F2DBCD4751ABB (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (targetOBJ != null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_targetOBJ_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		// MoveTo(targetOBJ);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_targetOBJ_5();
		HitDamageBullet_MoveTo_mE6F6507496D23D4B3AB8D088CA6ED595909D21C6(__this, L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_001b:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamageBullet::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet_OnTriggerEnter2D_mAB6CA0EAC2978E765F261ABC6424B7FD3C3A38E8 (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.gameObject == targetOBJ)
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_targetOBJ_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		// var target = other.GetComponent<Enemy.Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_4 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_5;
		L_5 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_4, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		// target.TakeHit(catFire.damage);
		CatFire_tE152E969E9E522EB9F418A81A60160A166CB14B5 * L_6 = __this->get_catFire_6();
		float L_7 = ((CatMelee_tA8D9DC899FF71E9C8F1B8CFAE623AEF5F730F241 *)L_6)->get_damage_13();
		Enemy_TakeHit_m32860D9210DF84FE06A4A5FD6094F6BE4676DA41(L_5, L_7, /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_8, /*hidden argument*/NULL);
	}

IL_0034:
	{
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamageBullet::MoveTo(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet_MoveTo_mE6F6507496D23D4B3AB8D088CA6ED595909D21C6 (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___target0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 enemyDir = target.transform.position - transform.position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___target0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// enemyDir.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		// transform.Translate(enemyDir*Time.deltaTime*speed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_7, L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_speed_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_9, L_10, /*hidden argument*/NULL);
		Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0(L_6, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Character.Hit.HitDamageBullet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HitDamageBullet__ctor_mF679D7ACFBDDC133C04164B0A8938F2835130DEA (HitDamageBullet_t0E76601CAF672DFC44E4444737DDE86DC782F7FD * __this, const RuntimeMethod* method)
{
	{
		HitDamage__ctor_m8543E46E21A1181502B34E95DC26096CB75A1294(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.UI.HpBar::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HpBar_Start_mD11553FFD1EC893692B5F48F7139A6691257FD0F (HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSlider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_mD501A27463515FA99A5A93A10E37F913696D20C4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// hpSlider = GetComponent<Slider>();
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_0;
		L_0 = Component_GetComponent_TisSlider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_mD501A27463515FA99A5A93A10E37F913696D20C4(__this, /*hidden argument*/Component_GetComponent_TisSlider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_mD501A27463515FA99A5A93A10E37F913696D20C4_RuntimeMethod_var);
		__this->set_hpSlider_5(L_0);
		// hpSlider.maxValue = exitDoorSript.hpMax;
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_1 = __this->get_hpSlider_5();
		ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * L_2 = __this->get_exitDoorSript_4();
		float L_3 = L_2->get_hpMax_4();
		Slider_set_maxValue_m5CDA3D451B68CF2D3FCFF43D1738D1DCC1C6425B(L_1, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.HpBar::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HpBar_Update_mD7026F64E4A9A8E085372A76CFBF13AEEBDDC7FB (HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB * __this, const RuntimeMethod* method)
{
	{
		// hpSlider.value = exitDoorSript.hp;
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_0 = __this->get_hpSlider_5();
		ExitDoor_t0ABC3C4B4D41A094D4727F0632D6C215A30B270E * L_1 = __this->get_exitDoorSript_4();
		float L_2 = L_1->get_hp_5();
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, L_2);
		// }
		return;
	}
}
// System.Void Script.UI.HpBar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HpBar__ctor_mAE6ED973236E66EE76BCEBB8058A3697174ED037 (HpBar_tD6D7A7F2D0C67313CA4DB6C6721950BDF25C79DB * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Utility.LoadManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadManager_Start_mBEBED79E4C651C714EEB9A187492A6941210F606 (LoadManager_t166D027CB2E4C1D1C702ECB86A25F71C8D834D91 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5092D61AADF5226DE949D8590D5BE79F89AF8618);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("GameEngine", LoadSceneMode.Additive);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m6B3C9B5DDE6CDE2A041D05C4F3BE4A3D3D745B70(_stringLiteral5092D61AADF5226DE949D8590D5BE79F89AF8618, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Utility.LoadManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadManager_Update_m5C6D8E807663A5938A7A442D8FC762F22ACC5933 (LoadManager_t166D027CB2E4C1D1C702ECB86A25F71C8D834D91 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Utility.LoadManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadManager__ctor_m2D6202BAFF14E8DC356007DA86DE8EF58C6474B2 (LoadManager_t166D027CB2E4C1D1C702ECB86A25F71C8D834D91 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainGameScene::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGameScene_Start_mD147BCC62885A9DD85408C33589F0260E2731B49 (MainGameScene_tCF91F64FDE86C11BE551FC8E1B32A9DB67761DB7 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void MainGameScene::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainGameScene__ctor_mB30666241CA8E49251D2F6B83FDC3A35D010515A (MainGameScene_tCF91F64FDE86C11BE551FC8E1B32A9DB67761DB7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.UI.MainMenu::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Start_mFB01E91118351E6A180E4B35DD8208570B3423A9 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// mainMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::ToMissionMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_ToMissionMenu_m4E0B4F72D691101344ED04BE27C17144BA284075 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::BackToMainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_BackToMainMenu_m4A1EB4F827008EAAE312A42222C0DAB9F487E7CE (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// mainMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::LoadMission(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_LoadMission_m49E22F2C0751DD492EFB1D927E1322C45530F5DB (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, String_t* ___mission0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// sceneNow = mission;
		String_t* L_1 = ___mission0;
		__this->set_sceneNow_12(L_1);
		// SceneManager.LoadScene(mission, LoadSceneMode.Additive);
		String_t* L_2 = ___mission0;
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m6B3C9B5DDE6CDE2A041D05C4F3BE4A3D3D745B70(L_2, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::ContinueGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_ContinueGame_m3FB0CD0540008DC130E94148E91E748DF5ED436F (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::Unpause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::GameOverMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_GameOverMenu_m1C3686D2523C552479A2FE8B38DD9C35754D16D2 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// Unpause();
		MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE(__this, /*hidden argument*/NULL);
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::ExitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_ExitGame_mB79D76C015CA5ED89BEC1719CC6574065C77B1FE (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::ToSelectMission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_ToSelectMission_m819C84188FCE9AAEFC66A4104C1ACBACCB50C5CB (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	{
		// Unpause();
		MainMenu_Unpause_m0881E4DFB26F9F48A904831743BD455063A69EAE(__this, /*hidden argument*/NULL);
		// QuitMission();
		MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898(__this, /*hidden argument*/NULL);
		// mainMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_mainMenu_7();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// pauseMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// missionMenu.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_missionMenu_9();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::QuitMission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_11();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_0, /*hidden argument*/NULL);
		// GameManager.Instance.DestroyAllEnemy();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1;
		L_1 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		GameManager_DestroyAllEnemy_m3A3AFCD2EC6A82A49FAC55F527ACC6F9E91910EE(L_1, /*hidden argument*/NULL);
		// GameManager.Instance.DestroyAllUnit();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_2;
		L_2 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		GameManager_DestroyAllUnit_m6BDE5206F29B58B5724BD590AA77466D6B545E97(L_2, /*hidden argument*/NULL);
		// gameOverMenu.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_gameOverMenu_8();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// SceneManager.UnloadSceneAsync(sceneNow);
		String_t* L_4 = __this->get_sceneNow_12();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * L_5;
		L_5 = SceneManager_UnloadSceneAsync_mF564BF92447F58313A518206EE15E5DEED0448EF(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::RestartMission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu_RestartMission_m0587B90AEE8AC5E8868B75261CFAEA3F64ACF7D1 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// QuitMission();
		MainMenu_QuitMission_mFA4C67B3D99811F15211A638746C82E090BF5898(__this, /*hidden argument*/NULL);
		// SceneManager.LoadScene(sceneNow, LoadSceneMode.Additive);
		String_t* L_0 = __this->get_sceneNow_12();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m6B3C9B5DDE6CDE2A041D05C4F3BE4A3D3D745B70(L_0, 1, /*hidden argument*/NULL);
		// MissionManager.Instance.EnemyScoreReset();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_1;
		L_1 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		MissionManager_EnemyScoreReset_m4CD561F1F4C88D55C232A092577228D956C4284C(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.UI.MainMenu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainMenu__ctor_mBA722739613EF4F7A5503DD127B61987270B02A1 (MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1__ctor_m714FDB833649A832A7113836EC9F35AF4ADFF368_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m714FDB833649A832A7113836EC9F35AF4ADFF368(__this, /*hidden argument*/MonoSingleton_1__ctor_m714FDB833649A832A7113836EC9F35AF4ADFF368_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.Mission::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mission_Start_m5A6A792E9D8375F70EAF79C321BC046E3AC515EA (Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Money.Instance.money = moneyStart;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_0;
		L_0 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_1 = __this->get_moneyStart_5();
		L_0->set_money_7(L_1);
		// MissionManager.Instance.waveSystem.enemyLists = enemyLists;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_2;
		L_2 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * L_3 = L_2->get_waveSystem_11();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_4 = __this->get_enemyLists_4();
		L_3->set_enemyLists_5(L_4);
		// MissionManager.Instance.MissionUpdate();
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_5;
		L_5 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Map.Mission::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mission_Update_mC912E95F3235165B5335C184845D9B8986FBB89D (Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Map.Mission::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mission__ctor_mDB11C1F9D50E4E4C77631DDD2CE4E265DCE6F804 (Mission_t12DDEAAF5F45ACD550B2E628124B07433716BB51 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Utility.MissionManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_Start_mA02C8F3F85E450223FA4D69625B0572511A23275 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method)
{
	{
		// MissionUpdate();
		MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Utility.MissionManager::MissionUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_MissionUpdate_m9954A054CD8FBFB50E68F850BBCA865C8A6EDD09 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method)
{
	{
		// allEnemyThisMission = waveSystem.enemyLists.Length;
		WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * L_0 = __this->get_waveSystem_11();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1 = L_0->get_enemyLists_5();
		__this->set_allEnemyThisMission_7(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))));
		// enemyLeft = allEnemyThisMission;
		int32_t L_2 = __this->get_allEnemyThisMission_7();
		__this->set_enemyLeft_8(L_2);
		// }
		return;
	}
}
// System.Void Script.Utility.MissionManager::EnemyScoreReset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_EnemyScoreReset_m4CD561F1F4C88D55C232A092577228D956C4284C (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method)
{
	{
		// enemyLeft = allEnemyThisMission;
		int32_t L_0 = __this->get_allEnemyThisMission_7();
		__this->set_enemyLeft_8(L_0);
		// }
		return;
	}
}
// System.Void Script.Utility.MissionManager::EnemyScored(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager_EnemyScored_m7C68CEFFF5713951EB7533DF47E4C617E04142D9 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, int32_t ___quantity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemyLeft -= quantity;
		int32_t L_0 = __this->get_enemyLeft_8();
		int32_t L_1 = ___quantity0;
		__this->set_enemyLeft_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1)));
		// if (enemyLeft <= 0)
		int32_t L_2 = __this->get_enemyLeft_8();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		// GameManager.Instance.Win();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t5915A8510C95FAD082113E58437A047CBCA0D32E_il2cpp_TypeInfo_var);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_3;
		L_3 = MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC(/*hidden argument*/MonoSingleton_1_get_Instance_mE0D937CC2DD7A133DB024E1F9E7214CD206AFCBC_RuntimeMethod_var);
		GameManager_Win_mEE312032712783936AAC9B0EF0CED7BDB345C973(L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Void Script.Utility.MissionManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MissionManager__ctor_mD4375AA12B01D60C54E5E6C0314EC9DF7175FAF7 (MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1__ctor_m016CCA4BA41740634C0C66CCE7D7FF46B3ADD926_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m016CCA4BA41740634C0C66CCE7D7FF46B3ADD926(__this, /*hidden argument*/MonoSingleton_1__ctor_m016CCA4BA41740634C0C66CCE7D7FF46B3ADD926_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.Money::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money_Update_m09ECD0552CCBBBDD222F24A0F19DD77956267C75 (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Script.Map.Money::GiveMoney(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money_GiveMoney_m3B47B716442A4DE094025DB4B0DC1ACAAD76954F (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, int32_t ___money0, const RuntimeMethod* method)
{
	{
		// this.money += money;
		int32_t L_0 = __this->get_money_7();
		int32_t L_1 = ___money0;
		__this->set_money_7(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		// }
		return;
	}
}
// System.Void Script.Map.Money::MoneyCheckOut(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money_MoneyCheckOut_mC1AF552CE715614BCC588D381E378C2024F3CF16 (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, int32_t ___money0, const RuntimeMethod* method)
{
	{
		// this.money -= money;
		int32_t L_0 = __this->get_money_7();
		int32_t L_1 = ___money0;
		__this->set_money_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1)));
		// }
		return;
	}
}
// System.Void Script.Map.Money::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Money__ctor_m7E96BF51569536AE65915183551B6FCD4FB0D2D8 (Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1__ctor_m20F70D38D8139FF5AB0E960955FFF561F93D0710_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		MonoSingleton_1__ctor_m20F70D38D8139FF5AB0E960955FFF561F93D0710(__this, /*hidden argument*/MonoSingleton_1__ctor_m20F70D38D8139FF5AB0E960955FFF561F93D0710_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.UI.MoneyDisplay::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoneyDisplay_Start_mDED1D862FEE3D5C64D1E923EA86047A8A993099F (MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<Text>();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0;
		L_0 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(__this, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Script.UI.MoneyDisplay::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoneyDisplay_Update_m957EA6AF0036944C87FACD8546D25DCDFCEB6A21 (MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94);
		s_Il2CppMethodInitialized = true;
	}
	{
		// moneyText.text = $"{Money.Instance.money}";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_moneyText_4();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tD6DE4019FA583B82385A682920206EC580C1A6F0_il2cpp_TypeInfo_var);
		Money_t71F924FDF2F8F6A1E9562A94364FE4D7A02EE4B4 * L_1;
		L_1 = MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03(/*hidden argument*/MonoSingleton_1_get_Instance_mF2A3596AC22C2A3A39600C738D371CB86AD08D03_RuntimeMethod_var);
		int32_t L_2 = L_1->get_money_7();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5;
		L_5 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral23114468D04FA2B7A2DA455B545DB914D0A3ED94, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		// }
		return;
	}
}
// System.Void Script.UI.MoneyDisplay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoneyDisplay__ctor_m2981E1A16DF68402A137D54AA91CE41AE0E19C38 (MoneyDisplay_t8BF404A6A726267D1393369FB02FFD2FC48EDF4D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PauseBotton::Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseBotton_Pause_m7EACECE09CBE86C991D2E9E09B10EA654D2A3025 (PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// audioSource.Play();
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_0 = __this->get_audioSource_4();
		AudioSource_Play_mED16664B8F8F3E4D68785C8C00FC96C4DF053AE1(L_0, /*hidden argument*/NULL);
		// if (Time.timeScale >= 1)
		float L_1;
		L_1 = Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4(/*hidden argument*/NULL);
		if ((!(((float)L_1) >= ((float)(1.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// MainMenu.Instance.pauseMenu.SetActive(true);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * L_2;
		L_2 = MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97(/*hidden argument*/MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = L_2->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0032:
	{
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// MainMenu.Instance.pauseMenu.SetActive(false);
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_t08EA895392EED05D9F60F7B8FF91B2B135C32A5A_il2cpp_TypeInfo_var);
		MainMenu_t19BBFCFF5E6B3AEEDE40EED5941C842372BF3712 * L_4;
		L_4 = MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97(/*hidden argument*/MonoSingleton_1_get_Instance_mF0FB359BD6A50794909C7A6D090F8F53E43BFF97_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = L_4->get_pauseMenu_10();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PauseBotton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseBotton__ctor_mF41F008D32F714DFABAFF5B445B275F3E847851F (PauseBotton_tF8081AE015C60B3820F81B489DE3844B80A003AF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.PlaceableArea::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlaceableArea__ctor_m9A60E0D3BDEA111825956D4726867E016224C5C8 (PlaceableArea_t1C574A2DDFDD43314DB27481A9C0663680E04FDD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.PlaceableAreaOnField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlaceableAreaOnField__ctor_m4BF8F23BF29C82BA7B527A6C5BF8B7AB01415FF1 (PlaceableAreaOnField_tCD6B5829E4DD09D90E5B138E48ECBD87E9510A7B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.SnapPoint::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SnapPoint__ctor_m66C9037DF5523C44D203EAE05060D7CA456F9FEA (SnapPoint_t3E8BE09B4355AED7ED8CCF2049CFAAE5B8421FAD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.SnapPointField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SnapPointField__ctor_m1AFC9AE0173236234CB51345D78FE9F7A7A54A13 (SnapPointField_t7D8385A372032D364F54551D890308C67872B555 * __this, const RuntimeMethod* method)
{
	{
		SnapPoint__ctor_m66C9037DF5523C44D203EAE05060D7CA456F9FEA(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Map.Spawner::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Start_m03C737B976E3908947711644C9A7F83DAE335585 (Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C * __this, const RuntimeMethod* method)
{
	{
		// enemyListsIndex = 0;
		__this->set_enemyListsIndex_8(0);
		// }
		return;
	}
}
// System.Void Script.Map.Spawner::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Update_m6DDDF7293967DCABBE1AA3BD7822047EC6CDBAC9 (Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C * __this, const RuntimeMethod* method)
{
	{
		// cooldown -= Time.deltaTime;
		float L_0 = __this->get_cooldown_5();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_cooldown_5(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// Spawn();
		Spawner_Spawn_m04CCC86C5F5C3191376D7C37D63EA61685D94059(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Script.Map.Spawner::Spawn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner_Spawn_m04CCC86C5F5C3191376D7C37D63EA61685D94059 (Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (enemyListsIndex <= MissionManager.Instance.allEnemyThisMission)
		int32_t L_0 = __this->get_enemyListsIndex_8();
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_1;
		L_1 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		int32_t L_2 = L_1->get_allEnemyThisMission_7();
		if ((((int32_t)L_0) > ((int32_t)L_2)))
		{
			goto IL_0076;
		}
	}
	{
		// enemy = MissionManager.Instance.waveSystem.enemyLists[enemyListsIndex];
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_3;
		L_3 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * L_4 = L_3->get_waveSystem_11();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_5 = L_4->get_enemyLists_5();
		int32_t L_6 = __this->get_enemyListsIndex_8();
		int32_t L_7 = L_6;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_enemy_4(L_8);
		// if (cooldown <= 0)
		float L_9 = __this->get_cooldown_5();
		if ((!(((float)L_9) <= ((float)(0.0f)))))
		{
			goto IL_0076;
		}
	}
	{
		// Instantiate(enemy, transform.position, quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_enemy_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var);
		quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F  L_13 = ((quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t576E26BB07E929B6EA83D7186A181A799AF9920F_il2cpp_TypeInfo_var))->get_identity_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_14;
		L_14 = quaternion_op_Implicit_mB7132FF18D78E904DBEB54ED17189EE01A6AF093(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_10, L_12, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// cooldown = cooldownMax;
		float L_16 = __this->get_cooldownMax_6();
		__this->set_cooldown_5(L_16);
		// enemyListsIndex ++;
		int32_t L_17 = __this->get_enemyListsIndex_8();
		__this->set_enemyListsIndex_8(((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1)));
	}

IL_0076:
	{
		// }
		return;
	}
}
// System.Void Script.Map.Spawner::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Spawner__ctor_m01E343F3FE121E9DEBABBE6E33281F89BF33930C (Spawner_t24A7BD4E85E0B0E71BF3D852B24D46DCE0C21C4C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TurnDirPoint::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnDirPoint_Start_m0DD9DDD9AB0645EEBCE50C32708737663701F2AA (TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B * __this, const RuntimeMethod* method)
{
	{
		// DirStatus.ToLower();
		String_t* L_0 = __this->get_DirStatus_6();
		String_t* L_1;
		L_1 = String_ToLower_m7875A49FE166D0A68F3F6B6E70C0C056EBEFD31D(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void TurnDirPoint::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnDirPoint_OnTriggerEnter2D_mAB3275449BFFC89D2363BAD76749066A7596D8CB (TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemyScript = other.GetComponent<Enemy>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___other0;
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_1;
		L_1 = Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0(L_0, /*hidden argument*/Component_GetComponent_TisEnemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE_mB190E073B1FBECC9CD6872AA04549598420BB2C0_RuntimeMethod_var);
		__this->set_enemyScript_5(L_1);
		// if (enemyScript != null)
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_2 = __this->get_enemyScript_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0091;
		}
	}
	{
		// if (DirStatus == "up")
		String_t* L_4 = __this->get_DirStatus_6();
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, _stringLiteral1B22F82E513C1BB368DFDBFDFB5FB8C1178CA305, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		// enemyScript.MoveUp();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_6 = __this->get_enemyScript_5();
		Enemy_MoveUp_m32B0B65B3FD40ED2CA0ECF29B6B3791BD884B461(L_6, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0038:
	{
		// else if (DirStatus == "down")
		String_t* L_7 = __this->get_DirStatus_6();
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, _stringLiteral5379C25D8F9979AD371949D2F3201B2D326743A4, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		// enemyScript.MoveDown();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_9 = __this->get_enemyScript_5();
		Enemy_MoveDown_m80526AADF346ED1C56AFBADDC916661EF7BB1929(L_9, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0056:
	{
		// else if (DirStatus == "left")
		String_t* L_10 = __this->get_DirStatus_6();
		bool L_11;
		L_11 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_10, _stringLiteral8739227E8E687EF781DA0D923452C2686CFF10A2, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0074;
		}
	}
	{
		// enemyScript.MoveLeft();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_12 = __this->get_enemyScript_5();
		Enemy_MoveLeft_mED6FD3ADBA3124400EF31E70CB9F007F45C6E587(L_12, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0074:
	{
		// else if (DirStatus == "right")
		String_t* L_13 = __this->get_DirStatus_6();
		bool L_14;
		L_14 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, _stringLiteralB1E5119D36EC43B340C0A0DDC99F1156546EA9DF, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		// enemyScript.MoveRight();
		Enemy_t5168850FE016C659571B8360B76B5DBCB8CEE3EE * L_15 = __this->get_enemyScript_5();
		Enemy_MoveRight_m66E892FEA26E468581AF771A38DCE4FF3A784D2E(L_15, /*hidden argument*/NULL);
	}

IL_0091:
	{
		// }
		return;
	}
}
// System.Void TurnDirPoint::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurnDirPoint__ctor_m65113737944B0D2DFF22C8E643B13E9A3E0C3757 (TurnDirPoint_tE60E2B9BA0DEB327B39275C2BF159B69DA559A4B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Script.Utility.WaveSystem::SetEnemyList(UnityEngine.GameObject,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaveSystem_SetEnemyList_mB48BB6F64C31F9F83D7EB0DA2222BE53446FB3FC (WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___enemy0, int32_t ___amount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < amount; i++)
		V_0 = 0;
		goto IL_0023;
	}

IL_0004:
	{
		// enemyLists[i] = enemy;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_enemyLists_5();
		int32_t L_1 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___enemy0;
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_1), (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)L_2);
		// MissionManager.Instance.allEnemyThisMission++;
		IL2CPP_RUNTIME_CLASS_INIT(MonoSingleton_1_tE62C04761820B099D76A14D5264A167CB74EAE02_il2cpp_TypeInfo_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_3;
		L_3 = MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33(/*hidden argument*/MonoSingleton_1_get_Instance_mDCE43D6CF57C7988CB22CA62AA66AF291E206C33_RuntimeMethod_var);
		MissionManager_tD1948B847C3CDE2E32E4A53A907456892E982ECC * L_4 = L_3;
		int32_t L_5 = L_4->get_allEnemyThisMission_7();
		L_4->set_allEnemyThisMission_7(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		// for (int i = 0; i < amount; i++)
		int32_t L_6 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0023:
	{
		// for (int i = 0; i < amount; i++)
		int32_t L_7 = V_0;
		int32_t L_8 = ___amount1;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Script.Utility.WaveSystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaveSystem__ctor_m9AAE748E210C416A9AE4D6580C8E5EDE49E91278 (WaveSystem_t5DC7D1691E367066DC58F050A566775E852FD2BA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
